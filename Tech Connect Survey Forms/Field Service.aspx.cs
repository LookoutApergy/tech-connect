﻿using Microsoft.Ajax.Utilities;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Net;
using Twilio.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Tech_Connect_Survey_Forms.App_Code;

namespace Tech_Connect_Survey_Forms
{
    public partial class Field_Service : System.Web.UI.Page
    {
            protected void Page_Init(object sender, EventArgs e)
            {
                Page.ClientScript.RegisterHiddenField("pageHeight", "testtext");
                Page.ClientScript.RegisterHiddenField("pageWidth", "testtext");
                ClientScriptManager cs = this.ClientScript;
                StringBuilder cstext2 = new StringBuilder();

            cstext2.Append("var strW = screen.width;");
            cstext2.Append("var strH = screen.height;");
            cstext2.Append("document.getElementById('pageWidth').value = strW;");
            cstext2.Append("document.getElementById('pageHeight').value = strH;");
            cstext2.Append("if (strW ==  1920) {strW = 1536; strH = 940; document.getElementById('BannerDiv').style.height = '150px';} " +
                "else if (strW ==  1536) {strH = 710; document.getElementById('BannerDiv').style.height = '150px'; " +
                "}  else if (strW == 1280) {strH = 590; document.getElementById('BannerDiv').style.height = '100px';document.getElementById('TCStyles').setAttribute('href', 'TechConnect Computer Styles Short.css');};");
            cstext2.Append("document.getElementById('form1').style.height = strH + 'px';");
            cstext2.Append("document.getElementById('form1').style.background = '#f8f9fa';");
            cs.RegisterStartupScript(this.GetType(), "test1", cstext2.ToString(), true);
        }
            protected void Page_Load(object sender, EventArgs e)
            {
                string userEmail = Session["s_UserEmail"] as string;
                if (!IsPostBack)
                {
                    ClearColumnB();
                    hideAdditionalCommentsDiv();
                    hideAttachFilesButton();
                    hideFileUploadPanel();
                    UpdatePanelTitle.Update();
                }
            }
            private void ClearColumnB()
            {
                Label labelB = this.FindControl("LabelBCol") as Label;
                labelB.Text = "";
                labelB.Visible = false;

                OptionBListBox.Items.Clear();
                OptionBListBox.Visible = false;
                ColBOSelectButton.Text = String.Empty;
                ColBOSelectButton.Visible = false;

                UpdatePanelLabelColB.Update();
                UpdatePanelDDColB.Update();
            }
            private void hideFileUploadPanel()
            {
                LabelMaxFiles.Text = string.Empty;
                FileUploadDiv.Visible = false;
                UpdatePanelFileUpload.Update();
            }
            private void showFileUploadPanel()
            {
                FileUploadDiv.Visible = true;
                UpdatePanelFileUpload.Update();
            }
            private void hideAttachFilesButton()
            {
                AFButtonDiv.Visible = false;
                UpdatePanelAFButton.Update();
                TechConnect_Utilities util = new TechConnect_Utilities();
                util.CleanUploadsFolder();
            }
            private void showAttachFilesButton()
            {
                AFButtonDiv.Visible = true;
                UpdatePanelAFButton.Update();
            }
            private void showAdditionalCommentsDiv()
            {
                AdditionalCommentsDiv.Visible = true;
                UpdatePanelAddComments.Update();
                showAttachFilesButton();
            }
            private void hideAdditionalCommentsDiv()
            {
                AdditionalCommentsDiv.Visible = false;
                AdditionalCommentsTextBox.Text = string.Empty;
                UpdatePanelAddComments.Update();
            }
            protected void FieldSvcOptions_SelectedIndexChanged(object sender, EventArgs e)
            {
                ClearColumnB();
                showAdditionalCommentsDiv();

                string FieldSvcOption = FieldSvcOptions.SelectedValue.ToString();
                switch (FieldSvcOption)
                {
                    case "Process & Procedures":
                        LoadColB_PandPOptions();
                        LoadColB_SelectionButton("P&P Option", "Select Option...");
                        break;
                    case "Cable Testing":
                        LoadColB_CTestingOptions();
                        LoadColB_SelectionButton("CT Option", "Select Option...");
                        break;
                    case "VSD":
                        LoadColB_VSDOptions();
                        LoadColB_SelectionButton("VSD Option", "Select Option...");
                        break;
                    case "LOOKOUT":
                        LoadColB_LOOKOUTOptions();
                        LoadColB_SelectionButton("LOOKOUT Option", "Select Option...");
                        break;
                    case "Certifications":
                        LoadColB_CertOptions();
                        LoadColB_SelectionButton("Certification Option", "Select Option...");
                        break;
                    case "Training":
                        ClearColumnB();
                        break;
                    default:
                        break;
                }
            }
            private void LoadColB_SelectionButton(string labeltext, string selectiontext)
            {
                ColBOSelectButton.Text = selectiontext;
                ColBOSelectButton.Visible = true;
                Label labelC = this.FindControl("LabelBCol") as Label;
                labelC.Text = labeltext;

                labelC.Visible = true;
                UpdatePanelLabelColB.Update();
                UpdatePanelDDColB.Update();
            }
            private void LoadColB_PandPOptions()
            {
                OptionBListBox.Items.Clear();
                OptionBListBox.Items.Add(new ListItem("Select Option...", "Select Option..."));
                OptionBListBox.Items.Add(new ListItem("F/S Manual", "F/S Manual"));
                OptionBListBox.Items.Add(new ListItem("Installations", "Installations"));
                OptionBListBox.Items.Add(new ListItem("Pulls", "Pulls"));
                OptionBListBox.Items.Add(new ListItem("Motor Assembly", "Motor Assembly"));
                OptionBListBox.Items.Add(new ListItem("Protector filling", "Protector filling"));
                OptionBListBox.Items.Add(new ListItem("Sensors", "Sensors"));
                OptionBListBox.Items.Add(new ListItem("Precheck", "Precheck"));
                OptionBListBox.Items.Add(new ListItem("Documentation", "Documentation"));
                OptionBListBox.Items.Add(new ListItem("Other", "Other"));
            }
            private void LoadColB_CTestingOptions()
            {
                OptionBListBox.Items.Clear();
                OptionBListBox.Items.Add(new ListItem("Select Option...", "Select Option..."));
                OptionBListBox.Items.Add(new ListItem("Insulation testing", "Insulation testing"));
                OptionBListBox.Items.Add(new ListItem("Hi-Pot", "Hi-Pot"));
                OptionBListBox.Items.Add(new ListItem("Splicing", "Splicing"));
                OptionBListBox.Items.Add(new ListItem("Documentation", "Documentation"));
                OptionBListBox.Items.Add(new ListItem("Other", "Other"));
            }
            private void LoadColB_VSDOptions()
            {
                OptionBListBox.Items.Clear();
                OptionBListBox.Items.Add(new ListItem("Select Option...", "Select Option..."));
                OptionBListBox.Items.Add(new ListItem("Setups", "Setups"));
                OptionBListBox.Items.Add(new ListItem("Troubleshooting & Repairs", "Troubleshooting & Repairs"));
                OptionBListBox.Items.Add(new ListItem("Startup procedures", "Startup procedures"));
                OptionBListBox.Items.Add(new ListItem("Other", "Other"));
            }
        private void LoadColB_LOOKOUTOptions()
        {
            OptionBListBox.Items.Clear();
            OptionBListBox.Items.Add(new ListItem("Select Option...", "Select Option..."));
            OptionBListBox.Items.Add(new ListItem("Installation", "Installation"));
            OptionBListBox.Items.Add(new ListItem("Setup", "Setup"));
            OptionBListBox.Items.Add(new ListItem("GPS Devices", "GPS Devices"));
            OptionBListBox.Items.Add(new ListItem("Other", "Other"));
        }
        private void LoadColB_CertOptions()
            {
                OptionBListBox.Items.Clear();
                OptionBListBox.Items.Add(new ListItem("Select Option...", "Select Option..."));
                OptionBListBox.Items.Add(new ListItem("Tooling", "Tooling"));
                OptionBListBox.Items.Add(new ListItem("Lifting Equipment", "Lifting Equipment"));
                OptionBListBox.Items.Add(new ListItem("Measuring equipment", "Measuring equipment"));
                OptionBListBox.Items.Add(new ListItem("Other", "Other"));
            }
            protected void OptionBListBox_SelectedIndexChanged(object sender, EventArgs e)
            {
                string SCOptionB = OptionBListBox.SelectedValue.ToString();
                ColBOSelectButton.Text = SCOptionB;
                OptionBListBox.Visible = false;
                UpdatePanelDDColB.Update();
                UpdatePanelTitle.Update();
            }
            private void RefreshColB()
            {
                OptionBListBox.Items[0].Selected = true;
                OptionBListBox.Visible = true;
                OptionBListBox.Focus();

                UpdatePanelLabelColB.Update();
                UpdatePanelDDColB.Update();
            }
            protected void ColBOptionSelectButton_Click(object sender, EventArgs e)
            {
                OptionBListBox.Visible = true;
                UpdatePanelDDColB.Update();
            }
            protected void ButtonSubmit_Click(object sender, EventArgs e)
            {
                string errorMsg = "";
                errorMsg = validateTextBoxes();
                if (errorMsg.Length == 0)
                {
                    StringBuilder emailBody = createBody("email");
                    StringBuilder emailSubject = createSubject("email");
                    sendEmail(emailBody, emailSubject);

                    TechConnect_Utilities util = new TechConnect_Utilities();
                    bool sendText = util.CheckSeverity(SeverityDropDownList);
                    if (sendText)
                    {
                        StringBuilder textBody = createBody("text");
                        sendTextMsg(textBody);

                    }

                    UpdatePanelSubmitButton.Update();
                    util.CleanUploadsFolder();

                    ModalPopupLabel1.Text = "Thank you for your submission!";
                    ModalPopupLabel2.Text = "A ticket will be created and sent to your email address. ";
                    ModalPopupLabel3.Text = "The ticket generator will be ‘Service Desk Plus Ticketing System’.";
                    if (sendText)
                        ModalPopupLabel4.Text = "A text will also be sent to a staff member for immediate assistance.";

                    TextBox8.Text = "Exit";
                    UpdatePanelTitle.Update();
                    UpdatePanel1Modalpopup.Update();
                    mp1.Show();
                }
                else
                {
                    ModalPopupLabel1.Text = "Error:";
                    ModalPopupLabel2.Text = errorMsg;
                    ModalPopupLabel3.Text = "";
                    ModalPopupLabel4.Text = "";
                    TextBox8.Text = "Stay";
                    UpdatePanelTitle.Update();
                    UpdatePanel1Modalpopup.Update();
                    mp1.Show();
                }
            }
        private void sendEmail(StringBuilder emailBody, StringBuilder emailSubject)
        {
            String userEmailAddress = Session["s_UserEmail"].ToString();
            String supportEmailAddress = Session["s_SupportEmail"].ToString();
            String supportEmailPassword = Session["s_SupportPswd"].ToString();
            String sendGridUserName = Session["s_SendGridUserName"].ToString();
            String sendGridPassword = Session["s_SendGridPassword"].ToString();
            String ticketEmailAddress = Session["s_TicketingEmail"].ToString();

            //String ticketEmailAddress = "tbairok@gmail.com";

            using (MailMessage message = new MailMessage(supportEmailAddress, ticketEmailAddress, emailSubject.ToString(), emailBody.ToString()))
            {
                AddAttachments(message);
                message.ReplyToList.Add(userEmailAddress);
                SmtpClient emailClient = new SmtpClient("smtp.sendgrid.net", 587);
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential(sendGridUserName, sendGridPassword);
                emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                emailClient.EnableSsl = false;

                try
                {
                    emailClient.Send(message);
                }
                catch (SmtpFailedRecipientException ex)
                {

                }
            }
        }

        private void sendTextMsg(StringBuilder textBody)
        {
            var accountSid = Session["s_TwilioAcctID"].ToString();
            var authToken = Session["s_TwilioAuthToken"].ToString();
            var fromPhoneNum = Session["s_TwilioFromPhoneNum"].ToString();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            TwilioClient.Init(accountSid, authToken);
            List<string> TextNums = new List<string>();

            string textnum1 = Session["s_TwilioFromPhoneNum1"].ToString();
            if (textnum1 != "")
                TextNums.Add(textnum1);
            string textnum2 = Session["s_TwilioFromPhoneNum2"].ToString();
            if (textnum2 != "")
                TextNums.Add(textnum2);
            string textnum3 = Session["s_TwilioFromPhoneNum3"].ToString();
            if (textnum3 != "")
                TextNums.Add(textnum3);
            string textnum4 = Session["s_TwilioFromPhoneNum4"].ToString();
            if (textnum4 != "")
                TextNums.Add(textnum4);

            foreach (var textnum in TextNums)
            {
                var messageOptions = new CreateMessageOptions(
                new PhoneNumber(textnum));

                messageOptions.From = new PhoneNumber(fromPhoneNum);
                messageOptions.Body = textBody.ToString();

                var message = MessageResource.Create(messageOptions);
                Console.WriteLine(message.Body);
            }
        }

        private StringBuilder createSubject(string messagetype)
        {
            string fieldsvcOption = FieldSvcOptions.SelectedItem.Text;
            string severityLable = LabelSeverity.Text;
            string sLevel = "";

            String sColText = "";
            if (SeverityDropDownList.Items.Count > 0)
            {
                sColText = SeverityDropDownList.SelectedItem.Text;
            }
            int colonIndex = 0;
            colonIndex = sColText.IndexOf(":");
            sLevel = sColText.Substring(0, colonIndex);

            StringBuilder textSubject = new StringBuilder();
            textSubject.Append("TECH CONNECT - Field Service Option: ");
            textSubject.Append(fieldsvcOption + "; ");
            textSubject.Append("Severity: " + sLevel);

            return textSubject;
        }
        private void AddAttachments(MailMessage message)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            string uploadPath = util.GetUploadPath();
            var folderPath = HttpContext.Current.Server.MapPath(uploadPath);

            bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadupPathExists)
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    Attachment attachment = new Attachment(file);
                    message.Attachments.Add(attachment);
                }
            }
        }
        private string validateTextBoxes()
        {
            string errMsg = "";
            if (errMsg == "")
            {
                if (AdditionalCommentsTextBox.Text.Length > 0)
                {
                    string commentString = AdditionalCommentsTextBox.Text;
                    Regex r = new Regex("^[a-zA-Z0-9-@':&,.!?/_$ ]*$");
                    if (!r.IsMatch(commentString))
                    {
                        char[] commentsArray = commentString.ToCharArray();
                        for (int i = 0; i < commentsArray.Length; i++)
                        {
                            string commentsChar = commentsArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character.";
                                i = commentsArray.Length;
                            }
                        }  
                    }
                }
                else
                    errMsg = "Please enter 'Comments' before submitting ticket.";
            }
            return errMsg;
        }

        private StringBuilder createBody(string messagetype)
        {
            string userEmail = Session["s_UserEmail"] as string;
            int endPos = userEmail.IndexOf("@");
            string currentUser = userEmail.Substring(0, endPos);

            string addComentsLable = LabelAddComments.Text;
            string addComentsText = "";
            if (AdditionalCommentsTextBox.Text.Length > 0) addComentsText = AdditionalCommentsTextBox.Text;

            string severityLable = LabelSeverity.Text;
            string sColText = "";
            if (SeverityDropDownList.Items.Count > 0) sColText = SeverityDropDownList.SelectedItem.Text;

            StringBuilder emailBody = new StringBuilder();
            string fieldSvcOption = FieldSvcOptions.SelectedItem.Text;

            string bColLabel = "";
            string bColText = "";
 
            if (OptionBListBox.Items.Count > 0) { if (OptionBListBox.SelectedIndex > 0) { bColLabel = LabelBCol.Text; bColText = ColBOSelectButton.Text; } }

            if (messagetype == "email")
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("");

                emailBody.AppendLine("Requester's Name: " + "\t" + "\t" + currentUser);
                emailBody.AppendLine("Requester's Email: " + "\t" + "\t" + userEmail);

                emailBody.AppendLine("");
                emailBody.AppendLine("Field Service Option: " + "\t" + "\t" + fieldSvcOption);

                if (bColText.Length > 0)
                    emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "\t" + bColText);

                emailBody.AppendLine("Comments: " + "\t" + "\t" + "\t" + addComentsText);
                emailBody.AppendLine("Severity: " + "\t" + "\t" + "\t" + sColText);
            }
            else
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("Severity: Immediate");
                emailBody.AppendLine("");
                emailBody.AppendLine("Requester's Name: " + currentUser);
                emailBody.AppendLine("Requester's Email: " + userEmail);
                emailBody.AppendLine("Field Service Option: " + fieldSvcOption);
                if (bColText.Length > 0) emailBody.AppendLine(bColLabel + ": " + bColText);
                emailBody.AppendLine("Comments: " + addComentsText);
            }

            return emailBody;
        }
        protected void AFButton_Click(object sender, EventArgs e)
        {
            LabelMaxFiles.Text = String.Empty;
            UpdatePanelAFButton.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();

            if (FileUploadDiv.Visible == false)
            {
                ListBoxFileUpload.Items.Clear();
                FileUploadDiv.Visible = true;
            }
            else
            {
                FileUploadDiv.Visible = false;
                UpdatePanelFileUpload.Update();
            }

            UpdatePanelFileUpload.Update();
        }
        protected void uploadButton_Click(object sender, EventArgs e)
        {
            var postedFile = FileUpload.PostedFile;
            var newAttachment = FileUpload.PostedFile.FileName.ToString();

            TechConnect_Utilities util = new TechConnect_Utilities();
            bool canUpload = util.CheckUploadFiles(postedFile, ListBoxFileUpload, LabelMaxFiles, FileUpload, UpdatePanelFileUpload);
            if (canUpload)
            {
                ListBoxFileUpload.Items.Add(new ListItem(newAttachment, newAttachment));
                ListBoxFileUpload.SelectedIndex = -1;
                string uploadPath = util.GetUploadPath();

                bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
                if (!userUploadupPathExists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(uploadPath));

                FileUpload.PostedFile.SaveAs(Server.MapPath(uploadPath + newAttachment));
            }

            UpdatePanelFileUpload.Update();
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            int selectedFileIndex = ListBoxFileUpload.SelectedIndex;
            if (selectedFileIndex == -1) LabelMaxFiles.Text = "Please select file to delete.";
            else
            {
                LabelMaxFiles.Text = "";
                string fileName = ListBoxFileUpload.SelectedValue;
                ListBoxFileUpload.Items.RemoveAt(selectedFileIndex);

                string FileToDelete;
                TechConnect_Utilities util = new TechConnect_Utilities();
                string uploadPath = util.GetUploadPath();

                FileToDelete = Server.MapPath(uploadPath + fileName);
                File.Delete(FileToDelete);
            }

            UpdatePanelFileUpload.Update();
        }

        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            ListBoxFileUpload.Items.Clear();
            UpdatePanelFileUpload.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }

        protected void HomeButton_Click(object sender, EventArgs e)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.DeleteUserUploadFolder();

            var TechConnectHomeURL = Session["DefaultUrl"].ToString();
            Response.Redirect(TechConnectHomeURL, false);
        }

    }
}
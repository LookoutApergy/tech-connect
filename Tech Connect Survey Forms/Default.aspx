﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Tech_Connect_Survey_Forms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server"  >

    <div>
        <div id="TechConnectOptionsDiv" style="padding-top:5%;">    
            <div  id="Div1" runat="server" style=" display:grid; grid-template-columns:33% 33% 33% ; grid-auto-rows:50px; ">  

                <asp:UpdatePanel ID="UpdatePanelAdminButton" runat="server" UpdateMode="Conditional" >
                     <ContentTemplate>
                        <div style="grid-column:1; grid-row:1;  position:relative;  ">                      
                            <div id="AdminButtonDiv" runat="server" style=" display:grid; grid-template-columns:34% 24% 42% ; grid-auto-rows:50px; ">    
                                <div style="grid-column:2; grid-row:1; text-align:center;  ">                             
                                    <asp:ImageButton ID="AdminButton" runat="server" ImageUrl="~/Images/Admin Portal Button.png" style="height:70px; width:90%;  border-radius:5px; border-color: #00857D;" OnClick="AdminButton_Click" />                    
                                </div>
                                <div style="grid-column:3; grid-row:1; padding-top: 25px; ">   
                                    <asp:Label ID="AdminLabel" runat="server" Text="Admin Portal" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; padding-left:5px; "></asp:Label>
                                </div>   
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div style="grid-column:3; grid-row:1;float:right; position:relative; right:0">
                    <asp:Button ID="UserGuide" runat="server"  Text="TechConnect Users Guide" style="height:40px; width:52%; position:absolute; right:0px; font-family: 'Segoe UI'; background-color: #003E51; color:white;  border-radius:5px; font-size:115%; border-width:medium; padding-bottom:2px; border-color: #00857D; border-bottom:none" onClientClick="window.open('/TechConnect Users Guide/TechConnect Guide.pdf','target=_blank');"  />
                </div>
            </div>

            <div  id="labelDiv" runat="server" style=" display:grid; grid-template-columns:33% 33% 33% ; grid-auto-rows:100px; padding-top:7%;">  
                <div style="grid-column:1; grid-row:1;   position:relative; ">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/DHE.jpg" style=" max-width:25%; position:absolute; left:34%" OnClick="ImageButton1_Click"/>                  
                    <asp:Label ID="Label1" runat="server" Text="Downhole Equipment" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%; "></asp:Label>
                </div>
                <div style="grid-column:2; grid-row:1; position:relative; ">
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/Surface.jpg" style=" max-width:27%; position:absolute; left:33%"  OnClick="ImageButton2_Click"/>
                    <asp:Label ID="Label2" runat="server" Text="Surface" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%; "></asp:Label>
                </div>
                <div style="grid-column:3; grid-row:1;  position:relative; ">
                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/Applications.jpg" style=" max-width:25%; position:absolute; left:34%" OnClick="ImageButton3_Click" />
                    <asp:Label ID="Label3" runat="server" Text="Application" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%;"></asp:Label>
                </div>
                <div style="grid-column:1; grid-row:2;  position:relative; ">      
                    <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/Service Center Resized.png" style=" max-width:27%; position:absolute; left:33%" OnClick="ImageButton4_Click"/>                    
                    <asp:Label ID="Label4" runat="server" Text="Service Center" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%;"></asp:Label>
                </div>
                <div style="grid-column:2; grid-row:2;  position:relative; ">
                    <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/Field Service Revised.png" style=" max-width:25%; position:absolute; left:34%" OnClick="ImageButton5_Click"/>
                    <asp:Label ID="Label5" runat="server" Text="Field Service" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%;"></asp:Label>  
                    </div>
                <div style="grid-column:3; grid-row:2;  position:relative; ">
                    <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/Engineering Revised.png" style=" max-width:27%; position:absolute; left:32%" OnClick="ImageButton6_Click"/>
                    <asp:Label ID="Label6" runat="server" Text="Engineering" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%;"></asp:Label>
                </div>
                <div style="grid-column:1; grid-row:3;  position:relative;">
                    <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/Third Party Vendors Revised.png" style=" max-width:25%; position:absolute; left:34%" OnClick="ImageButton7_Click"/>
                    <asp:Label ID="Label7" runat="server" Text="Vendor-3rd Party" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%;"></asp:Label>
                    </div>
                <div style="grid-column:2; grid-row:3; position:relative;">
                    <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/Other Revised.png" style=" max-width:27%; position:absolute; left:33%" OnClick="ImageButton8_Click"/>
                    <asp:Label ID="Label8" runat="server" Text="Other" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%;"></asp:Label>
                </div>
                <div style="grid-column:3; grid-row:3;  position:relative; ">
                    <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/Ideas and Suggestions Revised.png" style=" max-width:25%; position:absolute; left:33%; "  OnClick="ImageButton9_Click"/>
                    <asp:Label ID="Label9" runat="server" Text="Ideas & Suggestions" Style="font-family:'Segoe UI'; color:#003E51; font-size:115%; position:absolute; left:60%; bottom:30%;"></asp:Label> 
                </div>
                <div style="grid-column:2; grid-row:5;position:relative;  text-align:center" >
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/TechConnect Icon with space.png"  width="70%" Height="50%"/>                       
                </div>
 
            </div>
        </div> 
    </div>

    <script type="text/javascript"  >

        $(document).ready(function () {

        });

     </script>

</asp:Content>


  

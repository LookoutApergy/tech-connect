﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DHE.aspx.cs" Inherits="Tech_Connect_Survey_Forms.DHE" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
    <link rel="stylesheet" href="css/modalanimate.css"/>
    <link rel="stylesheet" type="text/css" id="TCStyles" href="TechConnect Computer Styles.css" />
    <script type="text/javascript" src="~/Scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="MainDiv" runat="server"  class="MainDivStyle">    
            
            <div id="BannerDiv" style="height:150px; position:relative">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/New UNB Logo Banner.png" Height="100%" ImageAlign="Left" Width="98%"  />
                <asp:Label ID="Label2" runat="server" Text="TechConnect" CssClass="TechConnectLabelStyle"></asp:Label>
                <div style="position:absolute; left:90%; bottom:10%; z-index:100; "> 
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Home.png" Style="height:35px" onclick="clickHome()" />
                    <asp:Button ID="HomeButton" runat="server" style="visibility:hidden" OnClick="HomeButton_Click"/>
                </div>
            </div> 
            <div id="LabelMainDiv" class="LabelMainDivStyle">
                <asp:Label ID="LabelDEO" runat="server" Text="Downhole Equipment Options" CssClass="LabelMainStyle"></asp:Label>
            </div>

            <asp:UpdatePanel ID="UpdatePanelTitle" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>
                    <div id="headerDiv" class="headerDivStyle">
                        <asp:TextBox ID="CollapseColDE" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                        <asp:TextBox ID="TextBoxOption" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                        <asp:TextBox ID="TextBox2" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                        <asp:TextBox ID="TextBox3" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                        <asp:TextBox ID="TextBox4" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                        <asp:TextBox ID="TextBox7" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                        <asp:TextBox ID="TextBox8" runat="server" CssClass="HiddenTextStyle"></asp:TextBox>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>  
            
            <div  id="labelDiv" runat="server" class="LabelDivStyle">              
                <asp:Label ID="LabelOptionCol" runat="server" Text="Options" CssClass="LabelColumnStyle" ></asp:Label>
                <div class="UpdatePanelPadding">
                    <asp:UpdatePanel ID="UpdatePanelLabelColB" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                            <asp:Label ID="LabelBCol" runat="server" Text="" Cssclass="LabelColumnStyle" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="UpdatePanelPadding">
                    <asp:UpdatePanel ID="UpdatePanelLabelColC" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                            <script src="Scripts/jquery-3.3.1.intellisense.js"></script>
                            <asp:Label ID="LabelCCol" runat="server" Text="" CssClass="LabelColumnStyle" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="UpdatePanelLabelColDE" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <div runat="server" id="borderDiv" class="borderDivStyle">
                            <div id="voltsAmpsDiv" runat="server" class="voltsAmpsDivStyle">
                                <div id="VoltsAmpsLabels" runat="server" class="VoltsAmpsLabelsDivStyle"> 
                                    <asp:Label ID="LabelVolts" runat="server" Text="Volts" CssClass="LabelVoltsStyle" ></asp:Label>
                                    <asp:Label ID="LabelAmps" runat="server" Text="Amps" CssClass="LabelAmpsStyle" ></asp:Label>
                                </div>                        
                                <asp:RadioButtonList ID="VoltsAmpsToggle" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="VoltsAmpsToggle_SelectedIndexChanged" CssClass="VoltsAmpsToggleStyle" CellPadding="0" CellSpacing="0">
                                    <asp:ListItem Value="Volts" > Volts</asp:ListItem>
                                    <asp:ListItem Value="Amps" > Amps</asp:ListItem>
                                </asp:RadioButtonList>                
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="UpdatePanelPadding">
                    <asp:UpdatePanel ID="UpdatePanelLabelColF" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                            <asp:Label ID="LabelFCol" runat="server" Text="" CssClass="LabelColumnFStyle" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="UpdatePanelPadding">
                    <asp:UpdatePanel ID="UpdatePanelLabelColG" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="LabelGCol" class="LabelGColExpanded" runat="server" Text=""></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="UpdatePanelPadding">
                    <asp:UpdatePanel ID="UpdatePanelLabelColH" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="LabelHCol" class="LabelHColExpanded" runat="server" Text=""></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="DDGrid" runat="server" class="DropdownDivStyle">
                <div class="OptionsDivStyle">
                    <asp:DropDownList ID="DEOOptions" runat="server" AutoPostBack="True" CssClass="DropdownColumnStyle" OnSelectedIndexChanged="DEOOptions_SelectedIndexChanged" >
                        <asp:ListItem>  Select Equipment...</asp:ListItem>
                        <asp:ListItem>Pump</asp:ListItem>
                        <asp:ListItem>Gas Handling</asp:ListItem>
                        <asp:ListItem>Intake</asp:ListItem>
                        <asp:ListItem>Gas Separator</asp:ListItem>
                        <asp:ListItem>Protector</asp:ListItem>
                        <asp:ListItem>Motor</asp:ListItem>
                        <asp:ListItem>Sensor</asp:ListItem>
                        <asp:ListItem>MLE</asp:ListItem>
                        <asp:ListItem>Cable</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:UpdatePanel ID="UpdatePanelDDColB" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <div id="DEOOptionsB_Div" class="OptionBDropdownDivStyle">
                            <asp:DropDownList ID="OptionBDropdown" runat="server" CssClass="DropdownColumnStyleOverflow" OnSelectedIndexChanged="OptionBDropdown_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem>Blank</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="SizeText" runat="server" CssClass="SizeTextStyle" Enabled="false"></asp:TextBox>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
<%--                DropDown C--%>
                <asp:UpdatePanel ID="UpdatePanelDDColC" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <div id="DEOOptionsC_Div" runat="server" class="DEOOptionsC_DivStyle">
                            <asp:Button ID="ColCOSelectButton" runat="server" AutoPostBack="true" Text="" CssClass="ColCOptionSelectButtonStyle" OnClick="ColCOptionSelectButton_Click" BorderStyle="Solid" />
                            <asp:ListBox ID="OptionCListBox" runat="server" AutoPostBack="true" CssClass="OptionCListBoxStyle" OnSelectedIndexChanged="OptionCListBox_SelectedIndexChanged">
                            </asp:ListBox>
                            <asp:DropDownList ID="OptionCDropdown" runat="server" CssClass="OptionCDropdownHidden" AutoPostBack="True" OnSelectedIndexChanged="OptionCDropdown_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanelDDColDE" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <div id="DEOOptionsD_Div" runat="server" class="VoltsandAmpsDropdownDivStyle">
                            <div class="VoltsAmpsDropdownDivStyle" >
                                <div id="DEOOptionsD1_Div" class="VoltsDropdownDivStyle">
                                    <asp:TextBox ID="VoltsTextBox" runat="server" CssClass="VoltsAmpsTextBoxStyle" Enabled="false" ></asp:TextBox>
                                    <asp:DropDownList ID="OptionDDropdown" runat="server" CssClass="OptionDandEDropdownStyle" AutoPostBack="True" OnSelectedIndexChanged="OptionDDropdown_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                                <div id="DEOOptionsD2_Div" class="AmpsDropdownDivStyle">
                                    <asp:TextBox ID="AmpsTextBox" runat="server" CssClass="VoltsAmpsTextBoxStyle" Enabled="false"></asp:TextBox>
                                    <asp:DropDownList ID="OptionEDropdown" runat="server" CssClass="OptionDandEDropdownStyle" AutoPostBack="True" OnSelectedIndexChanged="OptionEDropdown_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanelDDColF" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="DEOOptionsF_Div" runat="server" class="DEOOptionsF_DivStyle">
                            <asp:DropDownList ID="OptionFDropdown" runat="server" CssClass="OptionFDropDownStyle" AutoPostBack="True" OnSelectedIndexChanged="OptionFDropdown_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:TextBox ID="TypeText" runat="server" CssClass="TypeTextStyle" Enabled="false"></asp:TextBox>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanelDDColG" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                         <div id="DEOOptionsG_Div" runat="server" class="OptionGDropdownDivStyle">  
                             <div class="DivPaddingLeft">
                                <asp:DropDownList ID="OptionGDropdown" runat="server" CssClass="OptionGDropdownStyle" OnSelectedIndexChanged="OptionGDropdown_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:TextBox ID="SeriesText" runat="server" CssClass="SeriesTextStyle" Enabled="false"></asp:TextBox>         
                            </div>
                            <asp:Button ID="ColGOSelectButton" CssClass="ColG_OptSelButtonBS" runat ="server" AutoPostBack="true" Text="" BorderStyle="Solid" OnClick="ColGOSelectButton_Click" />
                            <asp:ListBox ID="OptionGListBox" runat="server" AutoPostBack="true" CssClass="OptionGListBoxStyle" OnSelectedIndexChanged="OptionGListBox_SelectedIndexChanged" >
                            </asp:ListBox>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanelDDColH" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                         <div id="DEOOptionsH_Div" class="OptionHDropdownDivStyle">  
                             <div class="DivPaddingLeft">
                                <asp:DropDownList ID="OptionHDropdown" runat="server" CssClass="OptionHDropdownStyle" OnSelectedIndexChanged="OptionHDropdown_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:TextBox ID="TandemText" runat="server" CssClass="TandemTextStyle" Enabled="false"></asp:TextBox>
                            </div>
                             <asp:TextBox ID="StagesText" runat="server" AutoPostBack="True" CssClass="StagesTextStyle" Enabled="true" MaxLength="3" ToolTip="Please enter a number between 1 and 600" onkeyup="integersOnly(this)" ></asp:TextBox>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="AdditionalCommentsDiv" runat="server" >
                <div runat="server" class="AdditionalCommentsDivStyle">
                    <div class="LabelAddCommentsDivStyle">
                        <asp:Label ID="LabelAsterisk" runat="server" Text="*" CssClass="LabelAsteriskStyle"></asp:Label>
                        <asp:Label ID="LabelAddComments" runat="server" Text="Comments:" CssClass="LabelAddCommentsStyle"></asp:Label>
                    </div>                            
                    <div class="LabelSeverityDivStyle">
                        <asp:Label ID="LabelSeverity" runat="server" Text="Severity:" CssClass="LabelSeverityStyle"></asp:Label>
                    </div>
                </div>                           
                <div  id="Div1" runat="server" class="UpdatePanelAddCommentsDiv1Style">
                    <div class="UpdatePanelAddCommentsDiv2Style">
                    <asp:UpdatePanel ID="UpdatePanelAddComments" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="AdditionalCommentsTextBox" runat="server" AutoPostBack="True" CssClass="AdditionalCommentsTextBoxStyle" TextMode="MultiLine"  ></asp:TextBox>                              
                        </ContentTemplate>
                    </asp:UpdatePanel>                                                  
                    </div>
                    <div class="SeverityDropDownListDiv1Style">
                        <div runat="server" class="SeverityDropDownListDiv2Style">
                            <div class="SeverityDropDownListDiv3Style">
                                <asp:DropDownList ID="SeverityDropDownList" runat="server" AutoPostBack="True" CssClass="SeverityDropDownListStyle">
                                    <asp:ListItem>Low: 10-14 business days</asp:ListItem>
                                    <asp:ListItem>Medium: 3-5 business days</asp:ListItem>
                                    <asp:ListItem>High: 24-48 hours</asp:ListItem>
                                    <asp:ListItem>Immediate: Send text</asp:ListItem>
                                </asp:DropDownList>   
                            </div>
                        </div>                               
                    </div>
                    <div class="UpdatePanelSubmitButtonDivStyle">
                        <asp:UpdatePanel ID="UpdatePanelSubmitButton" runat="server" UpdateMode="Conditional" >
                            <ContentTemplate>
                                    <asp:Button ID="ButtonSubmit" runat="server" AutoPostBack="true" Text="Submit ticket" CssClass="ButtonSubmitStyle" OnClick="ButtonSubmit_Click" />                           
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanelAFButton" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="AFButtonDiv" runat="server" >
                        <div  id="Div6" runat="server" class="AFButtonDiv1Style">
                            <div class="AFButtonDiv2Style">   
                                <asp:Button ID="AFButton" runat="server" Text="Attach Files" CssClass="AFButtonStyle" OnClick="AFButton_Click" Visible="True"   />
                            </div>  
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanelFileUpload" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>
                    <div id="FileUploadDiv" runat="server"  >
                        <div  id="Div2" runat="server" class="FileUploadControlDiv1Style">
                            <div class="FileUploadControlDiv2Style" >   
                                <div class="FileUploadControlDiv3Style">
                                    <asp:FileUpload ID="FileUpload" runat="server" CssClass="FileUploadStyle" AllowMultiple="true" onchange="AddFileToList(this)" />
                                </div>                                                              
                            </div>                                
                            <div class="ListBoxFileUploadDivStyle" >
                                <asp:ListBox ID="ListBoxFileUpload" runat="server" CssClass="ListBoxFileUploadStyle" >
                                </asp:ListBox>
                            </div>                                  
                            <div class="UploadOptionsDiv1Style" >   
                                <div  id="Div5" runat="server" class="UploadOptionsDiv2Style">
                                    <div class="LabelMaxFilesDivStyle"> 
                                        <asp:Label ID="LabelMaxFiles" runat="server" Text="" CssClass="LabelMaxFilesStyle"></asp:Label>
                                    </div>
                                    <div class="ButtonDeleteDivStyle" > 
                                        <asp:Button ID="ButtonDelete" runat="server" Text="Delete" CssClass="ButtonDeleteClearStyle" OnClick="ButtonDelete_Click"  />
                                    </div>
                                    <div class="ButtonClearDivStyle"> 
                                        <asp:Button ID="ButtonClear" runat="server" Text="Clear" CssClass="ButtonDeleteClearStyle" OnClick="ButtonClear_Click" />
                                    </div>
                                </div>
                            </div>                            
                        </div>      
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UpdatePanel1Modalpopup" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>                  
                    <asp:Button ID="btnShow" runat="server" Text="Show Modal Popup" Style="visibility:hidden" />
                    <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="PanelModalPopup" TargetControlID="btnShow"
                        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="PanelModalPopup" runat="server" UpdateMode="Always" CssClass="modalPopup" align="left" style = "display:none; " >
                        <div style="position:relative">
                            <div class="ModalPopupBannerDivStyle">
                                <asp:Label ID="ModalPopupLabel1" runat="server" Text="Error:" CssClass="ModalPopupBannerStyle" ></asp:Label>
                            </div>
                            <div class="ModalPopupLabelDivStyle1">
                                <asp:Label ID="ModalPopupLabel2" runat="server" Text="" CssClass="ModalPopupLabelStyle" ></asp:Label>
                            </div>
                            <div class="ModalPopupLabelDivStyle2">
                                <asp:Label ID="ModalPopupLabel3" runat="server" Text="" CssClass="ModalPopupLabelStyle"  ></asp:Label>
                            </div>
                            <div class="ModalPopupLabelDivStyle2">
                                <asp:Label ID="ModalPopupLabel4" runat="server" Text="" CssClass="ModalPopupLabelStyle"  ></asp:Label>
                            </div>
                       </div>
                        <div class="ModalPopupBtnCloseDivStyle">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ModalPopupBtnCloseStyle" OnClientClick="return modalClose()"  />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div>
                <asp:Button ID="uploadButton" runat="server" Text="Button"  style="visibility:hidden" OnClick="uploadButton_Click"/>
                <asp:TextBox ID="TextBox5" runat="server"  CssClass="HiddenTextStyle"></asp:TextBox>
                <asp:TextBox ID="TextBox6" runat="server"  CssClass="HiddenTextStyle"></asp:TextBox>
                <input type="hidden" id="pageHeight" runat="server" value="" />
                <input type="hidden" id="pageWidth" runat="server" value="" />
            </div>

            <div class="TechConnectImageDivStyle">
                <asp:Image ID="TechConnectImage" runat="server" ImageUrl="~/Images/TechConnect Icon with space modified.png"  width="15%" Height="95%"  />
            </div>
        </div>

    </form>
        <script type="text/javascript" >

            $('#PanelModalPopup').on('hidden.bs.modal', function () {
                document.getElementById('TextBox5').value = "it is hidden";
            })

            function AddFileToList(fileUpload)
            {
                var myButton = document.getElementById('uploadButton');
                myButton.click();
            }

            function clickHome() {
                var homeButton = document.getElementById('HomeButton');
                homeButton.click();
            }

            function resetDDCss() {
                var OptCDD = document.getElementById('OptionCDropdown');
            }

            function integersOnly(obj) {
                obj.value = obj.value.replace(/[^0-9]/g, '');
            }

            function modalClose() {
                document.getElementById('TextBox5').value = "it is hidden";
                var popupMsg = document.getElementById('TextBox8').value;
                if (popupMsg == "Exit")
                    var homeButton = document.getElementById('HomeButton');
                    homeButton.click();
            }

    </script>
</body>
</html>





﻿using Microsoft.Ajax.Utilities;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Net;
using Twilio.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Tech_Connect_Survey_Forms.App_Code;

namespace Tech_Connect_Survey_Forms
{
    public partial class SurfaceMobile : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string userEmail = Session["s_UserEmail"] as string;

            if (!IsPostBack)
            {
                ClearColumnB();
                ClearColumnC();
                ClearColumnD();
                ClearColumnsE();
                hideAdditionalCommentsDiv();
                hideAttachFilesButton();
                hideFileUploadPanel();
                UpdatePanelTitle.Update();
            }
        }

        private void ClearColumnB()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "";
            labelB.Visible = false;

            OptionBDDList.Items.Clear();
            OptionBDDList.Visible = false;

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }
        private void ClearColumnC()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = String.Empty;
            labelC.Visible = false;
            OptionCDDList.Items.Clear();
            OptionCDDList.Visible = false;

            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "#f8f9fa");
            CompetitorNameText.Visible = false;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void hideFileUploadPanel()
        {
            LabelMaxFiles.Text = string.Empty;
            FileUploadDiv.Visible = false;
            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        private void showFileUploadPanel()
        {
            FileUploadDiv.Visible = true;
            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        private void hideAttachFilesButton()
        {
            AFButtonDiv.Visible = false;
            UpdatePanelAFButton.Update();
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }

        private void showAttachFilesButton()
        {
            AFButtonDiv.Visible = true;
            UpdatePanelAFButton.Update();
        }

        private void clearColumns()
        {
            ClearColumnB();         
            ClearColumnC();
            ClearColumnD();
            ClearColumnsE();
            hideAdditionalCommentsDiv();
            hideAttachFilesButton();
            hideFileUploadPanel();
        }

        private void showAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = true;
            showAttachFilesButton();
        }
        private void showColumnBDiv()
        {
            OptionBDDList.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void hideAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = false;
            AdditionalCommentsTextBox.Text = string.Empty;
            UpdatePanelAddComments.Update();
        }

        /***** Column A/DEOOptions Methods and Events *****/
        protected void SurfaceOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SurfaceOption = SurfaceOptions.SelectedValue.ToString();
            TextBoxOption.Text = SurfaceOption;
            UpdatePanelTitle.Update();
            ClearColumnB();
            ClearColumnC();
            ClearColumnD();
            ClearColumnsE();

            switch (SurfaceOption)
            {
                case "VSD":
                    LoadColB_SelectionButton("VSD Option", "Select Brand");
                    LoadColBDD_VSDBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "Transformers":
                    LoadColB_SelectionButton("Trans Option", "Select Option");
                    LoadColBDD_TransformersBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "SWB":
                    LoadColB_SelectionButton("Switchboard", "Select Option");
                    LoadColBDD_SWBBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "SWB Accessories":
                    LoadColB_SelectionButton("Switchboard Accs", "Select Option");
                    LoadColBDD_SWBAccessoriesBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "Sensor Surface Equipment":
                    LoadColB_SelectionButton("Sensor Surf Equip", "Select Option");
                    LoadColBDD_SensorSurfaceEquipOptions();
                    showAdditionalCommentsDiv();
                    break;
                case "Other":
                    showAdditionalCommentsDiv();
                    break;
                default:
                    clearColumns();
                    break;
            }
        }

        /*  Column B Methods  */
        private void RefreshColB()
        {
            OptionBDDList.Items[0].Selected = true;
            OptionBDDList.Visible = true;
            OptionBDDList.Focus();

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_SelectionButton(string labeltext, string selectiontext)
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = labeltext;

            labelB.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColBDD_VSDBrands()
        {
            OptionBDDList.Items.Clear();
            OptionBDDList.Items.Add(new ListItem("Select Brand", "Select Brand"));
            OptionBDDList.Items.Add(new ListItem("Smarten", "Smarten"));
            OptionBDDList.Items.Add(new ListItem("Spoc", "Spoc"));
            OptionBDDList.Items.Add(new ListItem("Competitor", "Competitor"));
            OptionBDDList.Visible = true;

            RefreshColB();
        }

        private void LoadColBDD_SWBAccessoriesBrands()
        {
            OptionBDDList.Items.Clear();
            OptionBDDList.Items.Add(new ListItem("Select Brand", "Select Brand"));
            OptionBDDList.Items.Add(new ListItem("ChampionX", "ChampionX"));
            OptionBDDList.Items.Add(new ListItem("Other", "Other"));
            OptionBDDList.Visible = true;

            RefreshColB();
        }

        private void LoadColBDD_SensorSurfaceEquipOptions()
        {
            OptionBDDList.Items.Clear();
            OptionBDDList.Items.Add(new ListItem("Select Option", "Select Option"));
            OptionBDDList.Items.Add(new ListItem("Read Out", "Read Out"));
            OptionBDDList.Items.Add(new ListItem("Choke", "Choke"));
            OptionBDDList.Visible = true;

            RefreshColB();
        }

        private void LoadColBDD_SWBBrands()
        {
            OptionBDDList.Items.Clear();
            OptionBDDList.Items.Add(new ListItem("Select Brand", "Select Brand"));
            OptionBDDList.Items.Add(new ListItem("ChampionX", "ChampionX"));
            OptionBDDList.Items.Add(new ListItem("Other", "Other"));
            OptionBDDList.Visible = true;

            RefreshColB();
        }

        private void LoadColBDD_TransformersBrands()
        {
            OptionBDDList.Items.Clear();
            OptionBDDList.Items.Add(new ListItem("Select Brand", "Select Brand"));
            OptionBDDList.Items.Add(new ListItem("Southwest", "Southwest"));
            OptionBDDList.Items.Add(new ListItem("Neco", "Neco"));
            OptionBDDList.Items.Add(new ListItem("Other", "Other"));
            OptionBDDList.Visible = true;

            RefreshColB();
        }

        protected void OptionBDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SCOptionB = OptionBDDList.SelectedValue.ToString();

            UpdatePanelDDColC.Update();
            UpdatePanelLabelColC.Update();
            UpdatePanelTitle.Update();

            ClearColumnC();
            ClearColumnD();
            ClearColumnsE();
            string SurfaceOptionA = TextBoxOption.Text;
            string SurfaceOptionB = OptionBDDList.SelectedValue.ToString();

            UpdatePanelDDColB.Update();

            TextBox1.Text = SurfaceOptionB;
            UpdatePanelTitle.Update();
            TextBox4.Text = SurfaceOptionB;
            UpdatePanelTitle.Update();

            switch (SurfaceOptionA)
            {
                case "VSD":
                    switch (SurfaceOptionB)
                    {
                        case "Smarten":
                            LoadColCDD_SmartenOptions();
                            LoadColC_Label("Model Options", "Select Model");
                            break;
                        case "Spoc":
                            LoadColDDD_SpocPulseOptions();
                            LoadColD_Label("Pulse Options", "Select Pulse");
                            break;
                    case "Competitor":
                            LoadColC_CompetitorNameOption();
                            LoadColD_PulseTextBoxOption();
                            LoadColD_KVATextBox();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Transformers":
                    LoadColCDD_KVAOptions();
                    LoadColC_Label("KVA", "Pick KVA");
                    break;

                case "SWB":
                    switch (SurfaceOptionB)
                    {
                        case "ChampionX":
                            LoadColCDD_SWBVoltOptions();
                            LoadColC_Label("Volts", "Select Volts");
                            break;
                        case "Other":
                            LoadColC_OtherSWB();
                            break;
                        default:
                            break;
                    }
                    break;

                    case "SWB Accessories":
                        switch (SurfaceOptionB)
                        {
                            case "ChampionX":
                                LoadColCDD_ControllerOptions();
                                LoadColC_Label("Controller", "Select Controller");
                                break;
                            case "Other":
                                LoadColCDD_OtherController();
                                LoadColD_AccessoriesTextBoxOption();
                                break;
                            default:
                                break;
                        }
                        break;

                case "Sensor Surface Equipment":
                    SSE_PNTextbox();
                    break;
            }
        }

        /* Column C Methods  */

        private void RefreshColC()
        {
            OptionCDDList.Items[0].Selected = true;
            OptionCDDList.Visible = true;
            OptionCDDList.Focus();

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void LoadColC_Label(string labeltext, string selectiontext)
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = labeltext;

            labelC.Visible = true;
            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void LoadColC_CompetitorNameOption()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Competitor Name";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            OptionCDDList.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void LoadColC_OtherSWB()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "SWB";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            OptionCDDList.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }
 
        private void LoadColD_KVATextBox()
        {
            Label labelKVA = this.FindControl("LabelKVA") as Label;
            labelKVA.Visible = true;
            UpdatePanelLabelColE.Update();

            DEOOptionsE_Div.Attributes.CssStyle.Add("border-color", "transparent");

            KVATextBox.Text = String.Empty;
            KVATextBox.Visible = true;
            KVATextBox.Enabled = true;
            KVATextBox.Attributes.CssStyle.Add("background-color", "white");
            DEOOptionsE_Div.Visible = true;
            UpdatePanelDDColE.Update(); 
        }

        private void LoadColCDD_OtherController()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Controller";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            OptionCDDList.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }
        
        private void SSE_PNTextbox()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Part Number";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            OptionCDDList.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        protected void OptionCDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string surfaceOptionA = TextBoxOption.Text;
            string surfaceOptionB = TextBox1.Text;
            string surfaceOptionC = OptionCDDList.SelectedValue.ToString();
            TextBox2.Text = surfaceOptionC;
            UpdatePanelDDColC.Update();
            UpdatePanelTitle.Update();

            switch (surfaceOptionA)
            {
                case "VSD":
                    switch (surfaceOptionB)
                    {
                        case "Smarten":
                            switch (surfaceOptionC)
                            {
                                case "Smarten for Life":
                                LoadColDDD_SmartenPulseOption();
                                    LoadColE_SFL_Options();
                                    break;
                                default:
                                LoadColDDD_SmartenPulseOption();
                                    LoadColE_SLH_Options();
                                    break;
                            }
                            break;
                    }
                    break;
                   
                case "SWB Accessories":
                    switch (surfaceOptionB)
                    {
                        case "ChampionX":
                        LoadColDDD_AccessoriesOptions();
                            LoadColD_Label("Accessories", "Select Accessory");
                            break;
                    }
                    break;                      
            }
        }

        private void LoadColCDD_ControllerOptions()
        {
            OptionCDDList.Items.Clear();
            OptionCDDList.Items.Add(new ListItem("Select Controller", "Select Controller"));
            OptionCDDList.Items.Add(new ListItem("Icon Motor Sensor", "Icon Motor Sensor"));
            OptionCDDList.Items.Add(new ListItem("Icon Display Sensor", "Icon Display Sensor"));
            OptionCDDList.Visible = true;

            RefreshColC();
        }

        private void LoadColCDD_SmartenOptions()
        {
            OptionCDDList.Items.Clear();
            OptionCDDList.Items.Add(new ListItem("Select Model", "Select Model"));
            OptionCDDList.Items.Add(new ListItem("Standard", "Standard"));
            OptionCDDList.Items.Add(new ListItem("Low Harmonic", "Low Harmonic"));
            OptionCDDList.Items.Add(new ListItem("Smarten for Life", "Smarten for Life"));
            OptionCDDList.Visible = true;

            RefreshColC();
        }

        private void LoadColCDD_CompetitorOptions()
        {
            OptionCDDList.Items.Clear();
            OptionCDDList.Items.Add(new ListItem("Select Option", "Select Option"));
            OptionCDDList.Items.Add(new ListItem("Option 1", "Option 1"));
            OptionCDDList.Items.Add(new ListItem("Option 2", "Option 2"));

            RefreshColC();
        }
     
        private void LoadColCDD_SWBVoltOptions()
        {
            OptionCDDList.Items.Clear();
            OptionCDDList.Items.Add(new ListItem("Select SWB", "Select SWB"));
            OptionCDDList.Items.Add(new ListItem("600 volts", "600 volts"));
            OptionCDDList.Items.Add(new ListItem("1000 volts", "1000 volts"));
            OptionCDDList.Items.Add(new ListItem("1500 volts", "1500 volts"));
            OptionCDDList.Items.Add(new ListItem("2500 volts", "2500 volts"));
            OptionCDDList.Items.Add(new ListItem("3600 volts", "3600 volts"));
            OptionCDDList.Items.Add(new ListItem("5000 volts", "5000 volts"));

            RefreshColC();
        }

        private void LoadColCDD_KVAOptions()
        {
            OptionCDDList.Items.Clear();
            OptionCDDList.Items.Add(new ListItem("Pick KVA", "Pick KVA"));
            OptionCDDList.Items.Add(new ListItem("100", "100"));
            OptionCDDList.Items.Add(new ListItem("130", "130"));
            OptionCDDList.Items.Add(new ListItem("150", "150"));
            OptionCDDList.Items.Add(new ListItem("210", "210"));
            OptionCDDList.Items.Add(new ListItem("260", "260"));
            OptionCDDList.Items.Add(new ListItem("355", "355"));
            OptionCDDList.Items.Add(new ListItem("400", "400"));
            OptionCDDList.Items.Add(new ListItem("520", "520"));
            OptionCDDList.Items.Add(new ListItem("1000", "1000"));

            RefreshColC();
        }

        /*  Column D Methods   */  
        private void ClearColumnD()
        {
            Label labelD = this.FindControl("LabelDCol") as Label;
            labelD.Text = String.Empty;
            labelD.Visible = false;

            OptionDDDList.Items.Clear();
            OptionDDDList.Visible = false;

            PulseText.Text = String.Empty;
            PulseText.Visible = false;

            UpdatePanelLabelColD.Update();
            UpdatePanelDDColD.Update();
        }

        private void LoadColDDD_SmartenPulseOption()
        {
            Label labelD = this.FindControl("LabelDCol") as Label;
            labelD.Text = "Pulse";
            PulseText.Text = "6";
            PulseText.Visible = true;

            OptionDDDList.Visible = false;
            labelD.Visible = true;
            PulseText.Visible = true;
            PulseText.Enabled = false;

            UpdatePanelLabelColD.Update();
            UpdatePanelDDColD.Update();
        }

        private void LoadColD_PulseTextBoxOption()
        {
            Label labelD = this.FindControl("LabelDCol") as Label;
            labelD.Text = "Pulse";
            PulseText.Text = String.Empty;
            PulseText.Visible = true;
            PulseText.Enabled = true;

            OptionDDDList.Visible = false;
            labelD.Visible = true;
            PulseText.Visible = true;
            PulseText.Attributes.CssStyle.Add("background-color", "white");

            UpdatePanelLabelColD.Update();
            UpdatePanelDDColD.Update();
        }

        private void LoadColD_AccessoriesTextBoxOption()
        {
            Label labelD = this.FindControl("LabelDCol") as Label;
            labelD.Text = "Accessories";
            PulseText.Text = String.Empty;
            PulseText.Visible = true;
            PulseText.Enabled = true;

            OptionDDDList.Visible = false;
            labelD.Visible = true;
            PulseText.Visible = true;
            PulseText.Attributes.CssStyle.Add("background-color", "white");

            UpdatePanelLabelColD.Update();
            UpdatePanelDDColD.Update();
        }

        private void LoadColDDD_SpocPulseOptions()
        {
            OptionDDDList.Items.Clear();
            OptionDDDList.Items.Add(new ListItem("Select Pulse", "Select Pulse"));
            OptionDDDList.Items.Add(new ListItem("6", "6"));
            OptionDDDList.Items.Add(new ListItem("AFE", "AFE"));
            OptionDDDList.Visible = true;

            RefreshColD();
        }

        private void LoadColDDD_AccessoriesOptions()
        {
            OptionDDDList.Items.Clear();
            OptionDDDList.Items.Add(new ListItem("Select Accessory", "Select Accessory"));
            OptionDDDList.Items.Add(new ListItem("Motor Saver, 520CP", "Motor Saver, 520CP"));
            OptionDDDList.Items.Add(new ListItem("Motor Saver, 777", "Motor Saver, 777"));
            OptionDDDList.Items.Add(new ListItem("Snsr Backspin Mon w/Probe", "Snsr Backspin Mon w/Probe"));
            OptionDDDList.Items.Add(new ListItem("Potential Trafo 1500 Volt", "Potential Trafo 1500 Volt"));
            OptionDDDList.Items.Add(new ListItem("Digital Amp Readout", "Digital Amp Readout"));
            OptionDDDList.Items.Add(new ListItem("Standard Junction Box", "Standard Junction Box"));

            OptionDDDList.Visible = true;
            RefreshColD();
        }

        private void RefreshColD()
        {
            OptionDDDList.Items[0].Selected = true;
            OptionDDDList.Visible = true;
            OptionDDDList.Focus();

            UpdatePanelLabelColD.Update();
            UpdatePanelDDColD.Update();
        }
    
        private void LoadColD_Label(string labeltext, string selectiontext)
        {
            Label labelD = this.FindControl("LabelDCol") as Label;
            labelD.Text = labeltext;

            labelD.Visible = true;
            UpdatePanelDDColD.Update();
            UpdatePanelLabelColD.Update();
        }

        protected void OptionDDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearColumnsE();
            string surfaceOptionA = TextBoxOption.Text;
            string surfaceOptionB = TextBox1.Text;
            string surfaceOptionC = TextBox2.Text;

            string surfaceOptionD = OptionDDDList.SelectedValue.ToString();
            TextBox3.Text = surfaceOptionD;

            UpdatePanelDDColD.Update();
            UpdatePanelTitle.Update();
            
            if (surfaceOptionB == "Spoc")
                LoadColE_SpocOptions();
        }


        /***** Columns F Methods and Events *****/
        private void ClearColumnsE()
        {
            DEOOptionsE_Div.Visible = false;
            voltsAmpsDiv.Visible = false;
            VoltsAmpsLabels.Visible = false;
            VoltsAmpsToggle.Visible = false;
            borderDiv.Visible = false;

            KVATextBox.Text = String.Empty;
            KVATextBox.Enabled = false;
            KVATextBox.Visible = false;
            AmpsTextBox.Text = String.Empty;
            AmpsTextBox.Visible = false;

            VoltsAmpsToggle.ClearSelection();
            OptionEDropdown.Items.Clear();
            OptionFDropdown.Items.Clear();
            OptionEDropdown.Enabled = false;
            OptionFDropdown.Enabled = false;

            OptionEDropdown.Visible = false;
            OptionFDropdown.Visible = false;

            Label labelKVA = this.FindControl("LabelKVA") as Label;
            labelKVA.Visible = false;
            DEOOptionsE_Div.Attributes.CssStyle.Add("border-color", "gray");

            UpdatePanelLabelColE.Update();
            UpdatePanelDDColE.Update();
        }
            
        private void refreshColE()
        {
            OptionEDropdown.Visible = true;
            OptionFDropdown.Visible = true;
            borderDiv.Visible = true;
           
            HideColE_VoltsAmpsText();
            ShowColE_ToggleOptions();

            UpdatePanelLabelColE.Update();
            UpdatePanelDDColE.Update();
        }
         
        protected void OptionEDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eDropdownIndex = 0;
            eDropdownIndex = OptionEDropdown.SelectedIndex;
            OptionFDropdown.SelectedIndex = eDropdownIndex;
            UpdatePanelDDColE.Update();

            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = TextBox2.Text;
            string DEOOptionD = OptionEDropdown.SelectedValue.ToString();
            TextBox3.Text = DEOOptionD;
            UpdatePanelTitle.Update();
        }
            
        protected void OptionFDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int fDropdownIndex = 0;

            fDropdownIndex = OptionFDropdown.SelectedIndex;
            OptionEDropdown.SelectedIndex = fDropdownIndex;
            UpdatePanelDDColE.Update();

            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = TextBox2.Text;
            string DEOOptionD = OptionEDropdown.SelectedValue.ToString();
            TextBox3.Text = DEOOptionD;
            UpdatePanelTitle.Update();
        }
        
        protected void VoltsAmpsToggle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (VoltsAmpsToggle.SelectedValue.ToString() == "Volts")
            {
                OptionEDropdown.Enabled = true;
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "-internal-light-dark(rgb(255, 255, 255), rgb(59, 59, 59))");
                OptionEDropdown.Attributes.CssStyle.Add("color", "darkblue");

                OptionFDropdown.Enabled = false;
                OptionFDropdown.Attributes.CssStyle.Add("background-color", "#F8F8F8");
                OptionFDropdown.Attributes.CssStyle.Add("color", "#888888");
            }
            else if (VoltsAmpsToggle.SelectedValue.ToString() == "Amps")
            {
                OptionEDropdown.Enabled = false;
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "#F8F8F8");
                OptionEDropdown.Attributes.CssStyle.Add("color", "#888888");

                OptionFDropdown.Enabled = true;
                OptionFDropdown.Attributes.CssStyle.Add("background-color", "-internal-light-dark(rgb(255, 255, 255), rgb(59, 59, 59))");
                OptionFDropdown.Attributes.CssStyle.Add("color", "darkblue");
            }
            UpdatePanelDDColE.Update();
        }
        
        private void HideColE_ToggleOptions()
        {
            VoltsAmpsToggle.ClearSelection();
            VoltsAmpsToggle.Enabled = false;

            DEOOptionsE_Div.Visible = false;
            voltsAmpsDiv.Visible = false;
            VoltsAmpsToggle.Visible = false;
            OptionEDropdown.Visible = false;
            OptionFDropdown.Visible = false;

            UpdatePanelLabelColE.Update();
            UpdatePanelDDColE.Update();
        }

        private void ShowColE_ToggleOptions()
        {
            VoltsAmpsToggle.Enabled = true;
            VoltsAmpsToggle.SelectedIndex = 0;
           
            OptionEDropdown.Attributes.CssStyle.Add("color", "darkblue");
            OptionEDropdown.Enabled = true;

            OptionFDropdown.Attributes.CssStyle.Add("background-color", "#F8F8F8");
            OptionFDropdown.Attributes.CssStyle.Add("color", "#888888");
            OptionFDropdown.Enabled = false;

            VoltsAmpsToggle.Visible = true;
            voltsAmpsDiv.Visible = true;
            DEOOptionsE_Div.Visible = true;
            OptionEDropdown.Visible = true;
            OptionFDropdown.Visible = true;
            UpdatePanelLabelColE.Visible = true;

            UpdatePanelLabelColE.Update();
            UpdatePanelDDColE.Update();
        }

        private void HideColE_VoltsAmpsText()
        {
            DEOOptionsE_Div.Visible = false;
            KVATextBox.Visible = false;
            AmpsTextBox.Visible = false;
            VoltsAmpsLabels.Visible = false;
            DEOOptionsE_Div.Visible = false;

            UpdatePanelLabelColE.Update();
            UpdatePanelDDColE.Update();
        }
       
        private void ShowColE_VoltsAmpsText()
        {
            VoltsAmpsLabels.Visible = true;
            KVATextBox.Visible = true;
            KVATextBox.Enabled = false;
            AmpsTextBox.Visible = true;
            voltsAmpsDiv.Visible = true;
            DEOOptionsE_Div.Visible = true;

            UpdatePanelLabelColE.Update();
            UpdatePanelDDColE.Update();
        }

        private void LoadColE_SFL_Options()
        {
            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick KVA", "Pick KVA"));
            OptionEDropdown.Items.Add(new ListItem("166", "166"));
            OptionEDropdown.Items.Add(new ListItem("251", "251"));

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionFDropdown.Items.Add(new ListItem("200", "200"));
            OptionFDropdown.Items.Add(new ListItem("302", "302"));

            refreshColE();
        }

        private void LoadColE_SLH_Options()
        {
            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick KVA", "Pick KVA"));
            OptionEDropdown.Items.Add(new ListItem("345", "345"));
            OptionEDropdown.Items.Add(new ListItem("400", "400"));
            OptionEDropdown.Items.Add(new ListItem("500", "500"));

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionFDropdown.Items.Add(new ListItem("415", "415"));
            OptionFDropdown.Items.Add(new ListItem("481", "481"));
            OptionFDropdown.Items.Add(new ListItem("600", "600"));

            refreshColE();
        }
                
        private void LoadColE_SpocOptions()
        {
            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick KVA", "Pick KVA"));
            OptionEDropdown.Items.Add(new ListItem("116", "116"));
            OptionEDropdown.Items.Add(new ListItem("141", "141"));
            OptionEDropdown.Items.Add(new ListItem("170", "170"));
            OptionEDropdown.Items.Add(new ListItem("217", "217"));
            OptionEDropdown.Items.Add(new ListItem("249", "249"));
            OptionEDropdown.Items.Add(new ListItem("319", "319"));
            OptionEDropdown.Items.Add(new ListItem("382", "382"));
            OptionEDropdown.Items.Add(new ListItem("432", "432"));
            OptionEDropdown.Items.Add(new ListItem("490", "490"));
            OptionEDropdown.Items.Add(new ListItem("540", "540"));

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionFDropdown.Items.Add(new ListItem("140", "140"));
            OptionFDropdown.Items.Add(new ListItem("170", "170"));
            OptionFDropdown.Items.Add(new ListItem("205", "205"));
            OptionFDropdown.Items.Add(new ListItem("261", "261"));
            OptionFDropdown.Items.Add(new ListItem("300", "300"));
            OptionFDropdown.Items.Add(new ListItem("385", "385"));
            OptionFDropdown.Items.Add(new ListItem("460", "460"));
            OptionFDropdown.Items.Add(new ListItem("520", "520"));
            OptionFDropdown.Items.Add(new ListItem("590", "590"));
            OptionFDropdown.Items.Add(new ListItem("650", "650"));

            refreshColE();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            string errorMsg = "";
            errorMsg = validateTextBoxes();
            if (errorMsg.Length == 0)
            {
                StringBuilder emailBody = createBody("email");
                StringBuilder emailSubject = createSubject("email");
                sendEmail(emailBody, emailSubject);

                TechConnect_Utilities util = new TechConnect_Utilities();
                bool sendText = util.CheckSeverity(SeverityDropDownList);
                if (sendText)
                {
                    StringBuilder textBody = createBody("text");
                    sendTextMsg(textBody);
                }

                UpdatePanelSubmitButton.Update();
                util.CleanUploadsFolder();

                ModalPopupLabel1.Text = "Thank you for your submission!";
                ModalPopupLabel2.Text = "A ticket will be created and sent to your email address. ";
                ModalPopupLabel3.Text = "The ticket generator will be ‘Service Desk Plus Ticketing System’.";
                if (sendText)
                    ModalPopupLabel4.Text = "A text will also be sent to a staff member for immediate assistance.";

                TextBox8.Text = "Exit";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
            else
            {
                ModalPopupLabel1.Text = "Error:";
                ModalPopupLabel2.Text = errorMsg;
                ModalPopupLabel3.Text = "";
                ModalPopupLabel4.Text = "";
                TextBox8.Text = "Stay";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
        }

        private void sendEmail(StringBuilder emailBody, StringBuilder emailSubject)
        {
            String userEmailAddress = Session["s_UserEmail"].ToString();
            String supportEmailAddress = Session["s_SupportEmail"].ToString();
            String supportEmailPassword = Session["s_SupportPswd"].ToString();
            String sendGridUserName = Session["s_SendGridUserName"].ToString();
            String sendGridPassword = Session["s_SendGridPassword"].ToString();
            String ticketEmailAddress = Session["s_TicketingEmail"].ToString();

            //String ticketEmailAddress = "tbairok@gmail.com";

            using (MailMessage message = new MailMessage(supportEmailAddress, ticketEmailAddress, emailSubject.ToString(), emailBody.ToString()))
            {
                AddAttachments(message);
                message.ReplyToList.Add(userEmailAddress);
                SmtpClient emailClient = new SmtpClient("smtp.sendgrid.net", 587);
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential(sendGridUserName, sendGridPassword);
                emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                emailClient.EnableSsl = false;

                try
                {
                    emailClient.Send(message);
                }
                catch (SmtpFailedRecipientException ex)
                {

                }
            }
        }

        private void sendTextMsg(StringBuilder textBody)
        {
            var accountSid = Session["s_TwilioAcctID"].ToString();
            var authToken = Session["s_TwilioAuthToken"].ToString();
            var fromPhoneNum = Session["s_TwilioFromPhoneNum"].ToString();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            TwilioClient.Init(accountSid, authToken);
            List<string> TextNums = new List<string>();

            string textnum1 = Session["s_TwilioFromPhoneNum1"].ToString();
            if (textnum1 != "")
                TextNums.Add(textnum1);
            string textnum2 = Session["s_TwilioFromPhoneNum2"].ToString();
            if (textnum2 != "")
                TextNums.Add(textnum2);
            string textnum3 = Session["s_TwilioFromPhoneNum3"].ToString();
            if (textnum3 != "")
                TextNums.Add(textnum3);
            string textnum4 = Session["s_TwilioFromPhoneNum4"].ToString();
            if (textnum4 != "")
                TextNums.Add(textnum4);

            foreach (var textnum in TextNums)
            {
                var messageOptions = new CreateMessageOptions(
                new PhoneNumber(textnum));

                messageOptions.From = new PhoneNumber(fromPhoneNum);
                messageOptions.Body = textBody.ToString();

                var message = MessageResource.Create(messageOptions);
                Console.WriteLine(message.Body);
            }
        }

        private StringBuilder createSubject(string messagetype)
        {
            string surfaceOption = SurfaceOptions.SelectedItem.Text;
            string severityLable = LabelSeverity.Text;
            string sLevel = "";

            String sColText = "";
            if (SeverityDropDownList.Items.Count > 0)
            {
                sColText = SeverityDropDownList.SelectedItem.Text;
            }
            int colonIndex = 0;
            colonIndex = sColText.IndexOf(":");
            sLevel = sColText.Substring(0, colonIndex);

            StringBuilder emailSubject = new StringBuilder();
            emailSubject.Append("TECH CONNECT - Surface Option: ");
            emailSubject.Append(surfaceOption + "; ");
            emailSubject.Append("Severity: " + sLevel);

            return emailSubject;
        }
        private void AddAttachments(MailMessage message)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            string uploadPath = util.GetUploadPath();
            var folderPath = HttpContext.Current.Server.MapPath(uploadPath);

            bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadupPathExists)
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    Attachment attachment = new Attachment(file);
                    message.Attachments.Add(attachment);
                }
            }
        }

        private string validateTextBoxes()
        {
            string errMsg = "";
            Regex r = new Regex("^[a-zA-Z0-9-@':&,.!?/_$ ]*$");

            if (CompetitorNameText.Text.Length > 0)
            {
                string cNameString = CompetitorNameText.Text;
                if (!r.IsMatch(cNameString))
                {
                    char[] competitorArray = cNameString.ToCharArray();
                    for (int i = 0; i < competitorArray.Length; i++)
                    {
                        string commentsChar = competitorArray[i].ToString();
                        if (!r.IsMatch(commentsChar))
                        {
                            errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character from Competitor Name.";
                            i = competitorArray.Length;
                        }
                    }
                }
            }

            if (errMsg == "")
            {
                if (PulseText.Text.Length > 0)
                {
                    string pulseString = PulseText.Text;
                    if (!r.IsMatch(pulseString))
                    {
                        char[] pulseArray = pulseString.ToCharArray();
                        for (int i = 0; i < pulseArray.Length; i++)
                        {
                            string commentsChar = pulseArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character from Pulse.";
                                i = pulseArray.Length;
                            }
                        }
                    }
                }
            }
           
            if (errMsg == "")
            {
                if (AdditionalCommentsTextBox.Text.Length > 0)
                {
                    string commentString = AdditionalCommentsTextBox.Text;
                    if (!r.IsMatch(commentString))
                    {
                        char[] commentsArray = commentString.ToCharArray();
                        for (int i = 0; i < commentsArray.Length; i++)
                        {
                            string commentsChar = commentsArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character.";
                                i = commentsArray.Length;
                            }
                        }
                    }
                }
                else
                    errMsg = "Please enter 'Comments' before submitting ticket.";
            }
            return errMsg;
        }

        private StringBuilder createBody(string messagetype)
        {
            string userEmail = Session["s_UserEmail"] as string;

            int endPos = userEmail.IndexOf("@");
            string currentUser = userEmail.Substring(0, endPos);
            
            string bColLabel = "";
            string bColText = "";
            if (OptionBDDList.Items.Count > 0)
            {
                if (OptionBDDList.SelectedIndex > 0)
                {
                    bColLabel = LabelBCol.Text;
                    bColText = OptionBDDList.SelectedItem.Text;
                }
            }
            
            string cColLabel = "";
            string cColText = "";
            if (OptionCDDList.Items.Count > 0)
            {
                if (OptionCDDList.SelectedIndex > 0)
                {
                    cColLabel = LabelCCol.Text;
                    cColText = OptionCDDList.Text;
                }
            }
            else
            {
                if (CompetitorNameText.Text.Length > 0)
                {
                    cColLabel = LabelCCol.Text;
                    cColText = CompetitorNameText.Text;
                }
            }

            string dColLabel = "";
            string dColDDText = "";
            if (OptionDDDList.Items.Count > 0)
            {
                if (OptionDDDList.SelectedIndex > 0)
                {
                    dColLabel = LabelDCol.Text;
                    dColDDText = OptionDDDList.Text;
                }
            }
            else
            {
                if (PulseText.Text.Length > 0)
                {
                    dColLabel = LabelDCol.Text;
                    dColDDText = PulseText.Text;
                }
            }

            string vLable = "";
            string aLable = "";
            string eColText = "";
            string fColText = "";
            if (OptionEDropdown.Items.Count > 0)
            {
                if (OptionEDropdown.SelectedIndex > 0)
                {
                    vLable = LabelVolts.Text;
                    aLable = LabelAmps.Text;
                    eColText = OptionEDropdown.SelectedItem.Text;
                    fColText = OptionFDropdown.SelectedItem.Text;
                }
            }
            else
            {
                if (KVATextBox.Text.Length > 0)
                {
                    vLable = LabelVolts.Text;
                    eColText = KVATextBox.Text;

                    if (AmpsTextBox.Text.Length > 0)
                    {
                        aLable = LabelAmps.Text;
                        fColText = AmpsTextBox.Text;
                    }
                }
            }
            
            string addComentsLable = LabelAddComments.Text;
            string addComentsText = "";
            if (AdditionalCommentsTextBox.Text.Length > 0) addComentsText = AdditionalCommentsTextBox.Text;

            string severityLable = LabelSeverity.Text;
            string sColText = "";
            if (SeverityDropDownList.Items.Count > 0) sColText = SeverityDropDownList.SelectedItem.Text;

            StringBuilder emailBody = new StringBuilder();
            string surfaceOption = SurfaceOptions.SelectedItem.Text;

            if (messagetype == "email")
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("");

                emailBody.AppendLine("Requester's Name: " + "\t" + currentUser);
                emailBody.AppendLine("Requester's Email: " + "\t" + "  " + userEmail);

                emailBody.AppendLine("");
                emailBody.AppendLine("Surface Option: " + "\t" + "    " + surfaceOption);

                if (bColText.Length > 0)
                {
                    //emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + bColText);
                    switch (bColLabel)
                    {
                        case "VSD Option":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "      " + bColText);
                            break;
                        case "Trans Option":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "      " + bColText);
                            break;
                        case "Switchboard":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "     " + bColText);
                            break;
                        case "Switchboard Accs":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "" + bColText);
                            break;
                        case "Sensor Surf Equip":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "  " + bColText);
                            break;
                        default:
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + bColText);
                            break;
                    }
                }

                if (cColText.Length > 0)
                {
                    //emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + cColText);
                    switch (cColLabel)
                    {
                        case "Model Options":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "   " + cColText);
                            break;
                        case "Competitor Name":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "" + cColText);
                            break;
                        case "KVA":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "\t" +"\t" +"     " + cColText);
                            break;
                        case "Volts":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "\t" + "\t" + "    " + cColText);
                            break;
                        case "SWB":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "\t" + "\t" + "    " + cColText);
                            break;
                        case "Controller":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "\t" + " " + cColText);
                            break;
                        case "Part Number":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "     " + cColText);
                            break;
                        default:
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "\t" + cColText);
                            break;
                    }
                }

                if (dColDDText.Length > 0)
                {
                    //emailBody.AppendLine(dColLabel + ": " + "\t" + "\t" + dColDDText);
                    switch (dColLabel)
                    {
                        case "Pulse":
                            emailBody.AppendLine(dColLabel + ": " + "\t" + "\t" + "\t" + "\t" + "    " + dColDDText);
                            break;
                        case "Pulse Options":
                            emailBody.AppendLine(dColLabel + ": " + "\t" + "\t" + "     " + dColDDText);
                            break;
                        case "Accessories":
                            emailBody.AppendLine(dColLabel + ": " + "\t" + "\t" + "      " + dColDDText);
                            break;
                        default:
                            emailBody.AppendLine(dColLabel + ": " + "\t" + "\t" + "\t" + dColDDText);
                            break;
                    }
                }

                if (eColText.Length > 0)
                {
                    if (fColText.Length > 0)
                        emailBody.AppendLine(vLable + ": " + "\t" + "\t" + eColText + "\t" + "\t" + aLable + ": " + "\t" + "\t" + fColText);
                   else
                        emailBody.AppendLine(vLable + ": " + "\t" + "\t" + "\t" + "\t" + "    " + eColText);
                }

                emailBody.AppendLine("Comments: " + "\t" + "  " + addComentsText);
                emailBody.AppendLine("Severity: " + "\t" + "\t" + sColText);
            }
            else
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("Severity: Immediate");
                emailBody.AppendLine("");
                emailBody.AppendLine("Requester's Name: " + currentUser);
                emailBody.AppendLine("Requester's Email: " + userEmail);
                emailBody.AppendLine("Surface Option: " + surfaceOption);
                
                if (bColText.Length > 0) emailBody.AppendLine(bColLabel + ": " + bColText);
                if (cColText.Length > 0) emailBody.AppendLine(cColLabel + ": " + cColText);
                if (dColDDText.Length > 0)
                {
                    if (eColText.Length > 0)
                        emailBody.AppendLine(vLable + ": " + dColDDText + "; " + aLable + ": " + eColText);
                    else
                        emailBody.AppendLine(vLable + ": " + dColDDText);
                }

                emailBody.AppendLine("Comments: " + addComentsText);
            }

            return emailBody;
        }

        protected void AFButton_Click(object sender, EventArgs e)
        {
            LabelMaxFiles.Text = String.Empty;
            UpdatePanelAFButton.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();

            if (FileUploadDiv.Visible == false)
            {
                ListBoxFileUpload.Items.Clear();
                FileUploadDiv.Visible = true;
            }
            else
            {
                FileUploadDiv.Visible = false;
                UpdatePanelFileUpload.Update();
            }

            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            var postedFile = FileUpload.PostedFile;
            var newAttachment = FileUpload.PostedFile.FileName.ToString();

            TechConnect_Utilities util = new TechConnect_Utilities();
            bool canUpload = util.CheckUploadFiles(postedFile, ListBoxFileUpload, LabelMaxFiles, FileUpload, UpdatePanelFileUpload);
            if (canUpload)
            {
                ListBoxFileUpload.Items.Add(new ListItem(newAttachment, newAttachment));
                ListBoxFileUpload.SelectedIndex = -1;

                UploadLabel.Visible = true;
                string uploadPath = util.GetUploadPath();

                bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
                if (!userUploadupPathExists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(uploadPath));

                FileUpload.PostedFile.SaveAs(Server.MapPath(uploadPath + newAttachment));
            }

            for (int i = 0; i < ListBoxFileUpload.Items.Count; i++)
            {
                string fileName = ListBoxFileUpload.Items[i].Text;

                TableRow row = new TableRow();
                TableItemStyle rowStyle = new TableItemStyle();
                rowStyle.Height = Unit.Pixel(25);
                row.ApplyStyle(rowStyle);
                TableCell cell1 = new TableCell();
                cell1.ApplyStyle(rowStyle);
                cell1.Text = fileName;
                row.Cells.Add(cell1);
                SelectedFilesTable.Rows.Add(row);
            }

            UpdatePanelFileUpload.Update();
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            int selectedFileIndex = ListBoxFileUpload.SelectedIndex;

            if (selectedFileIndex == -1) LabelMaxFiles.Text = "Please select file to delete.";
            else
            {
                LabelMaxFiles.Text = "";
                string fileName = ListBoxFileUpload.SelectedValue;
                ListBoxFileUpload.Items.RemoveAt(selectedFileIndex);

                string FileToDelete;
                TechConnect_Utilities util = new TechConnect_Utilities();
                string uploadPath = util.GetUploadPath();

                FileToDelete = Server.MapPath(uploadPath + fileName);
                File.Delete(FileToDelete);
            }

            for (int i = 0; i < ListBoxFileUpload.Items.Count; i++)
            {
                string fileName = ListBoxFileUpload.Items[i].Text;
                TableRow row = new TableRow();
                TableItemStyle rowStyle = new TableItemStyle();
                rowStyle.Height = Unit.Pixel(25);
                row.ApplyStyle(rowStyle);
                TableCell cell1 = new TableCell();
                cell1.ApplyStyle(rowStyle);
                cell1.Text = fileName;
                row.Cells.Add(cell1);
                SelectedFilesTable.Rows.Add(row);
            }
            if (ListBoxFileUpload.Items.Count == 0)
            {
                UploadLabel.Visible = false;
            }

            UpdatePanelFileUpload.Update();
        }
        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            ListBoxFileUpload.Items.Clear();
            UpdatePanelFileUpload.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }
        protected void HomeButton_Click(object sender, EventArgs e)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.DeleteUserUploadFolder();

            var TechConnectHomeURL = Session["DefaultUrl"].ToString();
            Response.Redirect(TechConnectHomeURL, false);
        }
    }
}
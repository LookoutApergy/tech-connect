﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Tech_Connect_Survey_Forms
{
    public partial class DefaultMobile : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterHiddenField("pageHeight", "testtext");
            Page.ClientScript.RegisterHiddenField("pageWidth", "testtext");
            ClientScriptManager cs = this.ClientScript;
            StringBuilder cstext2 = new StringBuilder();
            
            cstext2.Append("var strW = screen.width;");
            cstext2.Append("var strH = screen.height;");
            cstext2.Append("document.getElementById('pageWidth').value = strW;");
            cstext2.Append("document.getElementById('pageHeight').value = strH;");
            cstext2.Append("document.getElementById('ImageButton1').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton2').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton3').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton4').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton5').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton6').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton7').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton8').style.width = '50%';");
            cstext2.Append("document.getElementById('ImageButton9').style.width = '50%';");
            cs.RegisterStartupScript(this.GetType(), "test1", cstext2.ToString(), true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            //if (validUser == "true")
            //Response.Redirect("~/DHE.aspx", false);

            if (validUser == "true")
            Response.Redirect("~/DHEMobile.aspx", false);
            //Response.Redirect("~/Surface.aspx", false);
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/SurfaceMobile.aspx", false);
                //Response.Redirect("~/DHE.aspx", false);
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/ApplicationMobile.aspx", false);
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Service CenterMobile.aspx", false);
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Field ServiceMobile.aspx", false);
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/EngineeringMobile.aspx", false);
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Vendor3rdPartyMobile.aspx", false);
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/OtherMobile.aspx", false);
                //Response.Redirect("~/DHE.aspx", false);
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Ideas - SuggestionsMobile.aspx", false);
        }
    }
}
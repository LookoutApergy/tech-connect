﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Tech_Connect_Survey_Forms
{
    public partial class DHE : System.Web.UI.Page
    {
    
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            Page.ClientScript.RegisterHiddenField("pageHeight", "testtext");
            ClientScriptManager cs = this.ClientScript;
            StringBuilder cstext2 = new StringBuilder();
                cstext2.Append("var str1 = screen.height -150; var str2 = 'px';");
                cstext2.Append("document.getElementById('pageHeight').value = str1 + str2;");
                cstext2.Append("var pHeightValue = document.getElementById('pageHeight').value;");
                //cstext2.Append("alert(TextBoxValue1);");
                cstext2.Append("document.getElementById('form1').style.height = pHeightValue;");
                cstext2.Append("document.getElementById('form1').style.background = '#f8f9fa';");
            cs.RegisterStartupScript(this.GetType(), "test1", cstext2.ToString(), true);

            if (!IsPostBack)
            {
                Label labelB = this.FindControl("LabelBCol") as Label;
                labelB.Visible = false;
                OptionBDropdown.Visible = false;
                ClearColumnC();
                ClearColumnsDandE();
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string BOHOption = BOHOptions.SelectedValue.ToString();
            ClearColumnC();
            ClearColumnsDandE();

            switch (BOHOption)
            {
                case "Motor":
                    LoadMotorSizes();
                    break;
                case "Pump":
                    LoadPumpSizes();
                    break;
                default:
                    break;
            }
        }

        private void ClearColumnC()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Visible = false;

            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "";
            labelC.Visible = false;
        }

        private void LoadPumpSizes()
        {
            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Pump Size...", "Select Pump Size..."));
            OptionBDropdown.Items.Add(new ListItem("375", "375"));
            //OptionBDropdown.Items.Add(new ListItem("420", "420"));
            OptionBDropdown.Items.Add(new ListItem("456", "456"));
            OptionBDropdown.Items.Add(new ListItem("540", "540"));
            OptionBDropdown.Items.Add(new ListItem("562", "562"));
            OptionBDropdown.Visible = true;

            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Pump Size";
            labelB.Visible = true;
        }

        protected void LoadMotorSizes()
        {
            OptionBDropdown.Items.Clear();

            OptionBDropdown.Items.Add(new ListItem("Select Motor Size...", "Select Motor Size..."));
            OptionBDropdown.Items.Add(new ListItem("338", "338"));
            OptionBDropdown.Items.Add(new ListItem("400", "400"));
            OptionBDropdown.Items.Add(new ListItem("513", "513"));
            OptionBDropdown.Items.Add(new ListItem("538", "538"));
            OptionBDropdown.Visible = true;

            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Motor Size";
            labelB.Visible = true;
        }

        protected void OptionBDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            string BOHOptionB = OptionBDropdown.SelectedValue.ToString();

                switch (BOHOptionB)
                {
                    case "375":
                        Load375HPOptions();
                        break;
                    case "456":
                        Load456HPOptions();
                        break;
                    default:
                        break;
                }        
        }

        private void Load456HPOptions()
        {
            ClearColumnsDandE();

            OptionCDropdown.Items.Add(new ListItem("Select 456 HP...", "Select 456 HP..."));
            OptionCDropdown.Items.Add(new ListItem("17", "17"));
            OptionCDropdown.Items.Add(new ListItem("25", "25"));
            OptionCDropdown.Items.Add(new ListItem("33", "33"));
            OptionCDropdown.Items.Add(new ListItem("42", "42"));

            OptionCDropdown.Items.Add(new ListItem("50", "50"));
            OptionCDropdown.Items.Add(new ListItem("67", "67"));
            OptionBDropdown.Items.Add(new ListItem("84", "84"));

            OptionCDropdown.Items.Add(new ListItem("100", "100"));
            OptionCDropdown.Items.Add(new ListItem("134", "134"));
            OptionCDropdown.Items.Add(new ListItem("167", "167"));
            OptionCDropdown.Items.Add(new ListItem("200", "200"));
            OptionCDropdown.Visible = true;

            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Horsepower";
            labelC.Visible = true;
        }

        private void ClearColumnsDandE()
        {
            OptionCDropdown.Items.Clear();
            VoltsAmpsToggle.ClearSelection();
            VoltsAmpsToggle.Visible = false;
            OptionDDropdown.Enabled = false;
            OptionEDropdown.Enabled = false;
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Visible = false;
            OptionEDropdown.Items.Clear();
            OptionEDropdown.Visible = false;

            VoltsTextBox.Visible = false;
            AmpsTextBox.Visible = false;
            VoltsAmpsLabels.Visible = false;
        }

        private void Load375HPOptions()
        {
            ClearColumnsDandE();

            OptionCDropdown.Items.Add(new ListItem("Select 375 HP...", "Select 375 HP..."));
            OptionCDropdown.Items.Add(new ListItem("14", "14"));
            OptionCDropdown.Items.Add(new ListItem("19", "19"));
            OptionCDropdown.Items.Add(new ListItem("27", "27"));
            OptionCDropdown.Items.Add(new ListItem("36", "36"));

            OptionCDropdown.Items.Add(new ListItem("47", "47"));
            OptionCDropdown.Items.Add(new ListItem("55", "55"));
            OptionCDropdown.Items.Add(new ListItem("110", "110"));

            OptionCDropdown.Items.Add(new ListItem("11", "11"));
            OptionCDropdown.Items.Add(new ListItem("15", "15"));
            OptionCDropdown.Items.Add(new ListItem("21", "21"));
            OptionCDropdown.Items.Add(new ListItem("27", "27"));
            OptionCDropdown.Items.Add(new ListItem("36", "36"));
            OptionCDropdown.Visible = true;

            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Horsepower";
            labelC.Visible = true;
        }

        protected void OptionCDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            string BOHOptionC = OptionCDropdown.SelectedValue.ToString();
            switch (BOHOptionC)
            {
                case "14":
                    load375_14_Options();
                    break;
                case "110":
                    load375_110_Options();
                    break;
                default:
                    break;
            }
        }

        private void load375_110_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionEDropdown.Items.Clear();
            //OptionDDropdown.Attributes.CssStyle.Add("background-color", "white");

            OptionDDropdown.Items.Add(new ListItem(" Select Volts ...", " Select Volts ..."));
            OptionDDropdown.Items.Add(new ListItem("1152", "1152"));
            OptionDDropdown.Items.Add(new ListItem("1186", "1186"));

            OptionEDropdown.Items.Add(new ListItem(" Select Amps ...", " Select Amps ..."));
            OptionEDropdown.Items.Add(new ListItem("75", "75"));
            OptionEDropdown.Items.Add(new ListItem("76", "76"));

            hideVoltsAmpsText();
            showToggleOptions();
        }

        private void loadTypeOptions()
        {
            

        }



        private void showToggleOptions()
        {

            VoltsAmpsToggle.Enabled = true;
            VoltsAmpsToggle.Visible = true;
            VoltsAmpsToggle.SelectedIndex = 0;
            OptionDDropdown.Attributes.CssStyle.Add("background-color", "white");
            OptionDDropdown.Enabled = true;
            OptionDDropdown.Visible = true;

            OptionEDropdown.Attributes.CssStyle.Add("background-color", "#f8f9fa");
            OptionEDropdown.Enabled = false;
            OptionEDropdown.Visible = true;

            VoltsAmpsLabels.Visible = false;
        }

        private void ShowVoltsAmpsText()
        {
            VoltsTextBox.Visible = true;
            AmpsTextBox.Visible = true;
            VoltsAmpsLabels.Visible = true;
        }

        private void hideVoltsAmpsText()
        {
            VoltsTextBox.Visible = false;
            AmpsTextBox.Visible = false;
            VoltsAmpsLabels.Visible = false;
        }

        private void hideToggleOptions()
        {
            VoltsAmpsToggle.ClearSelection();
            VoltsAmpsToggle.Enabled = false;
            VoltsAmpsToggle.Visible = false;
            OptionDDropdown.Visible = false;
            OptionEDropdown.Visible = false;
        }

        private void load375_14_Options()
        {
            VoltsTextBox.Text = "481";
            AmpsTextBox.Text = "24";

            hideToggleOptions();
            ShowVoltsAmpsText();
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (VoltsAmpsToggle.SelectedValue.ToString() == "Volts")
            {
                OptionDDropdown.Enabled = true;
                OptionEDropdown.Enabled = false;
                OptionDDropdown.Attributes.CssStyle.Add("background-color", "white");
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "#f8f9fa");

            }
            else if (VoltsAmpsToggle.SelectedValue.ToString() == "Amps")
            {
                OptionDDropdown.Enabled = false;
                OptionEDropdown.Enabled = true;
                OptionDDropdown.Attributes.CssStyle.Add("background-color", "#f8f9fa");
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "white");
            }
        }

        protected void OptionDDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int dDropdownIndex = 0;
            dDropdownIndex = OptionDDropdown.SelectedIndex;
            OptionEDropdown.SelectedIndex = dDropdownIndex;
        }

        protected void OptionEDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eDropdownIndex = 0;
            eDropdownIndex = OptionEDropdown.SelectedIndex;
            OptionDDropdown.SelectedIndex = eDropdownIndex;
        }

    }
}
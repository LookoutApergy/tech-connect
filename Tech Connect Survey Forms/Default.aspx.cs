﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Security.Cryptography;


namespace Tech_Connect_Survey_Forms
{
    public partial class _Default : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            InitializeSessionParameters();
            GetAdminEmails();

            String screenWidth = Session["s_screenWidth"].ToString();
            String screenHeight = Session["s_screenHeight"].ToString();
            //TextBoxVariableInfo.Text = screenWidth;
            //TextBoxVariableInfo2.Text = screenHeight;
            //UpdatePanelTitle.Update();

            AdminButton.Visible = false;
            AdminLabel.Visible = false;
            UpdatePanelAdminButton.Update();

          int sWidth = int.Parse(screenWidth);
          if (sWidth < 1280)
           {
                string encryptedUserEmail = Request.QueryString["email"] ?? "";
                string encryptedAppPassword = Request.QueryString["password"] ?? "";
                var baseurl = "~/DefaultMobile.aspx";
                var urlWithParams = baseurl + "?email=" + encryptedUserEmail + "&password=" + encryptedAppPassword;
                Response.Redirect(urlWithParams);
            }
        }

        private void InitializeSessionParameters()
        {
            Session["s_encryptedUserEmail"] = Request.QueryString["email"] ?? "";
            Session["s_encryptedAppPassword"] = Request.QueryString["password"] ?? "";
            Session["s_screenWidth"] = Request.QueryString["swidth"] ?? "";
            Session["s_screenHeight"] = Request.QueryString["sheight"] ?? "";
            Session["s_EncryptionKey"] = Properties.Settings.Default.ParamEncryptionKey;
            Session["s_TC_Password"] = Properties.Settings.Default.ApplicationPassword;
            Session["DefaultUrl"] = HttpContext.Current.Request.Url.AbsoluteUri;
        }

        private void GetAdminEmails()
        {
            Session["s_Admin1"] = Properties.Settings.Default.AdminEmail1;
            Session["s_Admin2"] = Properties.Settings.Default.AdminEmail2;
            Session["s_Admin3"] = Properties.Settings.Default.AdminEmail3;
            Session["s_Admin4"] = Properties.Settings.Default.AdminEmail4;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            String encryptedEmail = Session["s_encryptedUserEmail"].ToString() ?? "";
            if (encryptedEmail != "")
            {
                String currentUser = DecryptStringAES(encryptedEmail.Trim());
                //AdminLabel.Text = currentUser;
                String admin1 = Session["s_Admin1"].ToString();
                String admin2 = Session["s_Admin2"].ToString();
                String admin3 = Session["s_Admin3"].ToString();
                String admin4 = Session["s_Admin4"].ToString();

                if (currentUser == admin1 || currentUser == admin2 || currentUser == admin3 || currentUser == admin4)
                {
                    AdminButton.Visible = true;
                    AdminLabel.Visible = true;
                    UpdatePanelAdminButton.Update();
                }
            }
        }

        private string DecryptStringAES(string encryptedValue)
        {
            String encryptionKey = Session["s_EncryptionKey"].ToString();
            var keybytes = Encoding.UTF8.GetBytes(encryptionKey);
            var iv = Encoding.UTF8.GetBytes(encryptionKey);
            int encryptedLength = encryptedValue.Length;

            //DECRYPT FROM CRIPTOJS
            encryptedValue = encryptedValue.Replace(' ', '+');
            var encrypted = Convert.FromBase64String(encryptedValue);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);

            return decriptedFromJavascript;
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");

            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");

            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("key");

            string plaintext = null;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/DHE.aspx", false);
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Surface.aspx", false);
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Application.aspx", false);
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Service Center.aspx", false);
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Field Service.aspx", false);
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Engineering.aspx", false);
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Vendor3rdParty.aspx", false);
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Other.aspx", false);
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("~/Ideas - Suggestions.aspx", false);
        }

        protected void AdminButton_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/TechConnect Admin Desktop.aspx", false);
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Site.Mobile.Master" AutoEventWireup="true" CodeBehind="DefaultMobile.aspx.cs" Inherits="Tech_Connect_Survey_Forms.DefaultMobile" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server"  >

    <div>
        <div id="TechConnectOptionsDiv" style="padding-top:5%; width:100%;">    
            <div  id="Div1" runat="server" style=" display:grid; grid-template-columns:45% 52% 3%; grid-auto-rows:30px; width:100%">  
                <div style="grid-column:2; grid-row:1;  ">
                        <asp:Button ID="UserGuide" runat="server"  Text="TechConnect Users Guide" style="height:30px; width:100%; font-family: 'Segoe UI'; background-color: #003E51; color:white;  border-radius:5px; font-size:95%; border-width:medium; padding-bottom:2px; border-color: #00857D; border-bottom:none" onClientClick="window.open('/TechConnect Users Guide/TechConnect Guide.pdf','target=_parent');"  />
                </div>
            </div>

            <input type="hidden" id="pageHeight" runat="server" value="" />
            <input type="hidden" id="pageWidth" runat="server" value="" />

            <div  id="labelDiv" runat="server" style=" display:grid; grid-template-columns:33% 33% 33% ; grid-auto-rows:50px; padding-top:7%; ">  
                <div style="grid-column:1; grid-row:1; text-align:center; ">
                    <div  id="Div2" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center;">
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/DHE.jpg" style=" height:100%; " OnClick="ImageButton1_Click"/>   
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center; ">
                            <asp:Label ID="Label7" runat="server" Text="Downhole Equipment" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="grid-column:2; grid-row:1; text-align:center;">
                    <div  id="Div3" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/Surface.jpg" style=" height:100%; "  OnClick="ImageButton2_Click"/>
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;">
                            <asp:Label ID="Label1" runat="server" Text="Surface" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="grid-column:3; grid-row:1; text-align:center;  ">
                    <div  id="Div4" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/Applications.jpg" style="height:100%; " OnClick="ImageButton3_Click" />
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;">
                            <asp:Label ID="Label2" runat="server" Text="Application" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="grid-column:1; grid-row:3; text-align:center;">      
                    <div  id="Div5" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/Service Center Resized.png" style=" height:100%; " OnClick="ImageButton4_Click"/> 
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;">                     
                            <asp:Label ID="Label4" runat="server" Text="Service Center" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="grid-column:2; grid-row:3; text-align:center;">
                    <div  id="Div6" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/Field Service Revised.png" style=" height:100%; " OnClick="ImageButton5_Click"/> 
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;">  
                            <asp:Label ID="Label3" runat="server" Text="Field Service" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label> 
                        </div>
                    </div>
                </div>
                <div style="grid-column:3; grid-row:3; text-align:center;">
                    <div  id="Div7" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/Engineering Revised.png" style=" height:100%; " OnClick="ImageButton6_Click"/>
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;">               
                            <asp:Label ID="Label6" runat="server" Text="Engineering" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="grid-column:1; grid-row:5;  text-align:center; ">
                    <div  id="Div8" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/Third Party Vendors Revised.png" style="height:100%; " OnClick="ImageButton7_Click"/>
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;">    
                            <asp:Label ID="Label10" runat="server" Text="Vendor-3rd Party" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="grid-column:2; grid-row:5; text-align:center; ">
                    <div  id="Div9" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/Other Revised.png" style=" height:100%; " OnClick="ImageButton8_Click"/>
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;"> 
                            <asp:Label ID="Label11" runat="server" Text="Other" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="grid-column:3; grid-row:5; text-align:center;  ">
                    <div  id="Div10" runat="server" style=" display:grid; grid-template-columns:100% ; grid-auto-rows:50px;">  
                        <div style="grid-column:1; grid-row:1; text-align:center; ">
                            <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/Ideas and Suggestions Revised.png" style=" height:100%; " OnClick="ImageButton9_Click"/>
                        </div>
                        <div style="grid-column:1; grid-row:2; text-align:center;"> 
                            <asp:Label ID="Label12" runat="server" Text="Ideas & Suggestions" Style="font-family:'Segoe UI'; color:#003E51; font-size:95%; "></asp:Label> 
                        </div>
                    </div>
                </div>
                <div style="grid-column:2; grid-row:7; text-align:center; width: 100%; height: 100%;" >      
                    <asp:Image ID="TechConnectImage" runat="server" ImageUrl="~/Images/TechConnect Icon with space modified.png"  width="100%" Height="100%"  />
                </div>
            </div>
        </div> 
    </div>

    <script type="text/javascript"  >

        $(document).ready(function () {

            //document.getElementById('TextBox3').value = "Test 1";
            //document.getElementById('TextBox4').value = "Test 2";

        });

     </script>

</asp:Content>


  


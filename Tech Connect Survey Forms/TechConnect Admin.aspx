﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TechConnect Admin.aspx.cs" Inherits="Tech_Connect_Survey_Forms.TechConnect_Admin" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
    <link rel="stylesheet" href="css/modalanimate.css"/>
    <link rel="stylesheet" type="text/css" id="TCStyles" href="TechConnect Computer Styles.css" />
    <script type="text/javascript" src="~/Scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanelForm" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>
                <div id="MainDiv" runat="server"  style="margin-left: 4%; margin-right: 2%; opacity: 0.5;">     

                    <div id="BannerDiv" style="height:150px; position:relative">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Intranet-Banner Final  Longer.jpg" Height="100%" ImageAlign="Left" Width="98%"  />
                        <asp:Label ID="Label2" runat="server" Text="TechConnect" CssClass="TechConnectLabelStyle"></asp:Label>
                        <div style="position:absolute; left:90%; bottom:10%; z-index:100; "> 
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Home.png" Style="height:35px" onclick="clickHome()" />
                            <asp:Button ID="HomeButton" runat="server" style="visibility:hidden" OnClick="HomeButton_Click"/>
                        </div>
                    </div>     

                    <div id="LabelMainDiv" class="LabelMainDivStyle">
                        <asp:Label ID="LabelAdmin" runat="server" Text="TechConnect Administration" CssClass="LabelMainStyle"></asp:Label>
                    </div>
                    <div style="padding-top:5%"></div>

                    <div id="TextboxesDiv" style="display:none" runat="server">
                        <asp:UpdatePanel ID="UpdatePanelContactRow1" runat="server" UpdateMode="Conditional" >
                            <ContentTemplate>
                                <div  class="TextBoxesDivStyleAdminShading">
                                    <div style="grid-row:1; grid-column:1;  padding-top:2%; background-color:rgba(0, 62, 81, 0.09); " >
                                        <asp:Label runat="server" Text="Contact Name" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; padding-left:6%; " ></asp:Label>
                                    </div>
                                    <div style="grid-row:2; grid-column:1; background-color:rgba(0, 62, 81, 0.09); padding-bottom:1%; padding-left:6%;  ">
                                        <asp:UpdatePanel ID="ContactName1" runat="server" UpdateMode="Conditional" style="height:100%" >
                                            <ContentTemplate>
                                                <asp:TextBox ID="Name1" runat="server" TabIndex="1" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:94%; padding-left:1%; height:100%" ></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel> 
                                    </div>
                                    <div style="grid-row:3; grid-column:1; padding-top:2%; background-color:rgba(0, 62, 81, 0.09); padding-left:6%"  >
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 5% 18%; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:Label runat="server" Text="Contact Cell Number" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                            <div style="grid-row:1; grid-column:3; ">
                                                <asp:Label runat="server" Text="Active?" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="grid-row:4; grid-column:1;background-color:rgba(0, 62, 81, 0.09); padding-left:6%;  padding-bottom:1%; ">
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 3% 13% 1% 13% 1% 13% ; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:UpdatePanel ID="ContactPhone1" runat="server" UpdateMode="Conditional" style="height:100%">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="PhoneNum1" runat="server" TabIndex="2"  AutoPostBack="True" TextMode="Phone" placeholder="(555) 555-5555" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:98%; padding-left:2%; height:85%;"></asp:TextBox> 
                                                    </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:3; border:inset; border-color:rgba(0, 62, 81, 0.15)">
                                                <asp:UpdatePanel ID="ContactToggle1" runat="server" UpdateMode="Conditional" style="height:100%">
                                                    <ContentTemplate>
                                                        <asp:RadioButtonList ID="Contact1Toggle" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" CssClass="VoltsAmpsToggleAdminStyle" CellPadding="0" CellSpacing="0" >
                                                            <asp:ListItem Value="Yes" >Yes</asp:ListItem>
                                                            <asp:ListItem Value="No" >No</asp:ListItem>
                                                        </asp:RadioButtonList>   
                                                    </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:5; ">
                                                <asp:Button ID="ButtonSave1" runat="server" Text="Save"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonSave1_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:7; ">
                                                <asp:Button ID="ButtonCancel1" runat="server" Text="Cancel"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonCancel1_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:9; ">
                                                <asp:Button ID="ButtonClear1" runat="server" Text="Clear"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonClear1_Click" onClientClick="javascript:return confirm('Are you sure you want to clear this contact information?');" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="grid-row:5; grid-column:1;background-color:rgba(0, 62, 81, 0.09); padding-left:6%; padding-bottom:1%; ">     
                                          <asp:UpdatePanel ID="UpdatePanelContact1Error" runat="server" UpdateMode="Conditional" style="height:100%">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Contact1Error" runat="server" Enabled="false" style="font-family: 'ITC Lubalin Graph Std';color: #DC4405; font-size: 115%; width:94%; height:100%; background-color:transparent; padding-top:5px; border:none"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                       
                                    </div>   

                                    <div style="grid-row:1; grid-column:3;  padding-top:2%; background-color:rgba(0, 62, 81, 0.09); " >
                                        <asp:Label runat="server" Text="Contact Name" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; padding-left:6%; " ></asp:Label>
                                    </div>
                                    <div style="grid-row:2; grid-column:3; background-color:rgba(0, 62, 81, 0.09); padding-bottom:1%; padding-left:6%;">
                                        <asp:UpdatePanel ID="ContactName2" runat="server" UpdateMode="Conditional" style="height:100%">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Name2" runat="server" TabIndex="3" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:94%; padding-left:1%; height:100%"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel> 
                                    </div>
                                    <div style="grid-row:3; grid-column:3; padding-top:2%; background-color:rgba(0, 62, 81, 0.09); padding-left:6%"  >
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 3% 13% 1% 13% 1% 13%; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:Label runat="server" Text="Contact Cell Number" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                            <div style="grid-row:1; grid-column:3; ">
                                                <asp:Label runat="server" Text="Active?" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="grid-row:4; grid-column:3;background-color:rgba(0, 62, 81, 0.09); padding-left:6%;  padding-bottom:1%; ">
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 3% 13% 1% 13% 1% 13%; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:UpdatePanel ID="ContactPhone2" runat="server" UpdateMode="Conditional" style="height:100%" >
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="PhoneNum2" runat="server" TabIndex="4"  AutoPostBack="True" TextMode="Phone" placeholder="(555) 555-5555" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:98%; padding-left:2%; height:85%;"></asp:TextBox> 
                                                        </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:3; border:inset; border-color:rgba(0, 62, 81, 0.15)">
                                                <asp:UpdatePanel ID="ContactToggle2" runat="server" UpdateMode="Conditional" style="height:100%" >
                                                    <ContentTemplate>
                                                        <asp:RadioButtonList ID="Contact2Toggle" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" CssClass="VoltsAmpsToggleAdminStyle" CellPadding="0" CellSpacing="0" >
                                                            <asp:ListItem Value="Yes" >Yes</asp:ListItem>
                                                            <asp:ListItem Value="No" >No</asp:ListItem>
                                                        </asp:RadioButtonList>   
                                                    </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:5; ">
                                                <asp:Button ID="ButtonSave2" runat="server" Text="Save"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%" OnClick="ButtonSave2_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:7; ">
                                                <asp:Button ID="ButtonCancel2" runat="server" Text="Cancel"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonCancel2_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:9; ">
                                                <asp:Button ID="ButtonClear2" runat="server" Text="Clear"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonClear2_Click" onClientClick="javascript:return confirm('Are you sure you want to clear this contact information?');" />
                                            </div>
                                        </div>
                                    </div>
                                    <div style="grid-row:5; grid-column:3;background-color:rgba(0, 62, 81, 0.09); padding-left:6%; padding-bottom:1%; ">     
                                          <asp:UpdatePanel ID="UpdatePanelContact2Error" runat="server" UpdateMode="Conditional" style="height:100%">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Contact2Error" runat="server" Enabled="false" style="font-family: 'ITC Lubalin Graph Std';color: #DC4405; font-size: 115%; width:94%; height:100%; background-color:transparent; padding-top:5px; border:none"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                       
                                    </div>    
                                </div>

                                <div style="padding-top:3%"></div>

                                <div class="TextBoxesDivStyleAdminShading">
                                    <div style="grid-row:1; grid-column:1;  padding-top:2%; background-color:rgba(0, 62, 81, 0.09); " >
                                        <asp:Label runat="server" Text="Contact Name" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; padding-left:6%; " ></asp:Label>
                                    </div>
                                    <div style="grid-row:2; grid-column:1; background-color:rgba(0, 62, 81, 0.09); padding-bottom:1%; padding-left:6%;">
                                        <asp:UpdatePanel ID="ContactName3" runat="server" UpdateMode="Conditional" style="height:100%">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Name3" runat="server" TabIndex="5" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:94%; padding-left:1%; height:100%"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel> 
                                    </div>
                                    <div style="grid-row:3; grid-column:1; padding-top:2%; background-color:rgba(0, 62, 81, 0.09); padding-left:6%"  >
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 3% 13% 1% 13% 1% 13% ; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:Label runat="server" Text="Contact Cell Number" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                            <div style="grid-row:1; grid-column:3; ">
                                                <asp:Label runat="server" Text="Active?" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="grid-row:4; grid-column:1;background-color:rgba(0, 62, 81, 0.09); padding-left:6%;  padding-bottom:1%; ">
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 3% 13% 1% 13% 1% 13% ; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:UpdatePanel ID="ContactPhone3" runat="server" UpdateMode="Conditional" style="height:100%">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="PhoneNum3" runat="server" TabIndex="6"  AutoPostBack="True" TextMode="Phone" placeholder="(555) 555-5555" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:98%; padding-left:2%; height:85%;"></asp:TextBox> 
                                                    </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:3; border:inset; border-color:rgba(0, 62, 81, 0.15)">
                                                    <asp:UpdatePanel ID="ContactToggle3" runat="server" UpdateMode="Conditional" style="height:100%" >
                                                    <ContentTemplate>
                                                        <asp:RadioButtonList ID="Contact3Toggle" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" CssClass="VoltsAmpsToggleAdminStyle" CellPadding="0" CellSpacing="0" >
                                                            <asp:ListItem Value="Yes" >Yes</asp:ListItem>
                                                            <asp:ListItem Value="No" >No</asp:ListItem>
                                                        </asp:RadioButtonList>   
                                                    </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:5; ">
                                                <asp:Button ID="ButtonSave3" runat="server" Text="Save"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%" OnClick="ButtonSave3_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:7; ">
                                                <asp:Button ID="ButtonCancel3" runat="server" Text="Cancel"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonCancel3_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:9; ">
                                                <asp:Button ID="ButtonClear3" runat="server" Text="Clear"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick ="ButtonClear3_Click" onClientClick="javascript:return confirm('Are you sure you want to clear this contact information?');" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="grid-row:5; grid-column:1;background-color:rgba(0, 62, 81, 0.09); padding-left:6%; padding-bottom:1%; ">     
                                          <asp:UpdatePanel ID="UpdatePanelContact3Error" runat="server" UpdateMode="Conditional" style="height:100%">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Contact3Error" runat="server" Enabled="false" style="font-family: 'ITC Lubalin Graph Std';color: #DC4405; font-size: 115%; width:94%; height:100%; background-color:transparent; padding-top:5px; border:none"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                       
                                    </div>  
                                
                                    <div style="grid-row:1; grid-column:3;  padding-top:2%; background-color:rgba(0, 62, 81, 0.09); " >
                                        <asp:Label runat="server" Text="Contact Name" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; padding-left:6%; " ></asp:Label>
                                    </div>
                                    <div style="grid-row:2; grid-column:3; background-color:rgba(0, 62, 81, 0.09); padding-bottom:1%; padding-left:6%;">
                                        <asp:UpdatePanel ID="ContactName4" runat="server" UpdateMode="Conditional" style="height:100%">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Name4" runat="server" TabIndex="7" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:94%; padding-left:1%; height:100%"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel> 
                                    </div>
                                    <div style="grid-row:3; grid-column:3; padding-top:2%; background-color:rgba(0, 62, 81, 0.09); padding-left:6%"  >
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 3% 13% 1% 13% 1% 13%; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:Label runat="server" Text="Contact Cell Number" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                            <div style="grid-row:1; grid-column:3; ">
                                                <asp:Label runat="server" Text="Active?" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 120%;padding-top: 4%;width:100%; "></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="grid-row:4; grid-column:3;background-color:rgba(0, 62, 81, 0.09); padding-left:6%;  padding-bottom:1%; ">
                                        <div style="display: grid; grid-template-columns: 33% 2% 17% 3% 13% 1% 13% 1% 13%; grid-auto-rows: 40px; ">
                                            <div style="grid-row:1; grid-column:1; ">
                                                <asp:UpdatePanel ID="ContactPhone4" runat="server" UpdateMode="Conditional" style="height:100%">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="PhoneNum4" runat="server" TabIndex="8"  AutoPostBack="True" TextMode="Phone" placeholder="(555) 555-5555" style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 115%; width:98%; padding-left:2%; height:85%;"></asp:TextBox> 
                                                    </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:3; border:inset; border-color:rgba(0, 62, 81, 0.15)">
                                                <asp:UpdatePanel ID="ContactToggle4" runat="server" UpdateMode="Conditional" style="height:100%">
                                                    <ContentTemplate>
                                                        <asp:RadioButtonList ID="Contact4Toggle" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" CssClass="VoltsAmpsToggleAdminStyle" CellPadding="0" CellSpacing="0" >
                                                            <asp:ListItem Value="Yes" >Yes</asp:ListItem>
                                                            <asp:ListItem Value="No" >No</asp:ListItem>
                                                        </asp:RadioButtonList>   
                                                    </ContentTemplate>
                                                </asp:UpdatePanel> 
                                            </div>
                                            <div style="grid-row:1; grid-column:5; ">
                                                <asp:Button ID="ButtonSave4" runat="server" Text="Save"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%" OnClick="ButtonSave4_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:7; ">
                                                <asp:Button ID="ButtonCancel4" runat="server" Text="Cancel"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonCancel4_Click" />
                                            </div>
                                            <div style="grid-row:1; grid-column:9; ">
                                                <asp:Button ID="ButtonClear4" runat="server" Text="Clear"   style="font-family: 'ITC Lubalin Graph Std';color: #003E51;font-size: 100%;height:100%; width:100%; " OnClick="ButtonClear4_Click" onClientClick="javascript:return confirm('Are you sure you want to clear this contact information?');" />
                                            </div>
                                        </div>
                                    </div>  

                                    <div style="grid-row:5; grid-column:3;background-color:rgba(0, 62, 81, 0.09); padding-left:6%; padding-bottom:1%; ">     
                                          <asp:UpdatePanel ID="UpdatePanelContact4Error" runat="server" UpdateMode="Conditional" style="height:100%">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Contact4Error" runat="server" Enabled="false"  style="font-family: 'ITC Lubalin Graph Std';color: #DC4405; font-size: 115%; width:94%; height:100%; background-color:transparent; padding-top:5px; border:none"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                       
                                    </div> 

                                </div>

                                <div  id="labelDiv" runat="server" style=" display:grid; grid-template-columns:33% 33% 33% ; grid-auto-rows:100px; padding-top:5%;">  
                                    <div style="grid-column:2; grid-row:1;position:relative;  text-align:center;" >
                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/TechConnect Icon with space.png"  width="50%" Height="50%"/>                       
                                    </div>
                                </div>
                                                                    
                            </ContentTemplate>
                        </asp:UpdatePanel> 
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel> 


                <div id="LoginDiv" runat="server" style="position:relative; height:400px; display:block; ">
                    <div style="margin: 0;position: absolute;top: 50%;left: 50%; transform: translate(-50%, -50%); "> 
                        <div style="width:500px; height:200px; display: grid; grid-template-columns: 100%; grid-auto-rows: 60px; border:solid; border-color:#003E51">
                            <div style="grid-row:1; grid-column:1;display:flex; justify-content:center ">
                                <asp:Label ID="Label1" runat="server" style="background-color: #003E51; color:white; height:30px; text-align:center; width:500px; font-size:115%; font-family:Arial; padding-top:2%">Log In</asp:Label>
                            </div>
                            <div style="grid-row:2; grid-column:1;  ">
                                <div  style="display: grid; grid-template-columns: 35% 60%; grid-auto-rows: 30px; ">
                                    <div style="grid-row:1; grid-column:1;  text-align:right; padding-top:6px ">
                                        <asp:Label ID="UserNameLabel" runat="server" style="color:#003E51; width:450px; font-size:100%; font-family:Arial; padding-right:5px; ">User Email:</asp:Label>
                                    </div>
                                    <div style="grid-row:1; grid-column:2; ">                                     
                                        <asp:UpdatePanel ID="UpdatePanelUserName" runat="server" UpdateMode="Conditional" >
                                            <ContentTemplate>
                                                <asp:TextBox ID="UserName" runat="server" Width="200px" Text="" AutoCompleteType="Disabled" style="color:#003E51; font-size:100%; font-family:Arial; width:275px; "></asp:TextBox>  
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                        <div style="grid-row:2; grid-column:1;text-align:right;  padding-top:6px  ">
                                        <asp:Label ID="PasswordLabel" runat="server" style="color:#003E51; height:30px; width:450px; font-size:100%; font-family:Arial; padding-right:5px">Admin Password: </asp:Label>
                                    </div>
                                    <div style="grid-row:2; grid-column:2; ">
                                        <asp:UpdatePanel ID="UpdatePanelPassword" runat="server" UpdateMode="Conditional" >
                                            <ContentTemplate>
                                                <asp:TextBox ID="Password" runat="server" TextMode="Password" Text="" AutoCompleteType="Disabled" style="color:#003E51; font-size:100%; font-family:Arial; width:275px"></asp:TextBox> 
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>                   
                                </div>
                            </div>
                            <div style="grid-row:3; grid-column:1; text-align:center; ">
                                <div  style="display: grid; grid-template-columns: 100%; grid-auto-rows: 30px; ">
                                    <div style="grid-row:1; grid-column:1;  padding-top:6px ">
                                        <asp:UpdatePanel ID="UpdatePanelFailureText" runat="server" UpdateMode="Conditional" >
                                            <ContentTemplate>
                                                <asp:Label ID="FailureText" runat="server" Text=" Failure text" style="color: #DC4405; font-size:100%; font-family:Arial; width:350px; "></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div  style="display: grid; grid-template-columns: 65% 30%; grid-auto-rows: 30px;">
                                    <div style="grid-row:1; grid-column:2; text-align:center">
                                        <asp:Button ID="LoginButton" runat="server" Text="Log In" Width="110px" BackColor="#00857D" BorderColor="#003E51" BorderStyle="Solid" Font-Bold="True" ForeColor="White" Height="30px" BorderWidth="1px" Font-Size="Small" OnClick="LoginButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

 
                     
    </form>

     <script type="text/javascript">                
                $(document).ready(function () {
                    //Mask the textbox as per your format 123-123-123
                    $('#PhoneNum1').mask('99-9999-999', { placeholder: "#" });       
                });

         window.onload = function () {
             document.getElementById("UserName").value = "";
             document.getElementById("Password").value = "";
         }

         function phoneNum1() {
             document.getElementById('PhoneNum1').value = "###-###-####";
         }

         function clickHome() {
             var homeButton = document.getElementById('HomeButton');
             homeButton.click();
         }

         function enableSaveButton1() {
             var saveButton = document.getElementById('ButtonSave1');
             saveButton.disabled = false;
         }
 </script>
</body>
</html>

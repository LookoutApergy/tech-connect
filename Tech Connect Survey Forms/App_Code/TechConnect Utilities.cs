﻿using Microsoft.Ajax.Utilities;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Net;
using Twilio.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Tech_Connect_Survey_Forms.App_Code;
using System.Security.AccessControl;

namespace Tech_Connect_Survey_Forms.App_Code
{
    public class TechConnect_Utilities
    {
        public string GetUploadPath()
        {
            string userEmail = HttpContext.Current.Session["s_UserEmail"] as string;
            int endPos = userEmail.IndexOf("@");
            string currentUser = userEmail.Substring(0, endPos);

            bool upPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("Uploads"));
            if (!upPathExists)
                System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("Uploads"));

            string pathName = HttpContext.Current.Server.MapPath("Uploads");
            string userUploadPath = "~/Uploads/";
            var userPath = currentUser;
            userUploadPath = userUploadPath + userPath + "/";

            return userUploadPath.ToString();
        }

        public bool CheckFileName(string newUploadName, ListBox ListBoxFileUpload)
        {
            bool duplicateFile = false;
            int uploadFileCount = ListBoxFileUpload.Items.Count;

            for (int i = 0; i <= uploadFileCount - 1; i++)
            {
                ListBoxFileUpload.SelectedIndex = i;
                string uploadedName = ListBoxFileUpload.SelectedItem.Text;
                if (uploadedName == newUploadName)
                    duplicateFile = true;
            }
            return duplicateFile;
        }

        public bool CheckFileType(string newUploadFileType)
        {
            bool validType = true;
            string[] validFileTypes = { "image/jpeg", "image/png", "application/x-zip-compressed", "application/pdf", "application/vnd.ms-excel",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "video/mp4", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "video/quicktime"};
            string newfileType = newUploadFileType;

            int pos = Array.IndexOf(validFileTypes, newfileType);
            if (pos < 0)
                validType = false;

            return validType;
        }

        public bool GetUploadedAttachmentByteCount(int selectedFileLength)
        {
            int maxBytes = 35840000;
            bool maxBytesExceeded = false;
            long byteCount = 0;

            string uploadPath = GetUploadPath();
            bool userUploadPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadPathExists)
            {
                var userUploadPath = HttpContext.Current.Server.MapPath(uploadPath);


                foreach (string UploadedAttachmentfile in Directory.GetFiles(userUploadPath))
                {
                    FileInfo uploadedFile = new FileInfo(UploadedAttachmentfile);
                    long fileSize = uploadedFile.Length;
                    byteCount += fileSize;
                }
            }

            byteCount += selectedFileLength;
            if (byteCount >= maxBytes)
                maxBytesExceeded = true;

            return maxBytesExceeded;
        }

        public bool CheckUploadFiles(HttpPostedFile postedFile, ListBox ListBoxFileUpload, Label LabelMaxFiles, FileUpload FileUpload, UpdatePanel UpdatePanelFileUpload)
        {
            LabelMaxFiles.Text = string.Empty;
            UpdatePanelFileUpload.Update();

            var newUploadFileType = postedFile.ContentType.ToString();
            var newUploadName = postedFile.FileName.ToString();
            bool uploadable = true;

            bool duplicateFile = CheckFileName(newUploadName, ListBoxFileUpload);
            if (duplicateFile)
            {
                LabelMaxFiles.Text = "File is already selected.";
                ListBoxFileUpload.SelectedIndex = -1;
                UpdatePanelFileUpload.Update();
                uploadable = false;
            }
            else
            {
                bool validFileExtension = CheckFileType(newUploadFileType);
                if (!validFileExtension)
                {
                    LabelMaxFiles.Text = "Invalid file type for attachment.";
                    ListBoxFileUpload.SelectedIndex = -1;
                    UpdatePanelFileUpload.Update();
                    uploadable = false;
                }
                else
                {
                    int selectedFileLength = FileUpload.PostedFile.ContentLength;
                    bool fileSizeExceeded = GetUploadedAttachmentByteCount(selectedFileLength);
                    if (fileSizeExceeded)
                    {
                        uploadable = false;
                        int uploadFileCount = ListBoxFileUpload.Items.Count;
                        string maxFileText = uploadFileCount > 0 ? "File cannot be added. Total maximum file size exceeded." : "File cannot be added. Maximum file size exceeded.";
                        LabelMaxFiles.Text = maxFileText;

                        ListBoxFileUpload.ClearSelection();
                        UpdatePanelFileUpload.Update();
                    }
                }
            }

            return uploadable;
        }

        public void CleanUploadsFolder()
        {
            string uploadPath = GetUploadPath();

            bool userUploadFolderExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadFolderExists)
            {
                var folderPath = HttpContext.Current.Server.MapPath(uploadPath);
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    File.Delete(file);
                }
            }
        }

        public void DeleteUserUploadFolder()
        {
            string uploadPath = GetUploadPath();

            bool userUploadFolderExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadFolderExists)
            {
                var folderPath = HttpContext.Current.Server.MapPath(uploadPath);
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    File.Delete(file);
                }

                Directory.Delete(folderPath);
            }
        }

        public long GetuploadedByteCount()
        {
            long byteCount = 0;
            string uploadPath = GetUploadPath();

            bool userUploadPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadPathExists)
            {
                var userUploadPath = HttpContext.Current.Server.MapPath(uploadPath);
                foreach (string file in Directory.GetFiles(userUploadPath))
                {
                    FileInfo f = new FileInfo(file);
                    long fileSize = f.Length;
                    byteCount += fileSize;
                }
            }

            return byteCount;
        }
        public bool CheckSeverity(DropDownList SeverityDropDownList)
        {
            String sColText = "";
            if (SeverityDropDownList.Items.Count > 0)
                sColText = SeverityDropDownList.SelectedItem.Text;

            int colonIndex = 0;
            colonIndex = sColText.IndexOf(":");
            sColText = sColText.Substring(0, colonIndex);
            if (sColText == "Immediate")
                return true;
            else
                return false;
        }
    }

}
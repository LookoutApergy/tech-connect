﻿using Microsoft.Ajax.Utilities;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web; 
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Net;
using Twilio.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Tech_Connect_Survey_Forms.App_Code;

namespace Tech_Connect_Survey_Forms
{
    public partial class DHEMobile : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string userEmail = Session["s_UserEmail"] as string;

            if (!IsPostBack)
            {
                ClearColumnB();
                ClearColumnC();
                ClearColumnsDandE();
                ClearColumnF();
                ClearColumnG();
                ClearColumnH();
                hideAdditionalCommentsDiv();
                hideAttachFilesButton();
                hideFileUploadPanel();
                UpdatePanelTitle.Update();
            }
        }

        private void hideFileUploadPanel()
        {
            LabelMaxFiles.Text = string.Empty;
            FileUploadDiv.Visible = false;
            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        private void showFileUploadPanel()
        {
            FileUploadDiv.Visible = true;
            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        private void hideAttachFilesButton()
        {
            AFButtonDiv.Visible = false;
            UpdatePanelAFButton.Update();
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }

        private void showAttachFilesButton()
        {
            AFButtonDiv.Visible = true;
            UpdatePanelAFButton.Update();
        }

        /***** Column A/DEOOptions Methods and Events *****/
        protected void DEOOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            string DEOOption = DEOOptions.SelectedValue.ToString();
            TextBoxOption.Text = DEOOption;
            UpdatePanelTitle.Update();
            ClearColumnB();
            ClearColumnC();
            ClearColumnsDandE();
            ClearColumnF();
            ClearColumnG();
            ClearColumnH();

            switch (DEOOption)
            {
                case "Gas Handling":
                    LoadColB_GHSeries();
                    LoadColC_SelectionButton("GH Size", "Select Size ");
                    LoadColC_GHOptions();
                    showAdditionalCommentsDiv();
                    break;
                case "Intake":
                    LoadColB_Intake();
                    showAdditionalCommentsDiv();
                    break;
                case "Gas Separator":
                    LoadColB_GSeparator();
                    showAdditionalCommentsDiv();
                    break;
                case "Protector":
                    LoadColB_Protector();
                    showAdditionalCommentsDiv();
                    break;
                case "Pump":
                    LoadColB_PumpSizes();
                    showAdditionalCommentsDiv();
                    break;
                case "MLE":
                    LoadColB_MLESizes();
                    showAdditionalCommentsDiv();
                    break;
                case "Cable":
                    LoadColB_CableSizes();
                    showAdditionalCommentsDiv();
                    break;
                case "Motor":
                    LoadColB_MotorSizes();
                    showAdditionalCommentsDiv();
                    break;
                case "Sensor":
                    LoadColB_SensorOptions();
                    showAdditionalCommentsDiv();
                    break;
                case "Other":
                    showAdditionalCommentsDiv();
                    break;
                default:
                    clearColumns();
                    break;
            }
        }

        private void clearColumns()
        {
            ClearColumnB();
            ClearColumnC();
            ClearColumnsDandE();
            ClearColumnF();
            ClearColumnG();
            ClearColumnH();
            hideAdditionalCommentsDiv();
            hideAttachFilesButton();
            hideFileUploadPanel();
        }

        private void showAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = true;
            showAttachFilesButton();
        }

        private void hideAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = false;
            AdditionalCommentsTextBox.Text = string.Empty;
            UpdatePanelAddComments.Update();
        }

        /***** Column B Methods abd Events  *****/
        private void ClearColumnB()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "";
            OptionBDropdown.Items.Clear();
            labelB.Visible = false;
            OptionBDropdown.Visible = false;
            SizeText.Text = "";
            SizeText.Visible = false;

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_PumpSizes()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Pump Series";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Series", "Select Series"));
            OptionBDropdown.Items.Add(new ListItem("338", "338"));
            OptionBDropdown.Items.Add(new ListItem("400", "400"));
            OptionBDropdown.Items.Add(new ListItem("513", "513"));
            OptionBDropdown.Items.Add(new ListItem("538", "538"));

            OptionBDropdown.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_MLESizes()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "MLE Series";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Series", "Select Series"));
            OptionBDropdown.Items.Add(new ListItem("375", "375"));
            OptionBDropdown.Items.Add(new ListItem("420", "420"));
            OptionBDropdown.Items.Add(new ListItem("456", "456"));
            OptionBDropdown.Items.Add(new ListItem("540", "540"));

            OptionBDropdown.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_CableSizes()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Cable Size";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionBDropdown.Items.Add(new ListItem("#6", "#6"));
            OptionBDropdown.Items.Add(new ListItem("#4", "#4"));
            OptionBDropdown.Items.Add(new ListItem("#2", "#2"));
            OptionBDropdown.Items.Add(new ListItem("#10", "#1/0"));

            OptionBDropdown.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        protected void LoadColB_MotorSizes()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Motor Size";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Motor Size", "Select Motor Size"));
            OptionBDropdown.Items.Add(new ListItem("375", "375"));
            OptionBDropdown.Items.Add(new ListItem("420 PowerFit", "420 PowerFit"));
            OptionBDropdown.Items.Add(new ListItem("456", "456"));
            OptionBDropdown.Items.Add(new ListItem("540", "540"));
            OptionBDropdown.Items.Add(new ListItem("562", "562"));

            OptionBDropdown.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        protected void LoadColB_SensorOptions()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Sensor Type";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Sensor Type", "Select Sensor Type"));
            OptionBDropdown.Items.Add(new ListItem("ACE", "ACE"));
            OptionBDropdown.Items.Add(new ListItem("GRC", "GRC"));
            OptionBDropdown.Items.Add(new ListItem("SpyPro 10", "SpyPro 10"));
            OptionBDropdown.Items.Add(new ListItem("SpyPro 11", "SpyPro 11"));

            OptionBDropdown.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_GSeparator()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "GS Size";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionBDropdown.Items.Add(new ListItem("338", "338"));
            OptionBDropdown.Items.Add(new ListItem("400", "400"));
            OptionBDropdown.Items.Add(new ListItem("400UNBGas", "400UNBGas"));
            OptionBDropdown.Items.Add(new ListItem("513/538", "513/538"));
            OptionBDropdown.Visible = true;

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_Protector()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Protector Series";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Series", "Select Series"));
            OptionBDropdown.Items.Add(new ListItem("325/375", "325/375"));
            OptionBDropdown.Items.Add(new ListItem("400/456", "400/456"));
            OptionBDropdown.Items.Add(new ListItem("420", "420"));
            OptionBDropdown.Items.Add(new ListItem("538/540", "538/540"));
            OptionBDropdown.Visible = true;

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_Intake()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "Intake Size";
            labelB.Visible = true;
            SizeText.Text = "";
            SizeText.Visible = false;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionBDropdown.Items.Add(new ListItem("338", "338"));
            OptionBDropdown.Items.Add(new ListItem("400", "400"));
            OptionBDropdown.Items.Add(new ListItem("513/538", "513/538"));
            OptionBDropdown.Visible = true;

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void LoadColB_GHSeries()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "GH Series";
            labelB.Visible = true;

            OptionBDropdown.Items.Clear();
            OptionBDropdown.Visible = false;
            SizeText.Text = "400";
            SizeText.Visible = true;
            SizeText.Enabled = false;

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }
       
        protected void OptionBDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearColumnC();
            ClearColumnsDandE();
            ClearColumnF();
            ClearColumnG();
            ClearColumnH();
            string DEOOptionA = TextBoxOption.Text;
            string DEOOptionB = OptionBDropdown.SelectedValue.ToString();
            TextBox1.Text = DEOOptionB;
            UpdatePanelTitle.Update();
   
            switch (DEOOptionA)
            {
                case "Intake":
                    break;
                case "Gas Separator":
                    switch (DEOOptionB)
                    {
                        case "338":
                            LoadColC_SelectionButton("Model", "Select Model");
                            LoadColC_RotaryModel();
                            break;
                        case "400":
                            LoadColC_SelectionButton("Model", "Select Model");
                            LoadColC_BothModels();
                            break;
                        case "400UNBGas":
                            LoadColC_SelectionButton("Model", "Select Model");
                            LoadColC_TVModel(); 
                            break;
                        case "513/538":
                            LoadColC_SelectionButton("Model", "Select Model");
                            LoadColC_BothModels();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Motor":
                    switch (DEOOptionB)
                    {
                        case "375":
                            LoadColC_SelectionButton("Horsepower", "Select 375 HP");
                            LoadColC_375HPOptions();
                            break;
                        case "420 PowerFit":
                            LoadColC_SelectionButton("Horsepower", "Select 420 HP");
                            LoadColC_420HPOptions();
                            break;
                        case "456":
                            LoadColC_SelectionButton("Horsepower", "Select 456 HP");
                            LoadColC_456HPOptions();
                            break;
                        case "540":
                            LoadColC_SelectionButton("Horsepower", "Select 540 HP");
                            LoadColC_540HPOptions();
                            break;
                        case "562":
                            LoadColC_SelectionButton("Horsepower", "Select 562 HP");
                            LoadColC_562HPOptions();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Pump":
                    switch (DEOOptionB)
                    {
                        case "338":
                            LoadColC_SelectionButton("Pump Size", "Select Size");
                            LoadColC_338SOptions();
                            break;
                        case "400":
                            LoadColC_SelectionButton("Pump Size", "Select Size");
                            LoadColC_400SOptions();
                            break;
                        case "513":
                            LoadColC_SelectionButton("Pump Size", "Select Size");
                            LoadColC_513SOptions();
                            break;
                        case "538":
                            LoadColC_SelectionButton("Pump Size", "Select Size");
                            LoadColC_558SOptions();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Protector":
                    switch (DEOOptionB)
                    {
                        case "325/375":
                            LoadColC_SelectionButton("Protector Type", "Select Type");
                            LoadColC_325_375POptions();
                            break;
                        case "400/456":
                            LoadColC_SelectionButton("Protector Type", "Select Type");
                            LoadColC_400_456POptions();
                            break;
                        case "420":
                            LoadColC_SelectionButton("Protector Type", "Select Type");
                            LoadColC_420POptions();
                            break;
                        case "538/540":
                            LoadColC_SelectionButton("Protector Type", "Select Type");
                            LoadColC_538_540_POptions();
                            break;
                        default:
                            break;
                    }
                    break;
                case "MLE":
                    switch (DEOOptionB)
                    {
                        case "375":
                            LoadColC_SelectionButton("MLE Length", "Select Length");
                            LoadColC_375MLEOptions();
                            break;
                        case "420":
                            LoadColC_SelectionButton("MLE Length", "Select Length");
                            LoadColC_420MLEOptions();
                            break;
                        case "456":
                            LoadColC_SelectionButton("MLE Length", "Select Length");
                            LoadColC_456MLEOptions();
                            break;
                        case "540":
                            LoadColC_SelectionButton("MLE Length", "Select Length");
                            LoadColC_540MLEOptions();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Cable":
                    LoadColC_SelectionButton("Cable Orientation", "Select Orientation");
                    LoadColC_CableOrientationOptions();
                    break;
            }
        }

        private void LoadColC_SelectionButton(string labeltext, string selectiontext)
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = labeltext;
            labelC.Visible = true;

            UpdatePanelLabelColC.Update();
        }

        private void LoadColC_TVModel()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Model...", "Select Model..."));
            OptionCDropdown.Items.Add(new ListItem("Tandem Vortex", "Tandem Vortex"));

            RefreshColC();
        }

        private void LoadColC_BothModels()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Model", "Select Model"));
            OptionCDropdown.Items.Add(new ListItem("Rotary", "Rotary"));
            OptionCDropdown.Items.Add(new ListItem("Vortex", "Vortex"));

            RefreshColC();
        }

        private void LoadColC_RotaryModel()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Model", "Select Model"));
            OptionCDropdown.Items.Add(new ListItem("Rotary", "Rotary"));

            RefreshColC();
        }
        private void LoadColC_325_375POptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Type", "Select Type"));
            OptionCDropdown.Items.Add(new ListItem("BPBSL", "BPBSL"));
            OptionCDropdown.Items.Add(new ListItem("BSBSL", "BSBSL"));
            OptionCDropdown.Items.Add(new ListItem("LSLSL", "LSLSL"));
            OptionCDropdown.Items.Add(new ListItem("LSL ", "LSL "));
            OptionCDropdown.Items.Add(new ListItem("66L ", "66L "));
            OptionCDropdown.Items.Add(new ListItem("PSSB ", "PSSB "));

            RefreshColC();
        }
        private void LoadColC_400_456POptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Type", "Select Type"));
            OptionCDropdown.Items.Add(new ListItem("BPBSL", "BPBSL"));
            OptionCDropdown.Items.Add(new ListItem("BSBSL", "BSBSL"));
            OptionCDropdown.Items.Add(new ListItem("LSLSL", "LSLSL"));
            OptionCDropdown.Items.Add(new ListItem("LSL", "LSL"));
            OptionCDropdown.Items.Add(new ListItem("BSL", "BSL"));
            OptionCDropdown.Items.Add(new ListItem("66L", "66L"));
            OptionCDropdown.Items.Add(new ListItem("PSSB", "PSSB"));
            OptionCDropdown.Items.Add(new ListItem("PSDB", "PSDB"));

            RefreshColC();
        }
        private void LoadColC_420POptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Type", "Select Type"));
            OptionCDropdown.Items.Add(new ListItem("BPBSL", "BPBSL"));
            OptionCDropdown.Items.Add(new ListItem("BSBSL", "BSBSL"));

            RefreshColC();
        }
        private void LoadColC_538_540_POptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Type", "Select Type"));
            OptionCDropdown.Items.Add(new ListItem("BPBSL", "BPBSL"));
            OptionCDropdown.Items.Add(new ListItem("BSBSL", "BSBSL"));
            OptionCDropdown.Items.Add(new ListItem("LSLSL", "LSLSL"));
            OptionCDropdown.Items.Add(new ListItem("LSL", "LSL"));
            OptionCDropdown.Items.Add(new ListItem("BSL", "BSL"));
            OptionCDropdown.Items.Add(new ListItem("66L", "66L"));
            OptionCDropdown.Items.Add(new ListItem("PSSB", "PSSB"));
            OptionCDropdown.Items.Add(new ListItem("PSDB", "PSDB"));

            RefreshColC();
        }
        private void LoadColC_375MLEOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Length", "Select Length"));
            OptionCDropdown.Items.Add(new ListItem("20", "20"));
            OptionCDropdown.Items.Add(new ListItem("30", "30"));
            OptionCDropdown.Items.Add(new ListItem("40", "40"));
            OptionCDropdown.Items.Add(new ListItem("55", "55"));
            OptionCDropdown.Items.Add(new ListItem("70", "70"));
            OptionCDropdown.Items.Add(new ListItem("90", "90"));
            OptionCDropdown.Items.Add(new ListItem("110", "110"));
            OptionCDropdown.Items.Add(new ListItem("150", "150"));

            RefreshColC();
        }
        private void LoadColC_420MLEOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Length", "Select Length"));
            OptionCDropdown.Items.Add(new ListItem("180", "180"));

            RefreshColC();
        }
        private void LoadColC_456MLEOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Length", "Select Length"));
            OptionCDropdown.Items.Add(new ListItem("20", "20"));
            OptionCDropdown.Items.Add(new ListItem("30", "30"));
            OptionCDropdown.Items.Add(new ListItem("40", "40"));
            OptionCDropdown.Items.Add(new ListItem("55", "55"));
            OptionCDropdown.Items.Add(new ListItem("70", "70"));
            OptionCDropdown.Items.Add(new ListItem("90", "90"));
            OptionCDropdown.Items.Add(new ListItem("110", "110"));
            OptionCDropdown.Items.Add(new ListItem("130", "130"));
            OptionCDropdown.Items.Add(new ListItem("150", "150"));

            RefreshColC();
        }
        private void LoadColC_540MLEOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Length", "Select Length"));
            OptionCDropdown.Items.Add(new ListItem("40", "40"));
            OptionCDropdown.Items.Add(new ListItem("55", "55"));
            OptionCDropdown.Items.Add(new ListItem("70", "70"));
            OptionCDropdown.Items.Add(new ListItem("90", "90"));
            OptionCDropdown.Items.Add(new ListItem("120", "120"));

            RefreshColC();
        }

        private void LoadColC_CableOrientationOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Orientation", "Select Orientation"));
            OptionCDropdown.Items.Add(new ListItem("Round", "Round"));
            OptionCDropdown.Items.Add(new ListItem("Flat", "Flat"));

            RefreshColC();
        }
        private void LoadColC_GHOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionCDropdown.Items.Add(new ListItem("GKX1500", "GKX1500"));
            OptionCDropdown.Items.Add(new ListItem("GKX3000", "GKX3000"));
            OptionCDropdown.Items.Add(new ListItem("GKXHV8", "GKXHV8"));
            OptionCDropdown.Items.Add(new ListItem("GKXHV9", "GKXHV9"));

            RefreshColC();
        }
        private void LoadColC_558SOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionCDropdown.Items.Add(new ListItem("538AAL2700", "538AAL2700"));
            OptionCDropdown.Items.Add(new ListItem("538AAL4100", "538AAL4100"));
            OptionCDropdown.Items.Add(new ListItem("538AAL5900", "538AAL5900"));
            OptionCDropdown.Items.Add(new ListItem("538AAL9000", "538AAL9000"));
            OptionCDropdown.Items.Add(new ListItem("538AAL11400", "538AAL11400"));

            RefreshColC();
        }

        private void LoadColC_513SOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionCDropdown.Items.Add(new ListItem("513AAL1200", "513AAL1200"));
            OptionCDropdown.Items.Add(new ListItem("513AAL2200", "513AAL2200"));
            OptionCDropdown.Items.Add(new ListItem("513AAL2650", "513AAL2650"));
            OptionCDropdown.Items.Add(new ListItem("513AAL4000", "513AAL4000"));
            OptionCDropdown.Items.Add(new ListItem("513AAL5600", "513AAL5600"));
            OptionCDropdown.Items.Add(new ListItem("513AAL7000", "513AAL7000"));

            RefreshColC();
        }

        private void LoadColC_338SOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionCDropdown.Items.Add(new ListItem("338AAL400", "338AAL400"));
            OptionCDropdown.Items.Add(new ListItem("338AAL550", "338AAL550"));
            OptionCDropdown.Items.Add(new ListItem("338AAL900", "338AAL900"));
            OptionCDropdown.Items.Add(new ListItem("338AAL1200", "338AAL1200"));
            OptionCDropdown.Items.Add(new ListItem("338AAL2400", "338AAL2400"));

            RefreshColC();
        }

        private void LoadColC_400SOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select Size", "Select Size"));
            OptionCDropdown.Items.Add(new ListItem("400AAL150", "400AAL150"));
            OptionCDropdown.Items.Add(new ListItem("400AAL330", "400AAL330"));
            OptionCDropdown.Items.Add(new ListItem("400AAL500", "400AAL500"));
            OptionCDropdown.Items.Add(new ListItem("400AAL650", "400AAL650"));
            OptionCDropdown.Items.Add(new ListItem("400AAL800", "400AAL800"));
            OptionCDropdown.Items.Add(new ListItem("400AAL1200", "400AAL1200"));
            OptionCDropdown.Items.Add(new ListItem("400AAL1300", "400AAL1300"));
            OptionCDropdown.Items.Add(new ListItem("400AAL1750", "400AAL1750"));
            OptionCDropdown.Items.Add(new ListItem("400AAL3000", "400AAL3000"));
            OptionCDropdown.Items.Add(new ListItem("400AAL4300", "400AAL4300"));
            OptionCDropdown.Items.Add(new ListItem("400AAL5800", "400AAL5800"));
            OptionCDropdown.Items.Add(new ListItem("400UNB7.5", "400UNB7.5"));
            OptionCDropdown.Items.Add(new ListItem("400UNB17.5", "400UNB17.5"));
            OptionCDropdown.Items.Add(new ListItem("400UNB35", "400UNB35"));
            OptionCDropdown.Items.Add(new ListItem("400UNB43", "400UNB43"));
            OptionCDropdown.Items.Add(new ListItem("400UNB60", "400UNB60"));

            RefreshColC();
        }

       /***** Column C Methods and Events *****/
        private void ClearColumnC()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = String.Empty;
            labelC.Visible = false;

            OptionCDropdown.Items.Clear();
            OptionCDropdown.Visible = false;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }
        
        protected void OptionCDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearColumnF();
            ClearColumnG();
            ClearColumnH();
            string DEOOptionA = TextBoxOption.Text;
            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = OptionCDropdown.SelectedValue.ToString();
            TextBox2.Text = DEOOptionC;
            UpdatePanelTitle.Update();

            switch (DEOOptionA)
            {
                case "Protector":
                    if (DEOOptionC == "PSDB"){
                        LoadColF_TypeOptions("UL");
                    }                   
                    break;
                case "Gas Handling":
                    LoadColF_Type_HX();
                    break;
                case "Gas Separator":
                    if (DEOOptionB != "400UNBGas")
                    LoadColF_GSPosition();
                    break;
                case "MLE":
                    LoadColF_ArmorOptions();
                    break;                   
                case "Cable":
                    LoadColF_InsulationOptions();
                    break;
                case "Pump":
                    switch (DEOOptionB)
                    {
                        case "338":
                            LoadColF_PumpType_FL();
                            LoadColG_Housings();
                            break;
                        case "400":
                            switch (DEOOptionC)
                            {
                                case "400AAL150":
                                case "400AAL330":
                                case "400AAL500":
                                case "400AAL800":
                                case "400AAL1300":
                                case "400AAL5800":
                                    LoadColF_PumpType_FL();
                                    LoadColG_Housings();
                                    break;
                                case "400UNB7.5":
                                case "400UNB17.5":
                                case "400UNB35":
                                case "400UNB43":
                                case "400UNB60":
                                    LoadColF_PumpType_X();
                                    LoadColG_UNB_Housings();
                                    break;
                                default:
                                    LoadColF_PumpType_FLHX();
                                    break;
                            }
                            break;
                        case "513":
                            LoadColF_PumpType_FL();
                            LoadColG_Housings();
                            break;
                        case "538":
                            switch (DEOOptionC)
                            {
                                case "538AAL11400":
                                    LoadColF_PumpType_FL();
                                    LoadColG_Housings();
                                    break;
                                default:
                                    LoadColF_Type_HX();
                                    break;
                            }
                            break;
                    }
                    break;
                case "Motor":
                    switch (DEOOptionB)
                    {
                        case "375":
                            switch (DEOOptionC)
                            {
                                case "14":
                                    LoadColDE_TextViewOptions("481", "24", "S");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "19":
                                    LoadColDE_TextViewOptions("449", "33", "S");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "27":
                                    LoadColDE_375_27_Options();
                                    break;
                                case "36":
                                    LoadColDE_375_36_Options();
                                    break;
                                case "47":
                                    LoadColDE_TextViewOptions("886", "42", "S");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "55":
                                    LoadColDE_TextViewOptions("559", "80", "S");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "110":
                                    LoadColDE_375_110_Options();
                                    break;
                                case "11":
                                    LoadColDE_TextViewOptions("450", "19", "SUC");
                                    break;
                                case "15":
                                    LoadColDE_TextViewOptions("424", "27", "SUC");
                                    break;
                                case "21":
                                    LoadColDE_TextViewOptions("437", "38", "SUC");
                                    break;
                                case "42":
                                    LoadColDE_TextViewOptions("522", "64", "SUC");
                                    break;
                                case "84":
                                    LoadColDE_375_84_Options();
                                    break;
                                case "19.5":
                                    LoadColDE_TextViewOptions("387", "38", "SUC");
                                    break;
                                case "25.5":
                                    LoadColDE_375_25point5_Options();
                                    break;
                                case "30":
                                    LoadColDE_TextViewOptions("480", "48", "SUC");
                                    break;
                                case "60":
                                    LoadColDE_375_60_Options();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "420 PowerFit":
                            switch (DEOOptionC)
                            {
                                case "35":
                                    LoadColDE_TextViewOptions("430", "52", "S");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "52":
                                    LoadColDE_TextViewOptions("650", "52", "S");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "69":
                                    LoadColDE_TextViewOptions("865", "52", "S");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "87":
                                    LoadColDE_TextViewOptions("1080", "52", "S");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "104":
                                    LoadColDE_TextViewOptions("1295", "52", "S");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "121":
                                    LoadColDE_420_121_Options();
                                    break;
                                case "139":
                                    LoadColDE_420_139_Options();
                                    break;
                                case "156":
                                    LoadColDE_420_156_Options();
                                    break;
                                case "173":
                                    LoadColDE_420_173_Options();
                                    break;
                                case "191":
                                    LoadColDE_420_191_Options();
                                    break;
                                case "208":
                                    LoadColDE_420_208_Options();
                                    break;
                            }
                            break;
                        case "456":
                            switch (DEOOptionC)
                            {
                                case "10":
                                    LoadColDE_TextViewOptions("460", "15", "S");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "13":
                                    LoadColDE_TextViewOptions("486", "18", "S");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "15":
                                    LoadColDE_456_15_Options();
                                    break;
                                case "17":
                                    LoadColDE_TextViewOptions("523", "23", "S");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "20":
                                    LoadColDE_456_20_Options();
                                    break;
                                case "24":
                                    LoadColDE_456_24_Options();
                                    break;
                                case "25":
                                    LoadColDE_456_25_Options();
                                    break;
                                case "27":
                                    LoadColDE_456_27_Options();
                                    break;
                                case "30":
                                    LoadColDE_456_30_Options();
                                    break;
                                case "33":
                                    LoadColDE_456_33_Options();
                                    break;
                                case "36":
                                    LoadColDE_456_36_Options();
                                    break;
                                case "40":
                                    LoadColDE_456_40_Options();
                                    break;
                                case "42":
                                    LoadColDE_TextViewOptions("466", "60", "S");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "48":
                                    LoadColDE_456_48_Options();
                                    break;
                                case "50":
                                    LoadColDE_456_50_Options();
                                    break;
                                case "54":
                                    LoadColDE_456_54_Options();
                                    break;
                                case "60":
                                    LoadColDE_456_60_Options();
                                    break;
                                case "67":
                                    LoadColDE_456_67_Options();
                                    break;
                                case "72":
                                    LoadColDE_456_72_Options();
                                    break;
                                case "80":
                                    LoadColDE_456_80_Options();
                                    break;
                                case "84":
                                    LoadColDE_456_84_Options();
                                    break;
                                case "96":
                                    LoadColDE_456_96_Options();
                                    break;
                                case "100":
                                    LoadColDE_456_100_Options();
                                    break;
                                case "107":
                                    LoadColDE_456_107_Options();
                                    break;
                                case "108":
                                    LoadColDE_456_108_Options();
                                    break;
                                case "120":
                                    LoadColDE_456_120_Options();
                                    break;
                                case "132":
                                    LoadColDE_456_132_Options();
                                    break;
                                case "134":
                                    LoadColDE_456_134_Options();
                                    break;
                                case "144":
                                    LoadColDE_456_144_Options();
                                    break;
                                case "156":
                                    LoadColDE_456_156_Options();
                                    break;
                                case "161":
                                    LoadColDE_456_161_Options();
                                    break;
                                case "167":
                                    LoadColDE_456_167_Options();
                                    break;
                                case "168":
                                    LoadColDE_456_168_Options();
                                    break;
                                case "180":
                                    LoadColDE_TextViewOptions("2682", "43", "S");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "192":
                                    LoadColDE_456_192_Options();
                                    break;
                                case "200":
                                    LoadColDE_456_200_Options();
                                    break;
                                case "204":
                                    LoadColDE_456_204_Options();
                                    break;
                                case "216":
                                    LoadColDE_456_216_Options();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "540":
                            switch (DEOOptionC)
                            {
                                case "30":
                                    LoadColDE_TextViewOptions("710", "28", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "40":
                                    LoadColDE_TextViewOptions("730", "36", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "50":
                                    LoadColDE_TextViewOptions("807", "43", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "60":
                                    LoadColDE_TextViewOptions("990", "39", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "67":
                                    LoadColDE_TextViewOptions("831", "56", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "80":
                                    LoadColDE_TextViewOptions("1160", "45", "UTC");
                                    break;
                                case "100":
                                    LoadColDE_540_100_Options();
                                    break;
                                case "120":
                                    LoadColDE_540_120_Options();
                                    break;
                                case "134":
                                    LoadColDE_540_134_Options();
                                    break;
                                case "150":
                                    LoadColDE_TextViewOptions("2105", "44", "UTCL");
                                    break;
                                case "160":
                                    LoadColDE_TextViewOptions("1115", "88.5", "UTC");
                                    break;
                                case "167":
                                    LoadColDE_TextViewOptions("2466", "45", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "180":
                                    LoadColDE_540_180_Options();
                                    break;
                                case "200":
                                    LoadColDE_540_200_Options();
                                    break;
                                case "225":
                                    LoadColDE_TextViewOptions("2234", "64", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "251":
                                    LoadColDE_TextViewOptions("2395", "68", "UTCL");
                                    break;
                                case "267":
                                    LoadColDE_TextViewOptions("1268", "136", "UTCL");
                                    break;
                                case "301":
                                    LoadColDE_540_301_Options();
                                    break;
                                case "334":
                                    LoadColDE_TextViewOptions("2433", "83", "SUTCL");
                                    break;
                                case "376":
                                    LoadColDE_TextViewOptions("2540", "98", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                            }
                            break;
                        case "562":
                            switch (DEOOptionC)
                            {
                                case "60":
                                    LoadColDE_TextViewOptions("1205", "30", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "90":
                                    LoadColDE_562_90_Options();
                                    break;
                                case "120":
                                    LoadColDE_562_120_Options();
                                    break;
                                case "140":
                                    LoadColDE_TextViewOptions("1782", "48", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "148":
                                    LoadColDE_TextViewOptions("1606", "57", "U");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "150":
                                    LoadColDE_TextViewOptions("2655", "34", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "160":
                                    LoadColDE_TextViewOptions("2028", "48", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "172":
                                    LoadColDE_TextViewOptions("1876", "57", "U");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "180":
                                    LoadColDE_562_180_Options();
                                    break;
                                case "197":
                                    LoadColDE_TextViewOptions("2138", "57", "U");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "200":
                                    LoadColDE_TextViewOptions("1248", "98", "UTC");
                                    break;
                                case "206":
                                    LoadColDE_TextViewOptions("1962", "66", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "210":
                                    LoadColDE_TextViewOptions("2490", "51.5", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "220":
                                    LoadColDE_562_220_Options();
                                    break;
                                case "221":
                                    LoadColDE_TextViewOptions("2412", "57", "U");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "235":
                                    LoadColDE_TextViewOptions("2233", "66", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "240":
                                    LoadColDE_562_240_Options();
                                    break;
                                case "246":
                                    LoadColDE_TextViewOptions("1314", "116", "UTC");
                                    break;
                                case "265":
                                    LoadColDE_TextViewOptions("2525", "66", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "270":
                                    LoadColDE_TextViewOptions("2550", "64", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "271":
                                    LoadColDE_562_271_Options();
                                    break;
                                case "280":
                                    LoadColDE_TextViewOptions("2802", "61", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "294":
                                    LoadColDE_TextViewOptions("1375", "135", "UTC");
                                    break;
                                case "295":
                                    LoadColDE_TextViewOptions("1580", "116", "U");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "323":
                                    LoadColDE_562_323_Options();
                                    break;
                                case "330":
                                    LoadColDE_TextViewOptions("2725", "73.5", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "340":
                                    LoadColDE_TextViewOptions("3402", "61", "U");
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "344":
                                    LoadColDE_TextViewOptions("2951", "72", "U");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "353":
                                    LoadColDE_TextViewOptions("1652", "135", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "360":
                                    LoadColDE_TextViewOptions("3400", "64", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "390":
                                    LoadColDE_TextViewOptions("3685", "64", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "412":
                                    LoadColDE_TextViewOptions("3086", "84", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "418":
                                    LoadColDE_TextViewOptions("3582", "72", "U");
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "420":
                                    LoadColDE_TextViewOptions("3470", "73.5", "U");
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "450":
                                    LoadColDE_562_450_Options();
                                    break;
                                case "500":
                                    LoadColDE_TextViewOptions("3747", "84", "U");
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                break;
            }
        }

        private void LoadColDE_456_15_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("435", "435"));
            OptionDDropdown.Items.Add(new ListItem("655", "655"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("23", "23"));
            OptionEDropdown.Items.Add(new ListItem("16", "16"));

            refreshColDE();
        }

        private void LoadColDE_456_30_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("425", "425"));
            OptionDDropdown.Items.Add(new ListItem("750", "750"));
            OptionDDropdown.Items.Add(new ListItem("980", "980"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("44.5", "44.5"));
            OptionEDropdown.Items.Add(new ListItem("25.5", "25.5"));
            OptionEDropdown.Items.Add(new ListItem("20", "20"));

            refreshColDE();
        }

        private void LoadColDE_456_20_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("467", "467"));
            OptionDDropdown.Items.Add(new ListItem("701", "701"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("29", "29"));
            OptionEDropdown.Items.Add(new ListItem("20", "20"));

            refreshColDE();
        }

        private void LoadColDE_456_24_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("439", "439"));
            OptionDDropdown.Items.Add(new ListItem("682", "682"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("35", "35"));
            OptionEDropdown.Items.Add(new ListItem("22.5", "22.5"));

            refreshColDE();
        }

        private void LoadColDE_456_27_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", " Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("483", "483"));
            OptionDDropdown.Items.Add(new ListItem("811", "811"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("36", "36"));
            OptionEDropdown.Items.Add(new ListItem("22", "22"));

            refreshColDE();
        }

        private void LoadColDE_456_40_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("70", "70"));
            OptionDDropdown.Items.Add(new ListItem("430", "430"));
            OptionDDropdown.Items.Add(new ListItem("456", "456"));
            OptionDDropdown.Items.Add(new ListItem("804", "804"));
            OptionDDropdown.Items.Add(new ListItem("1054", "1054"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("33", "33"));
            OptionEDropdown.Items.Add(new ListItem("59", "59"));
            OptionEDropdown.Items.Add(new ListItem("56", "56"));
            OptionEDropdown.Items.Add(new ListItem("32", "32"));
            OptionEDropdown.Items.Add(new ListItem("25", "25"));

            refreshColDE();
        }
        private void refreshColDE()
        {
            OptionDDropdown.Visible = true;
            OptionEDropdown.Visible = true;
            borderDiv.Visible = true;
          
            HideColDE_VoltsAmpsText();
            ShowColDE_ToggleOptions();

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void LoadColDE_456_200_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1471", "1471"));
            OptionDDropdown.Items.Add(new ListItem("2550", "2550"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("90", "90"));
            OptionEDropdown.Items.Add(new ListItem("54", "54"));

            refreshColDE();
        }

        private void LoadColDE_456_167_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1222", "1222"));
            OptionDDropdown.Items.Add(new ListItem("2550", "2550"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("91", "91"));
            OptionEDropdown.Items.Add(new ListItem("54", "54"));

            refreshColDE();
        }

        private void LoadColDE_456_107_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("924", "924"));
            OptionDDropdown.Items.Add(new ListItem("1409", "1409"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("76", "76"));
            OptionEDropdown.Items.Add(new ListItem("50", "50"));

            refreshColDE();
        }

        private void LoadColDE_456_134_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("979", "979"));
            OptionDDropdown.Items.Add(new ListItem("1156", "1156"));
            OptionDDropdown.Items.Add(new ListItem("1490", "1490"));
            OptionDDropdown.Items.Add(new ListItem("2373", "2373"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("93", "93"));
            OptionEDropdown.Items.Add(new ListItem("75", "75"));
            OptionEDropdown.Items.Add(new ListItem("60", "60"));
            OptionEDropdown.Items.Add(new ListItem("36", "36"));

            refreshColDE();
        }
        private void LoadColDE_456_132_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1076", "1076"));
            OptionDDropdown.Items.Add(new ListItem("2635", "2635"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("78.5", "78.5"));
            OptionEDropdown.Items.Add(new ListItem("32", "32"));

            refreshColDE();
        }
        private void LoadColDE_456_144_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1174", "1174"));
            OptionDDropdown.Items.Add(new ListItem("2389", "2389"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("78.5", "78.5"));
            OptionEDropdown.Items.Add(new ListItem("38.5", "38.5"));

            refreshColDE();
        }

        private void LoadColDE_456_156_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1272", "1272"));
            OptionDDropdown.Items.Add(new ListItem("2588", "2588"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("78.5", "78.5"));
            OptionEDropdown.Items.Add(new ListItem("38.5", "38.5"));

            refreshColDE();
        }

        private void LoadColDE_456_161_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1210", "1210"));
            OptionDDropdown.Items.Add(new ListItem("1393", "1393"));
            OptionDDropdown.Items.Add(new ListItem("2415", "2415"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("89", "89"));
            OptionEDropdown.Items.Add(new ListItem("75", "75"));
            OptionEDropdown.Items.Add(new ListItem("45", "45"));

            refreshColDE();
        }

        private void LoadColDE_456_168_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1653", "1653"));
            OptionDDropdown.Items.Add(new ListItem("2508", "2508"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("65", "65"));
            OptionEDropdown.Items.Add(new ListItem("43", "43"));

            refreshColDE();
        }

        private void LoadColDE_456_192_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1890", "1890"));
            OptionDDropdown.Items.Add(new ListItem("2537", "2537"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("65", "65"));
            OptionEDropdown.Items.Add(new ListItem("48.5", "48.5"));

            refreshColDE();
        }

        private void LoadColDE_456_204_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1664", "1664"));
            OptionDDropdown.Items.Add(new ListItem("2685", "2695"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("78.5", "78.5"));
            OptionEDropdown.Items.Add(new ListItem("48.5", "48.5"));

            refreshColDE();
        }

        private void LoadColDE_456_216_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1762", "1762"));
            OptionDDropdown.Items.Add(new ListItem("2490", "2490"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("78.5", "78.5"));
            OptionEDropdown.Items.Add(new ListItem("55.5", "55.5"));

            refreshColDE();
        }

        private void LoadColDE_456_100_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1075", "1075"));
            OptionDDropdown.Items.Add(new ListItem("1103", "1103"));
            OptionDDropdown.Items.Add(new ListItem("1511", "1511"));
            OptionDDropdown.Items.Add(new ListItem("2205", "2205"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("59", "59"));
            OptionEDropdown.Items.Add(new ListItem("60", "60"));
            OptionEDropdown.Items.Add(new ListItem("45", "45"));
            OptionEDropdown.Items.Add(new ListItem("28.5", "28.5"));

            refreshColDE();
        }

        private void LoadColDE_456_120_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1125", "1125"));
            OptionDDropdown.Items.Add(new ListItem("1295", "1295"));
            OptionDDropdown.Items.Add(new ListItem("1586", "1586"));
            OptionDDropdown.Items.Add(new ListItem("2245", "2245"));
            OptionDDropdown.Items.Add(new ListItem("2598", "2598"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("70", "70"));
            OptionEDropdown.Items.Add(new ListItem("59", "59"));
            OptionEDropdown.Items.Add(new ListItem("48.5", "48.5"));
            OptionEDropdown.Items.Add(new ListItem("35", "35"));
            OptionEDropdown.Items.Add(new ListItem("29.5", "29.5"));

            refreshColDE();
        }

        private void LoadColDE_456_67_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("489", "489"));
            OptionDDropdown.Items.Add(new ListItem("877", "877"));
            OptionDDropdown.Items.Add(new ListItem("1028", "1028"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("91", "91"));
            OptionEDropdown.Items.Add(new ListItem("51", "51"));
            OptionEDropdown.Items.Add(new ListItem("42", "42"));

            refreshColDE();
        }

        private void LoadColDE_456_80_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("635", "635"));
            OptionDDropdown.Items.Add(new ListItem("801", "801"));
            OptionDDropdown.Items.Add(new ListItem("860", "860"));
            OptionDDropdown.Items.Add(new ListItem("1042", "1042"));
            OptionDDropdown.Items.Add(new ListItem("1310", "1310"));
            OptionDDropdown.Items.Add(new ListItem("1430", "1430"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("80", "80"));
            OptionEDropdown.Items.Add(new ListItem("66", "66"));
            OptionEDropdown.Items.Add(new ListItem("60", "60"));
            OptionEDropdown.Items.Add(new ListItem("49", "49"));
            OptionEDropdown.Items.Add(new ListItem("39", "39"));
            OptionEDropdown.Items.Add(new ListItem("37", "37"));

            refreshColDE();
        }

        private void LoadColDE_456_50_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("482", "482"));
            OptionDDropdown.Items.Add(new ListItem("852", "852"));
            OptionDDropdown.Items.Add(new ListItem("955", "955"));
            OptionDDropdown.Items.Add(new ListItem("1112", "1112"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("68", "68"));
            OptionEDropdown.Items.Add(new ListItem("39", "39"));
            OptionEDropdown.Items.Add(new ListItem("33", "33"));
            OptionEDropdown.Items.Add(new ListItem("31", "31"));

            refreshColDE();
        }

        private void LoadColDE_456_54_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("463", "463"));
            OptionDDropdown.Items.Add(new ListItem("829", "829"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("75", "75"));
            OptionEDropdown.Items.Add(new ListItem("42", "42"));

            refreshColDE();
        }

        private void LoadColDE_456_33_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("509", "509"));
            OptionDDropdown.Items.Add(new ListItem("852", "852"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("43", "43"));
            OptionEDropdown.Items.Add(new ListItem("26", "26"));

            refreshColDE();
        }

        private void LoadColDE_456_36_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("415", "415"));
            OptionDDropdown.Items.Add(new ListItem("901", "901"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("55.5", "55.5"));
            OptionEDropdown.Items.Add(new ListItem("25.5", "25.5"));

            refreshColDE();
        }
        private void LoadColDE_456_48_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("877", "877"));
            OptionDDropdown.Items.Add(new ListItem("1363", "1363"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("35", "35"));
            OptionEDropdown.Items.Add(new ListItem("22.5", "22.5"));

            refreshColDE();
        }

        private void LoadColDE_456_60_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));

            OptionDDropdown.Items.Add(new ListItem("745", "745"));
            OptionDDropdown.Items.Add(new ListItem("970", "970"));
            OptionDDropdown.Items.Add(new ListItem("995", "995"));
            OptionDDropdown.Items.Add(new ListItem("1330", "1330"));
            OptionDDropdown.Items.Add(new ListItem("1400", "1400"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("52", "52"));
            OptionEDropdown.Items.Add(new ListItem("39", "39"));
            OptionEDropdown.Items.Add(new ListItem("38.5", "38.5"));
            OptionEDropdown.Items.Add(new ListItem("29", "29"));
            OptionEDropdown.Items.Add(new ListItem("27.5", "27.5"));

            refreshColDE();
        }

        private void LoadColDE_456_72_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("951", "951"));
            OptionDDropdown.Items.Add(new ListItem("2044", "2044"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("48.5", "48.5"));
            OptionEDropdown.Items.Add(new ListItem("22.5", "22.5"));

            refreshColDE();
        }

        private void LoadColDE_456_84_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("968", "968"));
            OptionDDropdown.Items.Add(new ListItem("1088", "1088"));
            OptionDDropdown.Items.Add(new ListItem("2102", "2102"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("55.5", "55.5"));
            OptionEDropdown.Items.Add(new ListItem("51", "51"));
            OptionEDropdown.Items.Add(new ListItem("22.5", "22.5"));

            refreshColDE();
        }

        private void LoadColDE_456_96_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("945", "945"));
            OptionDDropdown.Items.Add(new ListItem("2202", "2202"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("65", "65"));
            OptionEDropdown.Items.Add(new ListItem("27.5", "27.5"));

            refreshColDE();
        }

        private void LoadColDE_456_108_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("881", "881"));
            OptionDDropdown.Items.Add(new ListItem("2520", "2520"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("78.5", "78.5"));
            OptionEDropdown.Items.Add(new ListItem("27.5", "27.5"));

            refreshColDE();
        }

        private void LoadColDE_456_25_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("493", "493"));
            OptionDDropdown.Items.Add(new ListItem("744", "744"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("35", "35"));
            OptionEDropdown.Items.Add(new ListItem("25", "25"));

            refreshColDE();
        }

        private void LoadColDE_375_60_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("990", "990"));
            OptionDDropdown.Items.Add(new ListItem("1020", "1020"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("45", "45"));
            OptionEDropdown.Items.Add(new ListItem("45.7", "45.7"));

            refreshColDE();
        }

        private void LoadColDE_375_25point5_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("370", "370"));
            OptionDDropdown.Items.Add(new ListItem("760", "760"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("51", "51"));
            OptionEDropdown.Items.Add(new ListItem("25", "25"));

            refreshColDE();
        }

        private void LoadColDE_375_84_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1077", "1077"));
            OptionDDropdown.Items.Add(new ListItem("1110", "1110"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("60", "60"));
            OptionEDropdown.Items.Add(new ListItem("61", "61"));

            refreshColDE();
        }

        private void LoadColDE_420_121_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1070", "1070"));
            OptionDDropdown.Items.Add(new ListItem("1515", "1515"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("73", "73"));
            OptionEDropdown.Items.Add(new ListItem("52", "52"));

            refreshColDE();
        }

        private void LoadColDE_420_139_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1225", "1225"));
            OptionDDropdown.Items.Add(new ListItem("1730", "1730"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("73", "73"));
            OptionEDropdown.Items.Add(new ListItem("52", "52"));

            refreshColDE();
        }

        private void LoadColDE_420_156_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1375", "1375"));
            OptionDDropdown.Items.Add(new ListItem("1945", "1945"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("73", "73"));
            OptionEDropdown.Items.Add(new ListItem("52", "52"));

            refreshColDE();
        }

        private void LoadColDE_420_173_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1530", "1530"));
            OptionDDropdown.Items.Add(new ListItem("2160", "2160"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("73", "73"));
            OptionEDropdown.Items.Add(new ListItem("52", "52"));

            refreshColDE();
        }

        private void LoadColDE_420_191_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1680", "1680"));
            OptionDDropdown.Items.Add(new ListItem("2380", "2380"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("73", "73"));
            OptionEDropdown.Items.Add(new ListItem("52", "52"));

            refreshColDE();
        }

        private void LoadColDE_420_208_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1835", "1835"));
            OptionDDropdown.Items.Add(new ListItem("2595", "2595"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("73", "73"));
            OptionEDropdown.Items.Add(new ListItem("52", "52"));

            refreshColDE();
        }

        private void LoadColDE_540_100_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1126", "1126"));
            OptionDDropdown.Items.Add(new ListItem("2170", "2170"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("60", "60"));
            OptionEDropdown.Items.Add(new ListItem("29", "29"));

            refreshColDE();
        }

        private void LoadColDE_540_120_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1030", "1030"));
            OptionDDropdown.Items.Add(new ListItem("1295", "1295"));
            OptionDDropdown.Items.Add(new ListItem("2165", "2165"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("73", "73"));
            OptionEDropdown.Items.Add(new ListItem("59", "59"));
            OptionEDropdown.Items.Add(new ListItem("33", "33"));

            refreshColDE();
        }

        private void LoadColDE_540_134_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1320", "1320"));
            OptionDDropdown.Items.Add(new ListItem("2466", "2466"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("69", "69"));
            OptionEDropdown.Items.Add(new ListItem("45", "45"));

            refreshColDE();
        }

        private void LoadColDE_540_180_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1275", "1275"));
            OptionDDropdown.Items.Add(new ListItem("1945", "1945"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("89", "89"));
            OptionEDropdown.Items.Add(new ListItem("59", "59"));

            refreshColDE();
        }

        private void LoadColDE_540_200_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1100", "1100"));
            OptionDDropdown.Items.Add(new ListItem("1171", "1171"));
            OptionDDropdown.Items.Add(new ListItem("1472", "1472"));
            OptionDDropdown.Items.Add(new ListItem("2140", "2140"));
            OptionDDropdown.Items.Add(new ListItem("2459", "2459"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("115", "115"));
            OptionEDropdown.Items.Add(new ListItem("112", "112"));
            OptionEDropdown.Items.Add(new ListItem("91", "91"));
            OptionEDropdown.Items.Add(new ListItem("54", "54"));
            OptionEDropdown.Items.Add(new ListItem("51", "51"));

            refreshColDE();
        }

        private void LoadColDE_540_301_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1450", "1450"));
            OptionDDropdown.Items.Add(new ListItem("2212", "2212"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("137", "137"));
            OptionEDropdown.Items.Add(new ListItem("91", "91"));

            refreshColDE();
        }

        private void LoadColDE_562_90_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1275", "1275"));
            OptionDDropdown.Items.Add(new ListItem("2125", "2125"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("43", "43"));
            OptionEDropdown.Items.Add(new ListItem("25.5", "25.5"));

            refreshColDE();
        }

        private void LoadColDE_562_120_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1524", "1524"));
            OptionDDropdown.Items.Add(new ListItem("2550", "2550"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("48", "48"));
            OptionEDropdown.Items.Add(new ListItem("28.5", "28.5"));

            refreshColDE();
        }

        private void LoadColDE_562_180_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("2292", "2292"));
            OptionDDropdown.Items.Add(new ListItem("2550", "2550"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("48", "48"));
            OptionEDropdown.Items.Add(new ListItem("43", "43"));

            refreshColDE();
        }

        private void LoadColDE_562_220_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1368", "1368"));
            OptionDDropdown.Items.Add(new ListItem("2202", "2202"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("98", "98"));
            OptionEDropdown.Items.Add(new ListItem("61", "61"));

            refreshColDE();
        }

        private void LoadColDE_562_240_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1500", "1500"));
            OptionDDropdown.Items.Add(new ListItem("2550", "2550"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("98", "98"));
            OptionEDropdown.Items.Add(new ListItem("64", "64"));

            refreshColDE();
        }

        private void LoadColDE_562_271_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1441", "1441"));
            OptionDDropdown.Items.Add(new ListItem("2321", "2321"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("116", "116"));
            OptionEDropdown.Items.Add(new ListItem("72", "72"));

            refreshColDE();
        }

        private void LoadColDE_562_323_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1506", "1506"));
            OptionDDropdown.Items.Add(new ListItem("2423", "2423"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("135", "135"));
            OptionEDropdown.Items.Add(new ListItem("84", "84"));

            refreshColDE();
        }

        private void LoadColDE_562_450_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("2655", "2655"));
            OptionDDropdown.Items.Add(new ListItem("3720", "3720"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("102.5", "102.5"));
            OptionEDropdown.Items.Add(new ListItem("73.5", "73.5"));

            refreshColDE();
        }
        
        private void LoadColC_456HPOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select 456 HP", "Select 456 HP"));
            OptionCDropdown.Items.Add(new ListItem("10", "10"));
            OptionCDropdown.Items.Add(new ListItem("13", "13"));
            OptionCDropdown.Items.Add(new ListItem("15", "15"));
            OptionCDropdown.Items.Add(new ListItem("17", "17"));
            OptionCDropdown.Items.Add(new ListItem("20", "20"));
            OptionCDropdown.Items.Add(new ListItem("24", "24"));
            OptionCDropdown.Items.Add(new ListItem("25", "25"));
            OptionCDropdown.Items.Add(new ListItem("27", "27"));
            OptionCDropdown.Items.Add(new ListItem("30", "30"));
            OptionCDropdown.Items.Add(new ListItem("33", "33"));
            OptionCDropdown.Items.Add(new ListItem("36", "36"));
            OptionCDropdown.Items.Add(new ListItem("40", "40"));
            OptionCDropdown.Items.Add(new ListItem("42", "42"));
            OptionCDropdown.Items.Add(new ListItem("48", "48"));
            OptionCDropdown.Items.Add(new ListItem("50", "50"));
            OptionCDropdown.Items.Add(new ListItem("54", "54"));
            OptionCDropdown.Items.Add(new ListItem("60", "60"));
            OptionCDropdown.Items.Add(new ListItem("67", "67"));
            OptionCDropdown.Items.Add(new ListItem("72", "72"));
            OptionCDropdown.Items.Add(new ListItem("80", "80"));
            OptionCDropdown.Items.Add(new ListItem("84", "84"));
            OptionCDropdown.Items.Add(new ListItem("96", "96"));
            OptionCDropdown.Items.Add(new ListItem("100", "100"));
            OptionCDropdown.Items.Add(new ListItem("107", "107"));
            OptionCDropdown.Items.Add(new ListItem("108", "108"));
            OptionCDropdown.Items.Add(new ListItem("120", "120"));
            OptionCDropdown.Items.Add(new ListItem("132", "132"));
            OptionCDropdown.Items.Add(new ListItem("134", "134"));
            OptionCDropdown.Items.Add(new ListItem("144", "144"));
            OptionCDropdown.Items.Add(new ListItem("156", "156"));
            OptionCDropdown.Items.Add(new ListItem("161", "161"));
            OptionCDropdown.Items.Add(new ListItem("167", "167"));
            OptionCDropdown.Items.Add(new ListItem("168", "168"));
            OptionCDropdown.Items.Add(new ListItem("180", "180"));
            OptionCDropdown.Items.Add(new ListItem("192", "192"));
            OptionCDropdown.Items.Add(new ListItem("200", "200"));
            OptionCDropdown.Items.Add(new ListItem("204", "204"));
            OptionCDropdown.Items.Add(new ListItem("216", "216"));

            RefreshColC();
        }

        private void LoadColC_375HPOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select 375 HP", "Select 375 HP"));
            OptionCDropdown.Items.Add(new ListItem("11", "11"));
            OptionCDropdown.Items.Add(new ListItem("14", "14"));
            OptionCDropdown.Items.Add(new ListItem("15", "15"));
            OptionCDropdown.Items.Add(new ListItem("19", "19"));
            OptionCDropdown.Items.Add(new ListItem("19.5", "19.5"));
            OptionCDropdown.Items.Add(new ListItem("21", "21"));
            OptionCDropdown.Items.Add(new ListItem("25.5", "25.5"));
            OptionCDropdown.Items.Add(new ListItem("27", "27"));
            OptionCDropdown.Items.Add(new ListItem("30", "30"));
            OptionCDropdown.Items.Add(new ListItem("36", "36"));
            OptionCDropdown.Items.Add(new ListItem("42", "42"));
            OptionCDropdown.Items.Add(new ListItem("47", "47"));
            OptionCDropdown.Items.Add(new ListItem("55", "55"));
            OptionCDropdown.Items.Add(new ListItem("60", "60"));
            OptionCDropdown.Items.Add(new ListItem("84", "84"));
            OptionCDropdown.Items.Add(new ListItem("110", "110"));

            RefreshColC();
        }

        private void LoadColC_420HPOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select 420 HP", "Select 420 HP"));
            OptionCDropdown.Items.Add(new ListItem("35", "35"));
            OptionCDropdown.Items.Add(new ListItem("52", "52"));
            OptionCDropdown.Items.Add(new ListItem("69", "69"));
            OptionCDropdown.Items.Add(new ListItem("87", "87"));
            OptionCDropdown.Items.Add(new ListItem("104", "104"));
            OptionCDropdown.Items.Add(new ListItem("121", "121"));
            OptionCDropdown.Items.Add(new ListItem("139", "139"));
            OptionCDropdown.Items.Add(new ListItem("156", "156"));
            OptionCDropdown.Items.Add(new ListItem("173", "173"));
            OptionCDropdown.Items.Add(new ListItem("191", "191"));
            OptionCDropdown.Items.Add(new ListItem("208", "208"));

            RefreshColC();
        }
 
        private void LoadColC_540HPOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select 540 HP", "Select 540 HP"));
            OptionCDropdown.Items.Add(new ListItem("30", "30"));
            OptionCDropdown.Items.Add(new ListItem("40", "40"));
            OptionCDropdown.Items.Add(new ListItem("50", "50"));
            OptionCDropdown.Items.Add(new ListItem("60", "60"));
            OptionCDropdown.Items.Add(new ListItem("67", "67"));
            OptionCDropdown.Items.Add(new ListItem("80", "80"));
            OptionCDropdown.Items.Add(new ListItem("100", "100"));
            OptionCDropdown.Items.Add(new ListItem("120", "120"));
            OptionCDropdown.Items.Add(new ListItem("134", "134"));
            OptionCDropdown.Items.Add(new ListItem("150", "150"));
            OptionCDropdown.Items.Add(new ListItem("160", "160"));
            OptionCDropdown.Items.Add(new ListItem("167", "167"));
            OptionCDropdown.Items.Add(new ListItem("180", "180"));
            OptionCDropdown.Items.Add(new ListItem("200", "200"));
            OptionCDropdown.Items.Add(new ListItem("225", "225"));
            OptionCDropdown.Items.Add(new ListItem("251", "251"));
            OptionCDropdown.Items.Add(new ListItem("267", "267"));
            OptionCDropdown.Items.Add(new ListItem("301", "301"));
            OptionCDropdown.Items.Add(new ListItem("334", "334"));
            OptionCDropdown.Items.Add(new ListItem("376", "376"));

            RefreshColC();
        }

        private void LoadColC_562HPOptions()
        {
            OptionCDropdown.Items.Clear();
            OptionCDropdown.Items.Add(new ListItem("Select 540 HP", "Select 540 HP"));

            OptionCDropdown.Items.Add(new ListItem("60", "60"));
            OptionCDropdown.Items.Add(new ListItem("90", "90"));
            OptionCDropdown.Items.Add(new ListItem("120", "120"));
            OptionCDropdown.Items.Add(new ListItem("140", "140"));
            OptionCDropdown.Items.Add(new ListItem("148", "148"));
            OptionCDropdown.Items.Add(new ListItem("150", "150"));
            OptionCDropdown.Items.Add(new ListItem("160", "160"));
            OptionCDropdown.Items.Add(new ListItem("172", "172"));
            OptionCDropdown.Items.Add(new ListItem("180", "180"));
            OptionCDropdown.Items.Add(new ListItem("197", "197"));
            OptionCDropdown.Items.Add(new ListItem("200", "200"));
            OptionCDropdown.Items.Add(new ListItem("206", "206"));
            OptionCDropdown.Items.Add(new ListItem("210", "210"));
            OptionCDropdown.Items.Add(new ListItem("220", "220"));
            OptionCDropdown.Items.Add(new ListItem("221", "221"));
            OptionCDropdown.Items.Add(new ListItem("235", "235"));
            OptionCDropdown.Items.Add(new ListItem("240", "240"));
            OptionCDropdown.Items.Add(new ListItem("246", "246"));
            OptionCDropdown.Items.Add(new ListItem("265", "265"));
            OptionCDropdown.Items.Add(new ListItem("270", "270"));
            OptionCDropdown.Items.Add(new ListItem("271", "271"));
            OptionCDropdown.Items.Add(new ListItem("280", "280"));
            OptionCDropdown.Items.Add(new ListItem("294", "294"));
            OptionCDropdown.Items.Add(new ListItem("295", "295"));
            OptionCDropdown.Items.Add(new ListItem("323", "323"));
            OptionCDropdown.Items.Add(new ListItem("330", "330"));
            OptionCDropdown.Items.Add(new ListItem("340", "340"));
            OptionCDropdown.Items.Add(new ListItem("344", "344"));
            OptionCDropdown.Items.Add(new ListItem("353", "353"));
            OptionCDropdown.Items.Add(new ListItem("360", "360"));
            OptionCDropdown.Items.Add(new ListItem("390", "390"));
            OptionCDropdown.Items.Add(new ListItem("412", "412"));
            OptionCDropdown.Items.Add(new ListItem("418", "418"));
            OptionCDropdown.Items.Add(new ListItem("420", "420"));
            OptionCDropdown.Items.Add(new ListItem("450", "450"));
            OptionCDropdown.Items.Add(new ListItem("500", "500"));

            RefreshColC();
        }
        
        private void RefreshColC()
        {
            OptionCDropdown.Items[0].Selected = true;
            OptionCDropdown.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        /***** Columns DE Methods and Events *****/
        private void ClearColumnsDandE()
        {
            DEOOptionsD_Div.Visible = false;
            voltsAmpsDiv.Visible = false;
            VoltsAmpsLabels.Visible = false;
            VoltsAmpsToggle.Visible = false;
            borderDiv.Visible = false;

            VoltsTextBox.Text = String.Empty;
            VoltsTextBox.Visible = false;
            AmpsTextBox.Text = String.Empty;
            AmpsTextBox.Visible = false;

            VoltsAmpsToggle.ClearSelection();
            OptionDDropdown.Items.Clear();
            OptionEDropdown.Items.Clear();
            OptionDDropdown.Enabled = false;
            OptionEDropdown.Enabled = false;

            OptionDDropdown.Visible = false;
            OptionEDropdown.Visible = false;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }
        
        protected void OptionDDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearColumnF();
            ClearColumnG();
            ClearColumnH();
            int dDropdownIndex = 0;

            dDropdownIndex = OptionDDropdown.SelectedIndex;
            OptionEDropdown.SelectedIndex = dDropdownIndex;
            UpdatePanelDDColDE.Update();

            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = TextBox2.Text;
            string DEOOptionD = OptionDDropdown.SelectedValue.ToString();
            TextBox3.Text = DEOOptionD;
            UpdatePanelTitle.Update();
        
            switch (DEOOptionB)
            {
                case "375":
                    switch (DEOOptionC)
                    {
                        case "27":
                            switch (DEOOptionD)
                            {
                                case "421":
                                    LoadColF_TypeOptions("SUC");
                                    break;
                                case "464":
                                    LoadColF_S_TypeOption();
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            LoadColF_TypeOptions("SUC");
                            break;
                    }
                    break;
                case "420 PowerFit":
                    switch (DEOOptionC)
                    {
                        case "121":
                            switch (DEOOptionD)
                            {
                                case "1515":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "139":
                            switch (DEOOptionD)
                            {
                                case "1730":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "156":
                            switch (DEOOptionD)
                            {
                                case "1945":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "173":
                            switch (DEOOptionD)
                            {
                                case "2160":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "191":
                            switch (DEOOptionD)
                            {
                                case "2380":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "208":
                            switch (DEOOptionD)
                            {
                                case "2595":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "456":
                    switch (DEOOptionC)
                    {
                        case "15":
                            LoadColF_S_TypeOption();
                            LoadColG_XSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "20":
                            LoadColF_S_TypeOption();
                            LoadColG_HSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "24":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "25":
                            LoadColF_S_TypeOption();
                            LoadColG_SSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "27":
                            LoadColF_S_TypeOption();
                            LoadColG_HSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "30":
                            if (DEOOptionD == "980")
                                LoadColF_U_TypeOption();
                            else
                                LoadColF_S_TypeOption();
                                LoadColG_XSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                       case "33":
                            LoadColF_S_TypeOption();
                            LoadColG_SSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "36":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "40":
                         switch (DEOOptionD)
                         {
                             case "70":
                                 LoadColF_TypeOptions("SU");
                                 break;
                             case "430":
                                 LoadColF_S_TypeOption();
                                 LoadColG_XSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_S_TypeOption();
                                 LoadColG_HSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                         }
                         break;
                     case "48":
                         LoadColF_U_TypeOption();
                         LoadColG_XTSeriesOption();
                         LoadColH_TandemOptions();
                         break;
                     case "50":
                         switch (DEOOptionD)
                         {
                             case "955":
                                 LoadColF_TypeOptions("UCL");
                                 break;
                             case "1112":
                                 LoadColF_TypeOptions("SU");
                                 break;
                             default:
                                 LoadColF_S_TypeOption();
                                 LoadColG_SSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                         }
                         break;
                     case "54":
                         switch (DEOOptionD)
                         {
                             case "463":
                                 LoadColF_S_TypeOption();
                                 LoadColG_HSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("SU");
                                 break;
                         }
                         break;
                     case "60":
                         switch (DEOOptionD)
                         {
                             case "745":
                                 LoadColF_S_TypeOption();
                                 LoadColG_XSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             case "995":
                             LoadColF_U_TypeOption();
                             LoadColG_XTSeriesOption();
                             LoadColH_TandemOptions();
                                break;
                             case "1400":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("SUCL");
                                 break;
                         }
                         break;
                     case "67":
                         switch (DEOOptionD)
                         {
                             case "877":
                                 LoadColF_TypeOptions("SU");
                                 break;
                             case "1028":
                                 LoadColF_TypeOptions("UL");
                                 break;
                             default:
                                 LoadColF_S_TypeOption();
                                 LoadColG_SSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                         }
                         break;
                     case "72":
                         LoadColF_U_TypeOption();
                         LoadColG_XTSeriesOption();
                         LoadColH_TandemOptions();
                         break;
                    case "80":
                         switch (DEOOptionD)
                         {
                             case "635":
                                 LoadColF_S_TypeOption();
                                 LoadColG_XSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             case "860":
                                 LoadColF_TypeOptions("SUC");
                                 break;
                             default:
                                 LoadColF_TypeOptions("SUCL");
                                 break;
                         }
                         break;
                     case "84":
                         if (DEOOptionD == "1088")
                             LoadColF_TypeOptions("UL");
                         else
                         {
                             LoadColF_U_TypeOption();
                             LoadColG_XTSeriesOption();
                             LoadColH_TandemOptions();
                         }
                         break;
                     case "96":
                         LoadColF_U_TypeOption();
                         LoadColG_XTSeriesOption();
                         LoadColH_TandemOptions();
                         break;
                     case "100":
                         if (DEOOptionD == "1103" || DEOOptionD == "1511")
                             LoadColF_TypeOptions("SUCL");
                         else
                             LoadColF_TypeOptions("UC");
                         break;
                     case "107":
                         if (DEOOptionD == "924")
                             LoadColF_TypeOptions("SUC");
                         else
                             LoadColF_TypeOptions("SUCL");
                         break;
                     case "108":
                         LoadColF_U_TypeOption();
                         LoadColG_XTSeriesOption();
                         LoadColH_TandemOptions();
                         break;
                     case "120":
                         switch (DEOOptionD)
                         {
                             case "1295":
                                 LoadColF_TypeOptions("SUC");
                                 break;
                             case "2598":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "132":
                         switch (DEOOptionD)
                         {
                             case "2635":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "134":
                         switch (DEOOptionD)
                         {
                             case "979":
                                 LoadColF_TypeOptions("SUC");
                                 break;
                             case "1156":
                                 LoadColF_TypeOptions("UC");
                                 break;
                             case "2373":
                                 LoadColF_TypeOptions("UC");
                                 break;
                             default:
                                 LoadColF_TypeOptions("SUCL");
                                 break;
                         }
                         break;
                     case "144":
                         switch (DEOOptionD)
                         {
                             case "2389":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "156":
                         switch (DEOOptionD)
                         {
                             case "2588":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "161":
                         switch (DEOOptionD)
                         {
                             case "1393":
                                 LoadColF_TypeOptions("UCL");
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "167":
                         LoadColF_TypeOptions("UC");
                         break;
                     case "168":
                         switch (DEOOptionD)
                         {
                             case "2508":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "192":
                         switch (DEOOptionD)
                         {
                             case "2537":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "200":
                         switch (DEOOptionD)
                         {
                             case "1471":
                                 LoadColF_TypeOptions("UCL");
                                 break;
                             case "2598":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "204":
                         switch (DEOOptionD)
                         {
                             case "2695":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     case "216":
                         switch (DEOOptionD)
                         {
                             case "2490":
                                 LoadColF_U_TypeOption();
                                 LoadColG_XTSeriesOption();
                                 LoadColH_TandemOptions();
                                 break;
                             default:
                                 LoadColF_TypeOptions("UC");
                                 break;
                         }
                         break;
                     default:
                         LoadColF_TypeOptions("SUCL");
                         break;
                     }
                     break;
                 case "540":
                     switch (DEOOptionC)
                     {
                         case "100":
                             LoadColF_U_TypeOption();
                             if (DEOOptionD == "1126")
                                 LoadColG_SSeriesOption();
                             else
                                 LoadColG_XSeriesOption();
                             LoadColH_TandemOptions();
                             break;
                         case "120":
                             if (DEOOptionD == "2165")
                             {
                                 LoadColF_U_TypeOption();
                                 LoadColG_XSeriesOption();
                                 LoadColH_TandemOptions();
                             }
                             else
                                 LoadColF_TypeOptions("UC");
                             break;
                         case "134":
                             if (DEOOptionD == "1320")
                                 LoadColF_TypeOptions("UL");
                             else
                             {
                                 LoadColF_U_TypeOption();
                                 LoadColG_SSeriesOption();
                                 LoadColH_TandemOptions();
                             }
                             break;
                         case "180":
                             if (DEOOptionD == "1275")
                                 LoadColF_TypeOptions("SUC");
                             else
                                 LoadColF_TypeOptions("UCL");
                             break;
                         case "200":
                             switch (DEOOptionD)
                             {
                                 case "1171":
                                 case "1472":
                                 case "2459":
                                     LoadColF_TypeOptions("UC");
                                     break;
                                 case "1100":
                                     LoadColF_TypeOptions("SUC");
                                     break;
                                 default:
                                     LoadColF_TypeOptions("UCL");
                                     break;
                             }
                             break;
                         case "301":
                             LoadColF_TypeOptions("UCL");
                             break;
                     }
                     break;
                    case "562":
                        switch (DEOOptionC)
                        {
                     case "90":
                         LoadColF_U_TypeOption();
                         LoadColG_XTSeriesOption();
                         LoadColH_TandemOptions();
                         break;
                     case "120":
                         LoadColF_U_TypeOption();
                         if (DEOOptionD == "1524")
                            LoadColG_XSeriesOption();
                         else
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                         break;
                     case "180":
                         LoadColF_U_TypeOption();
                         if (DEOOptionD == "2292")
                            LoadColG_XSeriesOption();
                         else
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                         break;
                     case "220":
                         if (DEOOptionD == "1368")
                         {
                             LoadColF_U_TypeOption();
                             LoadColG_XSeriesOption();
                             LoadColH_TandemOptions();
                         }
                         else
                             LoadColF_TypeOptions("UC");
                         break;
                     case "240":
                         LoadColF_U_TypeOption();
                         if (DEOOptionD == "1500")
                             LoadColG_XSeriesOption();
                         else
                             LoadColG_XTSeriesOption();
                         LoadColH_TandemOptions();
                         break;
                     case "271":
                         if (DEOOptionD == "2321")
                             LoadColF_U_TypeOption();
                         else
                            LoadColF_C_TypeOption();
                            LoadColG_HSeriesOption();
                            LoadColH_TandemOptions();
                         break;
                     case "323":
                         if (DEOOptionD == "2423")
                             LoadColF_U_TypeOption();
                         else
                            LoadColF_C_TypeOption();
                            LoadColG_SSeriesOption();
                            LoadColH_TandemOptions();
                         break;
                     case "450":
                         LoadColF_U_TypeOption();
                         LoadColG_XTSeriesOption();
                         LoadColH_TandemOptions();
                         break;
                }
                break;
                default:
                    break;
            }

        }
        protected void OptionEDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearColumnF();
            ClearColumnG();
            ClearColumnH();
            int dDropdownIndex = 0;

            dDropdownIndex = OptionEDropdown.SelectedIndex;
            OptionDDropdown.SelectedIndex = dDropdownIndex;
            UpdatePanelDDColDE.Update();

            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = TextBox2.Text;
            string DEOOptionD = OptionDDropdown.SelectedValue.ToString();
            TextBox3.Text = DEOOptionD;
            UpdatePanelTitle.Update();
            
            switch (DEOOptionB)
            {
                case "375":
                    switch (DEOOptionC)
                    {
                        case "27":
                            switch (DEOOptionD)
                            {
                                case "421":
                                    LoadColF_TypeOptions("SUC");
                                    break;
                                case "464":
                                    LoadColF_S_TypeOption();
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            LoadColF_TypeOptions("SUC");
                            break;
                    }
                    break;
                case "420 PowerFit":
                    switch (DEOOptionC)
                    {
                        case "121":
                            switch (DEOOptionD)
                            {
                                case "1515":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "139":
                            switch (DEOOptionD)
                            {
                                case "1730":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "156":
                            switch (DEOOptionD)
                            {
                                case "1945":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "173":
                            switch (DEOOptionD)
                            {
                                case "2160":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "191":
                            switch (DEOOptionD)
                            {
                                case "2380":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "208":
                            switch (DEOOptionD)
                            {
                                case "2595":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "456":
                    switch (DEOOptionC)
                    {
                        case "15":
                            LoadColF_S_TypeOption();
                            LoadColG_XSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "20":
                            LoadColF_S_TypeOption();
                            LoadColG_HSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "24":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "25":
                            LoadColF_S_TypeOption();
                            LoadColG_SSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "27":
                            LoadColF_S_TypeOption();
                            LoadColG_HSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "30":
                            if (DEOOptionD == "980")
                                LoadColF_U_TypeOption();
                            else
                                LoadColF_S_TypeOption();
                                LoadColG_XSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                        case "33":
                            LoadColF_S_TypeOption();
                            LoadColG_SSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "36":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "40":
                            switch (DEOOptionD)
                            {
                                case "70":
                                    LoadColF_TypeOptions("SU");
                                    break;
                                case "430":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;

                                default:
                                    LoadColF_S_TypeOption();
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                            }
                            break;
                        case "48":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "50":
                            switch (DEOOptionD)
                            {
                                case "955":
                                    LoadColF_TypeOptions("UCL");
                                    break;
                                case "1112":
                                    LoadColF_TypeOptions("SU");
                                    break;
                                default:
                                    LoadColF_S_TypeOption();
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                            }
                            break;
                        case "54":
                            switch (DEOOptionD)
                            {
                                case "463":
                                    LoadColF_S_TypeOption();
                                    LoadColG_HSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("SU");
                                    break;
                            }
                            break;
                        case "60":
                            switch (DEOOptionD)
                            {
                                case "745":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "995":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "1400":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("SUCL");
                                    break;
                            }
                            break;
                        case "67":
                            switch (DEOOptionD)
                            {
                                case "877":
                                    LoadColF_TypeOptions("SU");
                                    break;
                                case "1028":
                                    LoadColF_TypeOptions("UL");
                                    break;
                                default:
                                    LoadColF_S_TypeOption();
                                    LoadColG_SSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                            }
                            break;
                        case "72":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "80":
                            switch (DEOOptionD)
                            {
                                case "635":
                                    LoadColF_S_TypeOption();
                                    LoadColG_XSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                case "860":
                                    LoadColF_TypeOptions("SUC");
                                    break;
                                default:
                                    LoadColF_TypeOptions("SUCL");
                                    break;
                            }
                            break;
                        case "84":
                            if (DEOOptionD == "1088")
                                LoadColF_TypeOptions("UL");
                            else
                            {
                                LoadColF_U_TypeOption();
                                LoadColG_XTSeriesOption();
                                LoadColH_TandemOptions();
                            }
                            break;
                        case "96":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "100":
                            if (DEOOptionD == "1103" || DEOOptionD == "1511")
                                LoadColF_TypeOptions("SUCL");
                            else
                                LoadColF_TypeOptions("UC");
                            break;
                        case "107":
                            if (DEOOptionD == "924")
                                LoadColF_TypeOptions("SUC");
                            else
                                LoadColF_TypeOptions("SUCL");
                            break;
                        case "108":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "120":
                            switch (DEOOptionD)
                            {
                                case "1295":
                                    LoadColF_TypeOptions("SUC");
                                    break;
                                case "2598":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "132":
                            switch (DEOOptionD)
                            {
                                case "2635":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "134":
                            switch (DEOOptionD)
                            {
                                case "979":
                                    LoadColF_TypeOptions("SUC");
                                    break;
                                case "1156":
                                    LoadColF_TypeOptions("UC");
                                    break;
                                case "2373":
                                    LoadColF_TypeOptions("UC");
                                    break;
                                default:
                                    LoadColF_TypeOptions("SUCL");
                                    break;
                            }
                            break;
                        case "144":
                            switch (DEOOptionD)
                            {
                                case "2389":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "156":
                            switch (DEOOptionD)
                            {
                                case "2588":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "161":
                            switch (DEOOptionD)
                            {
                                case "1393":
                                    LoadColF_TypeOptions("UCL");
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "167":
                            LoadColF_TypeOptions("UC");
                            break;
                        case "168":
                            switch (DEOOptionD)
                            {
                                case "2508":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "192":
                            switch (DEOOptionD)
                            {
                                case "2537":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "200":
                            switch (DEOOptionD)
                            {
                                case "1471":
                                    LoadColF_TypeOptions("UCL");
                                    break;
                                case "2598":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "204":
                            switch (DEOOptionD)
                            {
                                case "2695":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        case "216":
                            switch (DEOOptionD)
                            {
                                case "2490":
                                    LoadColF_U_TypeOption();
                                    LoadColG_XTSeriesOption();
                                    LoadColH_TandemOptions();
                                    break;
                                default:
                                    LoadColF_TypeOptions("UC");
                                    break;
                            }
                            break;
                        default:
                            LoadColF_TypeOptions("SUCL");
                            break;
                    }
                    break;
                case "540":
                    switch (DEOOptionC)
                    {
                        case "100":
                            LoadColF_U_TypeOption();
                            if (DEOOptionD == "1126")
                                LoadColG_SSeriesOption();
                            else
                                LoadColG_XSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                        case "120":
                            if (DEOOptionD == "2165")
                            {
                                LoadColF_U_TypeOption();
                                LoadColG_XSeriesOption();
                                LoadColH_TandemOptions();
                            }
                            else
                                LoadColF_TypeOptions("UC");
                            break;
                        case "134":
                            if (DEOOptionD == "1320")
                                LoadColF_TypeOptions("UL");
                            else
                            {
                                LoadColF_U_TypeOption();
                                LoadColG_SSeriesOption();
                                LoadColH_TandemOptions();
                            }
                            break;
                        case "180":
                            if (DEOOptionD == "1275")
                                LoadColF_TypeOptions("SUC");
                            else
                                LoadColF_TypeOptions("UCL");
                            break;
                        case "200":
                            switch (DEOOptionD)
                            {
                                case "1171":
                                case "1472":
                                case "2459":
                                    LoadColF_TypeOptions("UC");
                                    break;
                                case "1100":
                                    LoadColF_TypeOptions("SUC");
                                    break;
                                default:
                                    LoadColF_TypeOptions("UCL");
                                    break;
                            }
                            break;
                        case "301":
                            LoadColF_TypeOptions("UCL");
                            break;
                    }
                    break;
                case "562":
                    switch (DEOOptionC)
                    {
                        case "90":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                        case "120":
                            LoadColF_U_TypeOption();
                            if (DEOOptionD == "1524")
                                LoadColG_XSeriesOption();
                            else
                                LoadColG_XTSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                        case "180":
                            LoadColF_U_TypeOption();
                            if (DEOOptionD == "2292")
                                LoadColG_XSeriesOption();
                            else
                                LoadColG_XTSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                        case "220":
                            if (DEOOptionD == "1368")
                            {
                                LoadColF_U_TypeOption();
                                LoadColG_XSeriesOption();
                                LoadColH_TandemOptions();
                            }
                            else
                                LoadColF_TypeOptions("UC");
                            break;
                        case "240":
                            LoadColF_U_TypeOption();
                            if (DEOOptionD == "1500")
                                LoadColG_XSeriesOption();
                            else
                                LoadColG_XTSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                        case "271":
                            if (DEOOptionD == "2321")
                                LoadColF_U_TypeOption();
                            else
                                LoadColF_C_TypeOption();
                                LoadColG_HSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                        case "323":
                            if (DEOOptionD == "2423")
                                LoadColF_U_TypeOption();
                            else
                                LoadColF_C_TypeOption();
                                LoadColG_SSeriesOption();
                                LoadColH_TandemOptions();
                            break;
                        case "450":
                            LoadColF_U_TypeOption();
                            LoadColG_XTSeriesOption();
                            LoadColH_TandemOptions();
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        protected void VoltsAmpsToggle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (VoltsAmpsToggle.SelectedValue.ToString() == "Volts")
            {
                OptionDDropdown.Enabled = true;
                OptionDDropdown.Attributes.CssStyle.Add("background-color", "-internal-light-dark(rgb(255, 255, 255), rgb(59, 59, 59))");
                OptionDDropdown.Attributes.CssStyle.Add("color", "darkblue");

                OptionEDropdown.Enabled = false;
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "#F8F8F8");
                OptionEDropdown.Attributes.CssStyle.Add("color", "#888888");
            }
            else if (VoltsAmpsToggle.SelectedValue.ToString() == "Amps")
            {
                OptionDDropdown.Enabled = false;
                OptionDDropdown.Attributes.CssStyle.Add("background-color", "#F8F8F8");
                OptionDDropdown.Attributes.CssStyle.Add("color", "#888888");

                OptionEDropdown.Enabled = true;
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "-internal-light-dark(rgb(255, 255, 255), rgb(59, 59, 59))");
                OptionEDropdown.Attributes.CssStyle.Add("color", "darkblue");
            }

            UpdatePanelDDColDE.Update();
        }
        
        private void LoadColDE_TextViewOptions(string volts, string amps, string tOptions)
        {
            VoltsTextBox.Text = volts;
            AmpsTextBox.Text = amps;
            borderDiv.Visible = true;
            
            HideColDE_ToggleOptions();
            ShowColDE_VoltsAmpsText();
           
            switch (tOptions)
            {
                case "S":
                    LoadColF_S_TypeOption();
                    break;
                case "U":
                    LoadColF_U_TypeOption();
                    break;
                case "UT":
                    LoadColF_TypeOptions("U");
                    break;
                case "UTC":
                    LoadColF_TypeOptions("UC");
                    break;
                case "UTL":
                    LoadColF_TypeOptions("UL");
                    break;
                case "UTCL":
                    LoadColF_TypeOptions("UCL");
                    break;
                case "SUTCL":
                    LoadColF_TypeOptions("SUCL");
                    break;
                case "SUC":
                    LoadColF_TypeOptions("SUC");
                    break;
                default:
                    break;                  
            }
        }

        private void LoadColDE_375_27_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("421", "421"));
            OptionDDropdown.Items.Add(new ListItem("464", "464"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("50", "50"));
            OptionEDropdown.Items.Add(new ListItem("46", "46"));

            refreshColDE();
        }

        private void LoadColDE_375_36_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("402", "402"));
            OptionDDropdown.Items.Add(new ListItem("450", "450"));
            OptionDDropdown.Items.Add(new ListItem("827", "827"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("67", "67"));
            OptionEDropdown.Items.Add(new ListItem("63", "63"));
            OptionEDropdown.Items.Add(new ListItem("33", "33"));

            refreshColDE();
        }

        private void LoadColDE_375_110_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem("Pick Volts", "Pick Volts"));
            OptionDDropdown.Items.Add(new ListItem("1152", "1152"));
            OptionDDropdown.Items.Add(new ListItem("1186", "1186"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem("Pick Amps", "Pick Amps"));
            OptionEDropdown.Items.Add(new ListItem("75", "75"));
            OptionEDropdown.Items.Add(new ListItem("76", "76"));

            refreshColDE();
        }

        private void HideColDE_ToggleOptions()
        {
            VoltsAmpsToggle.ClearSelection();
            VoltsAmpsToggle.Enabled = false;

            DEOOptionsD_Div.Visible = false;
            voltsAmpsDiv.Visible = false;
            VoltsAmpsToggle.Visible = false;
            OptionDDropdown.Visible = false;
            OptionEDropdown.Visible = false;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void ShowColDE_ToggleOptions()
        {
            VoltsAmpsToggle.Enabled = true;
            VoltsAmpsToggle.SelectedIndex = 0;

            OptionDDropdown.Attributes.CssStyle.Add("color", "darkblue");
            OptionDDropdown.Enabled = true;

            OptionEDropdown.Attributes.CssStyle.Add("background-color", "#F8F8F8");
            OptionEDropdown.Attributes.CssStyle.Add("color", "#888888");
            OptionEDropdown.Enabled = false;

            VoltsAmpsToggle.Visible = true;
            voltsAmpsDiv.Visible = true;
            DEOOptionsD_Div.Visible = true;
            OptionDDropdown.Visible = true;
            OptionEDropdown.Visible = true;
            UpdatePanelLabelColDE.Visible = true;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }
       
        private void HideColDE_VoltsAmpsText()
        {
            DEOOptionsD_Div.Visible = false;
            VoltsTextBox.Visible = false;
            AmpsTextBox.Visible = false;
            VoltsAmpsLabels.Visible = false;
            DEOOptionsD_Div.Visible = false;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void ShowColDE_VoltsAmpsText()
        {
            VoltsAmpsLabels.Visible = true;
            VoltsTextBox.Visible = true;
            AmpsTextBox.Visible = true;
            voltsAmpsDiv.Visible = true;
            DEOOptionsD_Div.Visible = true;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        /***** Column F Methods and Events *****/    
        private void ClearColumnF()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = String.Empty;
            TypeText.Text = String.Empty;
            OptionFDropdown.Items.Clear();

            OptionFDropdown.Visible = false;
            labelF.Visible = false;
            TypeText.Visible = false;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        protected void OptionFDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            string DEOOptionA = TextBoxOption.Text;
            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = TextBox2.Text;
            string DEOOptionD = TextBox3.Text;

            string DEOOptionF = OptionFDropdown.SelectedValue.ToString();
            TextBox7.Text = DEOOptionF;
            UpdatePanelTitle.Update();

            if (!DEOOptionF.Equals("Select Type"))
            {
                ClearColumnG();
                ClearColumnH();

                switch (DEOOptionA)
                {
                    case "Gas Handling":
                    case "Pump":
                        switch (DEOOptionC)
                        {
                            case "400UNB7.5":
                            case "400UNB17.5":
                            case "400UNB35":
                            case "400UNB43":
                            case "400UNB60":
                                LoadColG_UNB_Housings();
                                break;
                            default:
                                LoadColG_Housings();
                                break;
                        }
                        break;
                    case "Cable":
                        LoadColG_SheathOptions();
                        break;
                    case "Motor":
                        switch (DEOOptionB)
                        {
                            case "375":
                                switch (DEOOptionC)
                                {
                                    case "36":
                                        switch (DEOOptionD)
                                        {
                                            case "450":
                                                LoadColG_SSeriesOption();
                                                LoadColH_TandemOptions();
                                                break;
                                            default:
                                                LoadColG_ThreeSeriesOptions();
                                                break;
                                        }
                                        break;
                                    case "110":
                                        LoadColG_SSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    default:
                                        LoadColG_ThreeSeriesOptions();
                                        break;
                                }
                                break;
                            case "420 PowerFit":
                                LoadColG_XTSeriesOption();
                                LoadColH_TandemOptions();
                                break;
                            case "456":
                                switch (DEOOptionC)
                                {
                                    case "40":
                                        if (DEOOptionD == "70" || DEOOptionD == "430")
                                            LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "50":
                                        LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "54":
                                        LoadColG_SSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "60":
                                        LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "67":
                                        switch (DEOOptionD)
                                        {
                                            case "877":
                                                LoadColG_SSeriesOption();
                                                LoadColH_TandemOptions();
                                                break;
                                            case "1028":
                                                LoadColG_HSeriesOption();
                                                LoadColH_TandemOptions();
                                                break;
                                        }
                                        break;
                                    case "80":
                                        if (DEOOptionD == "860" || DEOOptionD == "1310")
                                            LoadColG_XSeriesOption();
                                        else
                                            LoadColG_HSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "84":
                                        if (DEOOptionD == "1088")
                                        {
                                            LoadColG_SSeriesOption();
                                            LoadColH_TandemOptions();
                                        }
                                        break;
                                    case "100":
                                        if (DEOOptionD == "1103" || DEOOptionD == "1511")
                                            LoadColG_SSeriesOption();
                                        else
                                            LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "107":
                                        LoadColG_HSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "120":
                                        if (DEOOptionD == "1586")
                                            LoadColG_XTSeriesOption();
                                        else
                                            LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "132":
                                        LoadColG_XTSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "134":
                                        if (DEOOptionD == "1156" || DEOOptionD == "2373")
                                            LoadColG_HSeriesOption();
                                        else
                                            LoadColG_SSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "144":
                                        LoadColG_XTSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "156":
                                        LoadColG_XTSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "161":
                                        LoadColG_HSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "167":
                                        LoadColG_SSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "168":
                                        LoadColG_XTSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "192":
                                        LoadColG_XTSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "200":
                                        LoadColG_SSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "204":
                                        LoadColG_XTSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "216":
                                        LoadColG_XTSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    default:
                                        LoadColG_ThreeSeriesOptions();
                                        break;
                                }
                                break;
                            case "540":
                                switch (DEOOptionC)
                                {
                                    case "80":
                                    case "120":
                                    case "150":
                                    case "160":
                                    case "180":
                                        LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "200":
                                        switch (DEOOptionD)
                                        {
                                            case "1100":
                                            case "2140":
                                                LoadColG_XSeriesOption();
                                                break;
                                            default:
                                                LoadColG_SSeriesOption();
                                                break;
                                        }
                                        LoadColH_TandemOptions();
                                        break;
                                    default:
                                        LoadColG_SSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                }
                                break;
                            case "562":
                                switch (DEOOptionC)
                                {
                                    case "200":
                                        LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "220":
                                        LoadColG_XSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "246":
                                        LoadColG_HSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    case "294":
                                        LoadColG_SSeriesOption();
                                        LoadColH_TandemOptions();
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void LoadColF_PumpType_FLHX()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Configuration";

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Select Config", "Select Config"));
            OptionFDropdown.Items.Add(new ListItem("FL: Floater Pumps", "FL: Floater Pumps"));
            OptionFDropdown.Items.Add(new ListItem("H: 1/3 AR pump", "H: 1/3 AR pump"));
            OptionFDropdown.Items.Add(new ListItem("X: 1/1 AR Pump", "X: 1/1 AR Pump"));

            labelF.Visible = true;
            OptionFDropdown.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_GSPosition()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Position";

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Select Position", "Select Position"));
            OptionFDropdown.Items.Add(new ListItem("Lower", "Lower"));
            OptionFDropdown.Items.Add(new ListItem("Center", "Center"));

            labelF.Visible = true;
            OptionFDropdown.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_TypeOptions(String optionstring)
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Type";
            TypeText.Text = "";

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Select Type", "Select Type"));

            if (optionstring.Contains("S")) OptionFDropdown.Items.Add(new ListItem("Single", "Single"));
            if (optionstring.Contains("U")) OptionFDropdown.Items.Add(new ListItem("Upper Tandem", "Upper Tandem"));
            if (optionstring.Contains("C")) OptionFDropdown.Items.Add(new ListItem("Center Tandem", "Center Tandem"));
            if (optionstring.Contains("L")) OptionFDropdown.Items.Add(new ListItem("Lower Tandem", "Lower Tandem"));

            labelF.Visible = true;
            OptionFDropdown.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }
        private void LoadColF_ArmorOptions()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Armor";
            TypeText.Text = "";

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Select Armor", "Select Armor"));

            OptionFDropdown.Items.Add(new ListItem("Galvanized", "Galvanized"));
            OptionFDropdown.Items.Add(new ListItem("SS", "SS"));
            OptionFDropdown.Items.Add(new ListItem("Monel", "Monel"));

            labelF.Visible = true;
            OptionFDropdown.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_InsulationOptions()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Insulation";
            TypeText.Text = "";

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Select Insulation", "Select Insulation"));

            OptionFDropdown.Items.Add(new ListItem("Polypropylene", "Polypropylene"));
            OptionFDropdown.Items.Add(new ListItem("EPDM", "EPDM"));

            labelF.Visible = true;
            OptionFDropdown.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColG_SheathOptions()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Sheath";
            SeriesText.Text = "";

            OptionGDropdown.Items.Clear();
            OptionGDropdown.Items.Add(new ListItem("Select Sheath", "Select Sheath"));

            OptionGDropdown.Items.Add(new ListItem("NBR", "NBR"));
            OptionGDropdown.Items.Add(new ListItem("Lead ", "Lead "));
            OptionGDropdown.Items.Add(new ListItem("Heavy Lead ", "Heavy Lead "));

            labelG.Visible = true;
            OptionGDropdown.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }
       
        private void LoadColF_S_TypeOption()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Type";
            TypeText.Text = "Single";
            TypeText.Enabled = false;
            TextBox7.Text = "Single";
            UpdatePanelTitle.Update();
            OptionFDropdown.Items.Clear();

            labelF.Visible = true;
            TypeText.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_U_TypeOption()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Type";
            TypeText.Text = "Upper Tandem";
            TypeText.Enabled = false;
            TextBox7.Text = "Upper Tandem";
            UpdatePanelTitle.Update();
            OptionFDropdown.Items.Clear();

            labelF.Visible = true;
            TypeText.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_C_TypeOption()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Type";
            TypeText.Text = "Center Tandem";
            TypeText.Enabled = false;
            TextBox7.Text = "Center Tandem";
            UpdatePanelTitle.Update();
            OptionFDropdown.Items.Clear();

            labelF.Visible = true;
            TypeText.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_Type_HX()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Configuration";

            OptionFDropdown.Items.Clear();
            OptionFDropdown.Items.Add(new ListItem("Select Config", "Select Config"));
            OptionFDropdown.Items.Add(new ListItem("H: 1/3 AR pump", "H: 1/3 AR pump"));
            OptionFDropdown.Items.Add(new ListItem("X: 1/1 AR Pump", "X: 1/1 AR Pump"));

            labelF.Visible = true;
            OptionFDropdown.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_PumpType_X()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Configuration";
            TypeText.Text = "X: 1/1 AR Pump";
            TypeText.Enabled = false;
            OptionFDropdown.Items.Clear();

            labelF.Visible = true;
            TypeText.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        private void LoadColF_PumpType_FL()
        {
            Label labelF = this.FindControl("LabelFCol") as Label;
            labelF.Text = "Configuration";
            TypeText.Text = "FL: Floater Pumps";
            TypeText.Enabled = false;
            OptionFDropdown.Items.Clear();

            labelF.Visible = true;
            TypeText.Visible = true;

            UpdatePanelLabelColF.Update();
            UpdatePanelDDColF.Update();
        }

        /***** Column G Methods and Events *****/    
        private void ClearColumnG()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = String.Empty;
            SeriesText.Text = String.Empty;
            OptionGDropdown.Items.Clear();

            labelG.Visible = false;
            SeriesText.Visible = false;
            OptionGDropdown.Visible = false;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        protected void OptionGDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            string DEOOptionA = TextBoxOption.Text;
            string DEOOptionG = OptionGDropdown.SelectedValue.ToString();
            int DEOOptionIndex = OptionGDropdown.SelectedIndex;

            if (DEOOptionIndex > 0)
            {
                ClearColumnH();
                switch (DEOOptionA)
                {
                    case "Pump":
                    case "Gas Handling":
                        LoadStagesTextBox();
                        break;
                    case "Cable":
                        LoadColH_ArmorOptions();
                        break;

                    default:
                        LoadColH_TandemOptions();
                        break;
                }
            }              
        }

        private void LoadColG_ThreeSeriesOptions()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Series";
            SeriesText.Text = "";
            SeriesText.Visible = false;

            OptionGDropdown.Items.Clear();
            OptionGDropdown.Items.Add(new ListItem("Select Series", "Select Series"));
            OptionGDropdown.Items.Add(new ListItem("S - 170°F BHT max", "S - 170°F BHT max"));
            OptionGDropdown.Items.Add(new ListItem("H - 200°F BHT max", "H - 200°F BHT max"));
            OptionGDropdown.Items.Add(new ListItem("X - 250/300°F BHT max", "X - 250/300°F BHT max"));

            labelG.Visible = true;
            OptionGDropdown.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        private void LoadColG_AllSeriesOptions()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Series";
            SeriesText.Text = "";
            SeriesText.Visible = false;

            OptionGDropdown.Items.Clear();
            OptionGDropdown.Items.Add(new ListItem("Select Series", "Select Series"));
            OptionGDropdown.Items.Add(new ListItem("S - 170°F BHT max", "S - 170°F BHT max"));
            OptionGDropdown.Items.Add(new ListItem("H - 200°F BHT max", "H - 200°F BHT max"));
            OptionGDropdown.Items.Add(new ListItem("X - 250/300°F BHT max", "X - 250/300°F BHT max"));
            OptionGDropdown.Items.Add(new ListItem("XT - 300°F BHT max", "XT - 300°F BHT max"));

            labelG.Visible = true;
            OptionGDropdown.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        private void LoadColG_SSeriesOption()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Series";
            OptionGDropdown.Items.Clear();
            OptionGDropdown.Visible = false;

            SeriesText.Text = "S - 170°F BHT Max";
            SeriesText.Enabled = false;

            labelG.Visible = true;
            SeriesText.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        private void LoadColG_HSeriesOption()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Series";
            OptionGDropdown.Items.Clear();
            OptionGDropdown.Visible = false;

            SeriesText.Text = "H - 200°F BHT Max";
            SeriesText.Enabled = false;

            labelG.Visible = true;
            SeriesText.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        private void LoadColG_XSeriesOption()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Series";
            OptionGDropdown.Items.Clear();
            OptionGDropdown.Visible = false;

            SeriesText.Text = "X - 250/300°F BHT Max";
            SeriesText.Enabled = false;

            labelG.Visible = true;
            SeriesText.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        private void LoadColG_XTSeriesOption()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Series";
            OptionGDropdown.Items.Clear();
            OptionGDropdown.Visible = false;

            SeriesText.Text = "XT - 300°F BHT Max";
            SeriesText.Enabled = false;

            labelG.Visible = true;
            SeriesText.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        /***** Column H Methods and Events *****/    
        private void ClearColumnH()
        {
            Label labelH = this.FindControl("LabelHCol") as Label;
            labelH.Text = String.Empty;
            OptionHDropdown.Items.Clear();
            TandemText.Text = String.Empty;
            StagesText.Text = String.Empty;

            labelH.Visible = false;
            OptionHDropdown.Visible = false;
            TandemText.Visible = false;
            StagesText.Visible = false;

            UpdatePanelLabelColH.Update();
            UpdatePanelDDColH.Update();
        }

        private void LoadColH_ArmorOptions()
        {
            Label labelH = this.FindControl("LabelHCol") as Label;
            //labelH.Attributes.CssStyle.Add("padding-left", "6%");
            labelH.Text = "Armor";
            TandemText.Text = "";
            TandemText.Visible = false;
            StagesText.Text = "";
            StagesText.Visible = false;

            OptionHDropdown.Items.Clear();
            OptionHDropdown.Items.Add(new ListItem("Select Armor", "Select Armor"));
            OptionHDropdown.Items.Add(new ListItem("Galvanized", "Galvanized"));
            OptionHDropdown.Items.Add(new ListItem("SS", "SS"));

            labelH.Visible = true;
            OptionHDropdown.Visible = true;

            UpdatePanelLabelColH.Update();
            UpdatePanelDDColH.Update();
        }

        private void LoadColH_TandemOptions()
        {
            Label labelH = this.FindControl("LabelHCol") as Label;
            labelH.Text = "Tandem";
            labelH.Visible = true;
            TandemText.Text = "";
            TandemText.Visible = false;
            StagesText.Text = "";
            StagesText.Visible = false;

            var selectedType = TextBox7.Text;
            if (selectedType == "Single")
            {
                OptionHDropdown.Items.Clear();
                OptionHDropdown.Visible = false;
                TandemText.Text = "No";
                TandemText.Enabled = false;
                TandemText.Visible = true;
            }
            else
            {
                OptionHDropdown.Items.Clear();
                OptionHDropdown.Items.Add(new ListItem("Select Tandem", "Select Tandem"));
                OptionHDropdown.Items.Add(new ListItem("Yes", "Yes"));
                OptionHDropdown.Items.Add(new ListItem("No", "No"));
                OptionHDropdown.Visible = true;
                TandemText.Visible = false;
            }

            UpdatePanelLabelColH.Update();
            UpdatePanelDDColH.Update();
        }

        protected void OptionHDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            string errorMsg = "";
            errorMsg = validateTextBoxes();
            if (errorMsg.Length == 0)
            {
                StringBuilder emailBody = createBody("email");
                StringBuilder emailSubject = createSubject("email");
                sendEmail(emailBody, emailSubject);

                TechConnect_Utilities util = new TechConnect_Utilities();
                bool sendText = util.CheckSeverity(SeverityDropDownList);
                if (sendText)
                {
                    StringBuilder textBody = createBody("text");
                    sendTextMsg(textBody);
                }

                UpdatePanelSubmitButton.Update();
                util.CleanUploadsFolder();

                ModalPopupLabel1.Text = "Thank you for your submission!";
                ModalPopupLabel2.Text = "A ticket will be created and sent to your email address. ";
                ModalPopupLabel3.Text = "The ticket generator will be ‘Service Desk Plus Ticketing System’.";
                if (sendText)
                    ModalPopupLabel4.Text = "A text will also be sent to a staff member for immediate assistance.";

                TextBox8.Text = "Exit";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
            else
            {
                ModalPopupLabel1.Text = "Error:";
                ModalPopupLabel2.Text = errorMsg;
                ModalPopupLabel3.Text = "";
                ModalPopupLabel4.Text = "";
                TextBox8.Text = "Stay";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
        }

        private void sendEmail(StringBuilder emailBody, StringBuilder emailSubject)
        {
            String userEmailAddress = Session["s_UserEmail"].ToString();
            String supportEmailAddress = Session["s_SupportEmail"].ToString();
            String supportEmailPassword = Session["s_SupportPswd"].ToString();
            String sendGridUserName = Session["s_SendGridUserName"].ToString();
            String sendGridPassword = Session["s_SendGridPassword"].ToString();
            String ticketEmailAddress = Session["s_TicketingEmail"].ToString();
            //String ticketEmailAddress = "tbairok@gmail.com";

            using (MailMessage message = new MailMessage(supportEmailAddress, ticketEmailAddress, emailSubject.ToString(), emailBody.ToString()))
            {
                AddAttachments(message);
                message.ReplyToList.Add(userEmailAddress);
                SmtpClient emailClient = new SmtpClient("smtp.sendgrid.net", 587);
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential(sendGridUserName, sendGridPassword);
                emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                emailClient.EnableSsl = false;

                try
                {
                    emailClient.Send(message);
                }
                catch (SmtpFailedRecipientException ex)
                {

                }
            }
        }

        private void sendTextMsg(StringBuilder textBody)
        {
            var accountSid = Session["s_TwilioAcctID"].ToString();
            var authToken = Session["s_TwilioAuthToken"].ToString();
            var fromPhoneNum = Session["s_TwilioFromPhoneNum"].ToString();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            TwilioClient.Init(accountSid, authToken);
            List<string> TextNums = new List<string>();

            string textnum1 = Session["s_TwilioFromPhoneNum1"].ToString();
            if (textnum1 != "")
                TextNums.Add(textnum1);
            string textnum2 = Session["s_TwilioFromPhoneNum2"].ToString();
            if (textnum2 != "")
                TextNums.Add(textnum2);
            string textnum3 = Session["s_TwilioFromPhoneNum3"].ToString();
            if (textnum3 != "")
                TextNums.Add(textnum3);
            string textnum4 = Session["s_TwilioFromPhoneNum4"].ToString();
            if (textnum4 != "")
                TextNums.Add(textnum4);

            foreach (var textnum in TextNums)
            {
                var messageOptions = new CreateMessageOptions(
                new PhoneNumber(textnum));

                messageOptions.From = new PhoneNumber(fromPhoneNum);
                messageOptions.Body = textBody.ToString();

                var message = MessageResource.Create(messageOptions);
                Console.WriteLine(message.Body);
            }
        }

        private StringBuilder createSubject(string messagetype)
        {
            string equipmentOption = DEOOptions.SelectedItem.Text;
            string severityLable = LabelSeverity.Text;
            string sLevel = "";

            String sColText = "";
            if (SeverityDropDownList.Items.Count > 0)
            {
                sColText = SeverityDropDownList.SelectedItem.Text;
            }
            int colonIndex = 0;
            colonIndex = sColText.IndexOf(":");
            sLevel = sColText.Substring(0, colonIndex);

            StringBuilder textSubject = new StringBuilder();
            textSubject.Append("TECH CONNECT - DHE Equipment Option: ");
            textSubject.Append(equipmentOption + "; ");
            textSubject.Append("Severity: " + sLevel);

            return textSubject;
        }

        private void AddAttachments(MailMessage message)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            string uploadPath = util.GetUploadPath();
            var folderPath = HttpContext.Current.Server.MapPath(uploadPath);

            bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadupPathExists)
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    Attachment attachment = new Attachment(file);
                    message.Attachments.Add(attachment);
                }
            }
        }

        private string validateTextBoxes()
        {
            string errMsg = "";         
            if (StagesText.Text.Length > 0)
            {              
               string stagesString = StagesText.Text;
               int stagesInt = int.Parse(stagesString);
               if (stagesInt > 0 && stagesInt < 600)
                    errMsg = ""; 
               else
                    errMsg = "Invalid 'Stages' entry. Please enter a number between 1 and 600."; 
            }

            if (errMsg == "")
            {
                if (AdditionalCommentsTextBox.Text.Length > 0)
                {
                    string commentString = AdditionalCommentsTextBox.Text;
                    Regex r = new Regex("^[a-zA-Z0-9-@':&,.!?/_$ ]*$");
                    if (!r.IsMatch(commentString))
                    {
                        char[] commentsArray = commentString.ToCharArray();
                        for (int i = 0; i < commentsArray.Length; i++)
                        {
                            string commentsChar = commentsArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character.";
                                i = commentsArray.Length;
                            }
                        }
                    }
                }
                else
                    errMsg = "Please enter 'Comments' before submitting ticket.";
            }
            return errMsg;
        }

        private StringBuilder createBody(string messagetype)
        {
            string userEmail = Session["s_UserEmail"] as string;
            int endPos = userEmail.IndexOf("@");
            string currentUser = userEmail.Substring(0, endPos);

            string bColLabel = "";
            string bColText = "";
            if (OptionBDropdown.Items.Count > 0) { if (OptionBDropdown.SelectedIndex > 0) { bColLabel = LabelBCol.Text; bColText = OptionBDropdown.SelectedItem.Text; } }
            else { if (SizeText.Text.Length > 0) { bColLabel = LabelBCol.Text; bColText = SizeText.Text; } }
            
            string cColLabel = "";
            string cColText = "";
            if (OptionCDropdown.Items.Count > 0) { if (OptionCDropdown.SelectedIndex > 0) { cColLabel = LabelCCol.Text; cColText = OptionCDropdown.Text; } }

            string vLable = "";
            string aLable = "";
            string dColText = "";
            string eColText = "";
            if (OptionDDropdown.Items.Count > 0)
            {
                if (OptionDDropdown.SelectedIndex > 0) { vLable = LabelVolts.Text; aLable = LabelAmps.Text; dColText = OptionDDropdown.SelectedItem.Text; eColText = OptionEDropdown.SelectedItem.Text; }
            }
            else
            {
                if (VoltsTextBox.Text.Length > 0) { vLable = LabelVolts.Text; dColText = VoltsTextBox.Text; aLable = LabelAmps.Text; eColText = AmpsTextBox.Text; }
            }

            string fColLable = "";
            string fColText = "";
            if (OptionFDropdown.Items.Count > 0) { if (OptionFDropdown.SelectedIndex > 0) { fColLable = LabelFCol.Text; fColText = OptionFDropdown.SelectedItem.Text; } }
            else { if (TypeText.Text.Length > 0) { fColLable = LabelFCol.Text; fColText = TypeText.Text; } }

            string gColLable = "";
            string gColText = "";
            if (OptionGDropdown.Items.Count > 0) { if (OptionGDropdown.SelectedIndex > 0) { gColLable = LabelGCol.Text; gColText = OptionGDropdown.SelectedItem.Text; } }
            else
            {
                if (SeriesText.Text.Length > 0) { gColLable = LabelGCol.Text; gColText = SeriesText.Text; }
                else { if (OptionGDropdown.Items.Count > 0) { if (OptionGDropdown.SelectedIndex > 0) { gColLable = LabelGCol.Text; gColText = OptionGDropdown.Text; } } }
            }

            string hColLable = "";
            string hColText = "";
            if (OptionHDropdown.Items.Count > 0)
            {
                if (OptionHDropdown.SelectedIndex > 0) { hColLable = LabelHCol.Text; hColText = OptionHDropdown.SelectedItem.Text; }
            }
            else
            {
                if (TandemText.Text.Length > 0) { hColLable = LabelHCol.Text; hColText = TandemText.Text; }
                else if (StagesText.Text.Length > 0) { hColLable = LabelHCol.Text; hColText = StagesText.Text; }
            }

            string addComentsLable = LabelAddComments.Text;
            string addComentsText = "";
            if (AdditionalCommentsTextBox.Text.Length > 0) addComentsText = AdditionalCommentsTextBox.Text;

            string severityLable = LabelSeverity.Text;
            string sColText = "";
            if (SeverityDropDownList.Items.Count > 0) sColText = SeverityDropDownList.SelectedItem.Text;

            StringBuilder emailBody = new StringBuilder();
            string equipmentOption = DEOOptions.SelectedItem.Text;

            if (messagetype == "email")
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("");

                emailBody.AppendLine("Requester's Name: " + "\t" + currentUser);
                emailBody.AppendLine("Requester's Email: " + "\t" + "  " + userEmail);

                emailBody.AppendLine("");
                emailBody.AppendLine("Equipment Option: " + "\t" + equipmentOption);

                if (bColText.Length > 0)
                {
                    switch (bColLabel)
                    {
                        case "Gas Handling Series":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + bColText);
                            break;
                        case "MLE Series":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "      " + bColText);
                            break;
                        case "Sensor Type":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "     " + bColText);
                            break;
                        case "Pump Series":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "    " + bColText);
                            break;
                        case "GS Size":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "\t" + "  " + bColText);
                            break;
                        case "Protector Series":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "   " + bColText);
                            break;
                        case "Motor Size":
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "       " + bColText);
                            break;
                        default:
                            emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + "\t" + bColText);
                            break;
                    }
                }
                
                if (cColText.Length > 0)
                {
                    switch (cColLabel)
                    {
                        case "GH Size":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "\t" + " " + cColText);
                            break;
                        case "Cable Orientation":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "   " + cColText);
                            break;
                        case "MLE Length":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "     " + cColText);
                            break;
                        case "Pump Size":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "      " + cColText);
                            break;
                        case "Model":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "\t" + "\t" + "  " + cColText);
                            break;
                        case "Protector Type":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "    " + cColText);
                            break;
                        case "Horsepower":
                            emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + "    " + cColText);
                            break;
                        default:
                            emailBody.AppendLine(cColLabel + ": " + "\t" + " " + cColText);
                            break;
                    }

                }

                if (dColText.Length > 0 & eColText.Length > 0) emailBody.AppendLine(vLable + ": " + "\t" + "\t" + dColText + "\t" + "\t" + aLable + ": " + "\t" + eColText);

                if (fColText.Length > 0)
                {
                    switch (fColLable)
                    {
                        case "Configuration":
                            emailBody.AppendLine(fColLable + ": " + "\t" + "\t" + "     " + fColText);
                            break;
                        case "Armor":
                            emailBody.AppendLine(fColLable + ": " + "\t" + "\t" + "\t" + "\t" + "  " + fColText);
                            break;
                        case "Position":
                            emailBody.AppendLine(fColLable + ": " + "\t" + "\t" + "\t" + " " + fColText);
                            break;
                        case "Type":
                            emailBody.AppendLine(fColLable + ": " + "\t" + "\t" + "\t" + "\t" + "   " + fColText);
                            break;
                        default:
                            emailBody.AppendLine(fColLable + ": " + "\t" + "\t" + "\t" + " " + fColText);
                            break;
                    }
                }

                if (gColText.Length > 0)
                {
                    switch (gColLable)
                    {
                        case "Housing":
                            emailBody.AppendLine(gColLable + ": " + "\t" + "\t" + "        " + gColText);
                            break;
                        case "Series":
                            emailBody.AppendLine(gColLable + ": " + "\t" + "\t" + "\t" + "   " + gColText);
                            break;
                        default:
                            emailBody.AppendLine(gColLable + ": " + "\t" + "\t" + "\t" + "  " + gColText);
                            break;
                    }
                }

                if (hColText.Length > 0)
                {
                    switch (hColLable)
                    {
                        case "Stages":
                            emailBody.AppendLine(hColLable + ": " + "\t" + "\t" + "\t" + "  " + hColText);
                            break;
                        case "Tandem":
                            emailBody.AppendLine(hColLable + ": " + "\t" + "\t" + "       " + hColText);
                            break;
                        default:
                            emailBody.AppendLine(hColLable + ": " + "\t" + "\t" + "\t" + "\t" + "  " + hColText);
                            break;
                    }
                }

                emailBody.AppendLine("Comments: " + "\t" + "  " + addComentsText);
                emailBody.AppendLine("Severity: " + "\t" + "\t" + sColText);
            }
            else
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("Severity: Immediate");
                emailBody.AppendLine("");
                emailBody.AppendLine("Requester's Name: " + currentUser);
                emailBody.AppendLine("Requester's Email: " + userEmail);
                emailBody.AppendLine("Equipment Option: " + equipmentOption);

                if (bColText.Length > 0) emailBody.AppendLine(bColLabel + ": " + bColText);
                if (cColText.Length > 0) emailBody.AppendLine(cColLabel + ": " + cColText);
                if (dColText.Length > 0 & eColText.Length > 0) emailBody.AppendLine(vLable + ": " + dColText + "; " + aLable + ": " + eColText);
                if (fColText.Length > 0) emailBody.AppendLine(fColLable.Trim() + ": " + fColText.Trim());
                if (gColText.Length > 0) emailBody.AppendLine(gColLable + ": " + gColText);
                if (hColText.Length > 0) emailBody.AppendLine(hColLable + ": " + hColText);

                emailBody.AppendLine("Comments: " + addComentsText);
            }
            return emailBody;
        }

        protected void AFButton_Click(object sender, EventArgs e)
        {
            LabelMaxFiles.Text = String.Empty;
            UpdatePanelAFButton.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();

            if (FileUploadDiv.Visible == false)
            {
                ListBoxFileUpload.Items.Clear();
                FileUploadDiv.Visible = true;
            }
            else
            {
                FileUploadDiv.Visible = false;
                UpdatePanelFileUpload.Update();
            }

            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            var postedFile = FileUpload.PostedFile;
            var newAttachment = FileUpload.PostedFile.FileName.ToString();

            TechConnect_Utilities util = new TechConnect_Utilities();
            bool canUpload = util.CheckUploadFiles(postedFile, ListBoxFileUpload, LabelMaxFiles, FileUpload, UpdatePanelFileUpload);
            if (canUpload)
            {
                ListBoxFileUpload.Items.Add(new ListItem(newAttachment, newAttachment));
                ListBoxFileUpload.SelectedIndex = -1;

                UploadLabel.Visible = true;
                string uploadPath = util.GetUploadPath();

                bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
                if (!userUploadupPathExists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(uploadPath));

                FileUpload.PostedFile.SaveAs(Server.MapPath(uploadPath + newAttachment));
            }

            for (int i = 0; i < ListBoxFileUpload.Items.Count; i++)
            {
                string fileName = ListBoxFileUpload.Items[i].Text;
                TableRow row = new TableRow();
                TableItemStyle rowStyle = new TableItemStyle();
                rowStyle.Height = Unit.Pixel(25);
                row.ApplyStyle(rowStyle);
                TableCell cell1 = new TableCell();
                cell1.ApplyStyle(rowStyle);
                cell1.Text = fileName;
                row.Cells.Add(cell1);
                SelectedFilesTable.Rows.Add(row);
            }
            UpdatePanelFileUpload.Update();
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            int selectedFileIndex = ListBoxFileUpload.SelectedIndex;
            if (selectedFileIndex == -1) LabelMaxFiles.Text = "Please select file to delete.";
            else
            {
                LabelMaxFiles.Text = "";
                string fileName = ListBoxFileUpload.SelectedValue;
                ListBoxFileUpload.Items.RemoveAt(selectedFileIndex);

                string FileToDelete;
                TechConnect_Utilities util = new TechConnect_Utilities();
                string uploadPath = util.GetUploadPath();

                FileToDelete = Server.MapPath(uploadPath + fileName);
                File.Delete(FileToDelete);
            }

            for (int i = 0; i < ListBoxFileUpload.Items.Count; i++)
            {
                string fileName = ListBoxFileUpload.Items[i].Text;
                TableRow row = new TableRow();
                TableItemStyle rowStyle = new TableItemStyle();
                rowStyle.Height = Unit.Pixel(25);
                row.ApplyStyle(rowStyle);
                TableCell cell1 = new TableCell();
                cell1.ApplyStyle(rowStyle);
                cell1.Text = fileName;
                row.Cells.Add(cell1);
                SelectedFilesTable.Rows.Add(row);
            }
            if (ListBoxFileUpload.Items.Count == 0)
            {
                UploadLabel.Visible = false;
            }
            UpdatePanelFileUpload.Update();
        }

        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            ListBoxFileUpload.Items.Clear();
            UpdatePanelFileUpload.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }

        private void LoadColG_UNB_Housings()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Housing";
            labelG.Visible = true;

            OptionGDropdown.Items.Clear();
            OptionGDropdown.Items.Add(new ListItem("Select Housing...", "Select Housing..."));
            OptionGDropdown.Items[0].Attributes.Add("style", "text-align:left; padding-left:2%");
            OptionGDropdown.Items.Add(new ListItem(" 1", "1"));
            OptionGDropdown.Items.Add(new ListItem(" 3", "3"));
            OptionGDropdown.Items.Add(new ListItem(" 5", "5"));
            OptionGDropdown.Items.Add(new ListItem(" 7", "7"));
            OptionGDropdown.Items.Add(new ListItem(" 9", "9"));
            OptionGDropdown.Items.Add(new ListItem("11", "11"));
            OptionGDropdown.Items.Add(new ListItem("13", "13"));
            OptionGDropdown.Items.Add(new ListItem("15", "15"));

            RefreshColG();
        }

        private void LoadColG_Housings()
        {
            Label labelG = this.FindControl("LabelGCol") as Label;
            labelG.Text = "Housing";
            labelG.Visible = true;

            OptionGDropdown.Items.Clear();
            OptionGDropdown.Items.Add(new ListItem("Select Housing", "Select Housing"));
            OptionGDropdown.Items[0].Attributes.Add("style", "text-align:left; padding-left:2%");
            OptionGDropdown.Items.Add(new ListItem(" 2", "2"));
            OptionGDropdown.Items.Add(new ListItem(" 4", "4"));
            OptionGDropdown.Items.Add(new ListItem(" 6", "6"));
            OptionGDropdown.Items.Add(new ListItem(" 8", "8"));
            OptionGDropdown.Items.Add(new ListItem("10", "10"));
            OptionGDropdown.Items.Add(new ListItem("12", "12"));

            RefreshColG();
        }

        private void RefreshColG()
        {
            OptionGDropdown.Items[0].Selected = true;
            OptionGDropdown.Visible = true;

            UpdatePanelLabelColG.Update();
            UpdatePanelDDColG.Update();
        }

        private void LoadStagesTextBox()
        {
            Label labelH = this.FindControl("LabelHCol") as Label;
            labelH.Text = "Stages";
            labelH.Visible = true;
            StagesText.Visible = true;

            StagesText.Focus();
            UpdatePanelLabelColH.Update();
            UpdatePanelDDColH.Update();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void HomeButton_Click(object sender, EventArgs e)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.DeleteUserUploadFolder();

            var TechConnectHomeURL = Session["DefaultUrl"].ToString();
            Response.Redirect(TechConnectHomeURL, false);
        }

        protected void StagesText_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
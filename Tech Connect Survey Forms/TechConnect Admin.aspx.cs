﻿using System;
using Microsoft.Ajax.Utilities;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Security.Cryptography;
using Tech_Connect_Survey_Forms.Properties;

namespace Tech_Connect_Survey_Forms
{
    public partial class TechConnect_Admin : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeSessionParameters();
            }

        }

        private void InitializeSessionParameters()
        {
            Session["s_encryptedUserEmail"] = Request.QueryString["email"] ?? "";
            Session["s_encryptedAppPassword"] = Request.QueryString["password"] ?? "";
            Session["s_EncryptionKey"] = Properties.Settings.Default.ParamEncryptionKey;
            Session["s_TC_Password"] = Properties.Settings.Default.ApplicationPassword;
            Session["DefaultUrl"] = HttpContext.Current.Request.Url.AbsoluteUri;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetContactData();

                String encryptedEmail = Session["s_encryptedUserEmail"].ToString() ?? "";
                String encryptedPassword = Session["s_encryptedAppPassword"].ToString() ?? "";
                if (encryptedEmail != "" && encryptedPassword != "")
                {
                    String decryptedEmail = DecryptStringAES(encryptedEmail.Trim());
                    Session["s_UserEmail"] = decryptedEmail;

                    String decryptedPassword = DecryptStringAES(encryptedPassword.Trim());
                    String techConnectPswd = Session["s_TC_Password"].ToString();

                    if (decryptedPassword == techConnectPswd)
                    {
                        Session["s_ValidUser"] = "true";
                        Session["s_userEmail"] = decryptedEmail;
                    }
                    else
                    {
                        Session["s_ValidUser"] = "false";
                        Session["s_userEmail"] = "";
                    }
                }
                else
                {
                    Session["s_ValidUser"] = "false";
                    Session["s_userEmail"] = "";
                }

                UserName.Attributes.Add("autocomplete", "off");
                Password.Attributes.Add("autocomplete", "off");
                UserName.Attributes["value"] = "";
                Password.Attributes["value"] = "";
                Label failureText = (Label)this.FindControl("FailureText");
                failureText.Text = " ";
                UpdatePanelUserName.Update();
                UpdatePanelPassword.Update();
                UpdatePanelFailureText.Update();
            }
        }

        private void GetContactData()
        {
            GetContact1Data();
            GetContact2Data();
            GetContact3Data();
            GetContact4Data();
        }

        private void GetContact1Data()
        {
            var twilioTextName1 = Properties.Settings.Default.TwilioTextName1;
            Name1.Text = (twilioTextName1 == "none") ? "" : twilioTextName1;
            var twilioTextNum1 = Properties.Settings.Default.TwilioTextNum1;
            PhoneNum1.Text = (twilioTextNum1 == "none" || twilioTextNum1 == "") ? "" : FormatCellNum(twilioTextNum1);
            var ttNumActive1 = Properties.Settings.Default.TwilioNum1Active;
            Contact1Toggle.SelectedValue = ttNumActive1;

            ContactName1.Update();
            ContactPhone1.Update();
            ContactToggle1.Update();
        }

        private void GetContact2Data()
        {
            var twilioTextName2 = Properties.Settings.Default.TwilioTextName2;
            Name2.Text = (twilioTextName2 == "none") ? "" : twilioTextName2;
            var twilioTextNum2 = Properties.Settings.Default.TwilioTextNum2;
            PhoneNum2.Text = (twilioTextNum2 == "none" || twilioTextNum2 == "") ? "" : FormatCellNum(twilioTextNum2);
            var ttNumActive2 = Properties.Settings.Default.TwilioNum2Active;
            Contact2Toggle.SelectedValue = ttNumActive2;

            ContactName2.Update();
            ContactPhone2.Update();
            ContactToggle2.Update();
        }

        private void GetContact3Data()
        {
            var twilioTextName3 = Properties.Settings.Default.TwilioTextName3;
            Name3.Text = (twilioTextName3 == "none") ? "" : twilioTextName3;
            var twilioTextNum3 = Properties.Settings.Default.TwilioTextNum3;
            PhoneNum3.Text = (twilioTextNum3 == "none" || twilioTextNum3 == "") ? "" : FormatCellNum(twilioTextNum3);
            var ttNumActive3 = Properties.Settings.Default.TwilioNum3Active;
            Contact3Toggle.SelectedValue = ttNumActive3;

            ContactName3.Update();
            ContactPhone3.Update();
            ContactToggle3.Update();
        }

        private void GetContact4Data()
        {
            var twilioTextName4 = Properties.Settings.Default.TwilioTextName4;
            Name4.Text = (twilioTextName4 == "none") ? "" : twilioTextName4;
            var twilioTextNum4 = Properties.Settings.Default.TwilioTextNum4;
            PhoneNum4.Text = (twilioTextNum4 == "none" || twilioTextNum4 == "") ? "" : FormatCellNum(twilioTextNum4);
            var ttNumActive4 = Properties.Settings.Default.TwilioNum4Active;
            Contact4Toggle.SelectedValue = ttNumActive4;

            ContactName4.Update();
            ContactPhone4.Update();
            ContactToggle4.Update();
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");

            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");

            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("key");

            string plaintext = null;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        private string DecryptStringAES(string encryptedValue)
        {
            String encryptionKey = Session["s_EncryptionKey"].ToString();
            var keybytes = Encoding.UTF8.GetBytes(encryptionKey);
            var iv = Encoding.UTF8.GetBytes(encryptionKey);
            int encryptedLength = encryptedValue.Length;

            //DECRYPT FROM CRIPTOJS
            encryptedValue = encryptedValue.Replace(' ', '+');
            var encrypted = Convert.FromBase64String(encryptedValue);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);

            return decriptedFromJavascript;
        }

        private void GetContact(string TextName, string TextNum, string TextNumActive, TextBox Name, TextBox PhoneNum, RadioButtonList ToggleName)
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");
            String settingName = "";
            String settingValue = "";
            string[] textContact = new string[] { "", "", "" };

            XmlReader xmlReader = XmlReader.Create(pathName);
            while (xmlReader.Read())
            {
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "setting"))
                {
                    if (xmlReader.HasAttributes)
                    {
                        settingName = xmlReader.GetAttribute("name");
                        if (settingName == TextName)
                        {
                            xmlReader.ReadToFollowing("value");
                            settingValue = xmlReader.ReadElementContentAsString();
                            textContact[0] = settingValue;
                        }
                        if (settingName == TextNum)
                        {
                            xmlReader.ReadToFollowing("value");
                            settingValue = xmlReader.ReadElementContentAsString();
                            textContact[1] = settingValue;
                        }
                        if (settingName == TextNumActive)
                        {
                            xmlReader.ReadToFollowing("value");
                            settingValue = xmlReader.ReadElementContentAsString();
                            textContact[2] = settingValue;
                        }
                    }
                }
            }

            if (textContact[0].Length > 0)
            {
                Name.Text = textContact[0];
                string textNum1 = textContact[1];
                string formattedCell = FormatCellNum(textNum1);
                PhoneNum.Text = formattedCell;
                string active1 = textContact[2];
                ToggleName.SelectedValue = active1;
            }
            else
            {
                Name.Text = "";
                PhoneNum.Text = "";
                ToggleName.SelectedValue = "No";
            }

            xmlReader.Close();
        }

        private string FormatCellNum(string textNum)
        {
            string formattedCell = "";
            textNum = textNum.Substring(2);
            string cellAreaCode = textNum.Substring(0, 3);
            string cellPrefix = textNum.Substring(3, 3);
            string cellNum = textNum.Substring(6, 4);
            formattedCell = "(" + cellAreaCode + ")" + " " + cellPrefix + "-" + cellNum;

            return formattedCell;
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
            {
                String userName = UserName.Text;
                String adminPassWord = Password.Text;

                String userEmail = Session["s_userEmail"].ToString() ?? "";
                //String userEmail = "teri.bair@apergy.com";
                String TechConnectAdminPassword = Properties.Settings.Default.AdminPassword;

                Label failureText = (Label)this.FindControl("FailureText");
                if (userName == userEmail && adminPassWord == TechConnectAdminPassword)
                {
                    failureText.Text = "";
                    LoginDiv.Attributes.CssStyle.Add("display", "none");
                    TextboxesDiv.Attributes.CssStyle.Add("display", "block");
                    MainDiv.Attributes.CssStyle.Add("opacity", "1.0");
                }
                else
                {
                    if (userName == "")
                        failureText.Text = "'User Email' is required.";
                    else if (userName != userEmail)
                        failureText.Text = "'User Email' is incorrect. Please re-enter.";
                    else if (adminPassWord == "")
                        failureText.Text = "'Admin Password' is required.";
                    else if (adminPassWord != TechConnectAdminPassword)
                        failureText.Text = "'Admin Password' is incorrect. Please re-enter.";
                }

                UpdatePanelFailureText.Update();
            }
        }

        private string ValidateCellNum(string cellNum, TextBox phoneNum)
        {
            string errMsg = "";
            string formattedPhoneNumber = "";
            Regex phoneRegex =
            new Regex(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");

            if (phoneRegex.IsMatch(cellNum))
            {
                formattedPhoneNumber =
                    phoneRegex.Replace(cellNum, "($1) $2-$3");
            }
            else
                errMsg = "invalid phone number. Please re-enter.";
            
            phoneNum.Text = formattedPhoneNumber;
            return errMsg;
        }

        private string validateContactName(string name)
        {
            string errMsg = "";
            if (errMsg == "")
            {
                if (name.Length > 0)
                {
                    string commentString = name;
                    Regex r = new Regex("^[a-zA-Z0-9-@':&,.!?/_$ ]*$");
                    if (!r.IsMatch(commentString))
                    {
                        char[] commentsArray = commentString.ToCharArray();
                        for (int i = 0; i < commentsArray.Length; i++)
                        {
                            string commentsChar = commentsArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character from 'Contact Name'.";
                                i = commentsArray.Length;
                            }
                        }
                    }
                }
                else
                    errMsg = "Please enter 'Contact Name'.";
            }
            return errMsg;
        }

        private bool SaveContact(String TextName, String TextNum, String NumActive, String newName, String newNum, String newActive)                              
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");
            string formattedTextNum = newNum.Replace("(", "").Replace(")","").Replace("-","").Replace(" ","");
            formattedTextNum = "+1" + formattedTextNum;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathName);
            XmlNodeList userNodes = xmlDoc.SelectNodes("//Tech_Connect_Survey_Forms.Properties.Settings/setting");
            foreach (XmlNode userNode in userNodes)
            {
                string name = userNode.Attributes["name"].Value;

                if (name == TextName)
                {
                    XmlNode nameSettingVal = userNode.FirstChild;
                    string oldName = nameSettingVal.InnerText;
                    nameSettingVal.InnerText = newName;
                }
                if (name == TextNum)
                {
                    XmlNode numSettingVal = userNode.FirstChild;
                    string oldNum = numSettingVal.InnerText;
                    numSettingVal.InnerText = formattedTextNum;
                }
                if (name == NumActive)
                {
                    XmlNode numActiveSettingVal = userNode.FirstChild;
                    string oldActive = numActiveSettingVal.InnerText;
                    numActiveSettingVal.InnerText = newActive;
                }
            }

            try
            {
                xmlDoc.Save(pathName);
            }
            catch (Exception e)
            {
                return false;
            }      
            return true;

        }

        private bool ClearContact(String TextName, String TextNum, String NumActive)
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathName);
            XmlNodeList userNodes = xmlDoc.SelectNodes("//Tech_Connect_Survey_Forms.Properties.Settings/setting");
            foreach (XmlNode userNode in userNodes)
            {
                string name = userNode.Attributes["name"].Value;

                if (name == TextName)
                {
                    XmlNode nameSettingVal = userNode.FirstChild;
                    string oldName = nameSettingVal.InnerText;
                    nameSettingVal.InnerText = "none";
                }
                if (name == TextNum)
                {
                    XmlNode numSettingVal = userNode.FirstChild;
                    string oldNum = numSettingVal.InnerText;
                    numSettingVal.InnerText = "none";
                }
                if (name == NumActive)
                {
                    XmlNode numActiveSettingVal = userNode.FirstChild;
                    string oldActive = numActiveSettingVal.InnerText;
                    numActiveSettingVal.InnerText = "No";
                }
            }

            try
            {
                xmlDoc.Save(pathName);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;

        }

        protected void ButtonSave1_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name1.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum1.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum1);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact1Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName1", "TwilioTextNum1", "TwilioNum1Active", newContactName, newContactNum, newIsActive);
                            if (success)
                                Contact1Error.Text = "Contact information has been updated.";
                            else
                                Contact1Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact1Error.Text = cellNumErrorMsg;                      
                    }
                    else
                        Contact1Error.Text = "Please enter 'Contact Cell Number'.";                   
                }
                else
                    Contact1Error.Text = contactNameErrorMsg;             
            }
            else
                Contact1Error.Text = "Please enter 'Contact Name'.";
           
            UpdatePanelContact1Error.Update();
        }

        protected void ButtonCancel1_Click(object sender, EventArgs e)
        {
            GetContact1Data();
            Contact1Error.Text = "";
            UpdatePanelContact1Error.Update();
        }

        protected void ButtonClear1_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName1", "TwilioTextNum1", "TwilioNum1Active");
            if (success)
            {
                Name1.Text = "";
                ContactName1.Update();
                PhoneNum1.Text = "";
                ContactPhone1.Update();
                Contact1Toggle.SelectedValue = "No";
                ContactToggle1.Update();
                Contact1Error.Text = "";
            }
            else
                Contact1Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact1Error.Update(); ;
        }

        protected void ButtonSave2_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name2.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum2.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum2);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact2Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName2", "TwilioTextNum2", "TwilioNum2Active", newContactName, newContactNum, newIsActive);
                            if (success)
                                Contact2Error.Text = "Contact information has been updated.";
                            else
                                Contact2Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact2Error.Text = cellNumErrorMsg;
                    }
                    else
                        Contact2Error.Text = "Please enter 'Contact Cell Number'.";
                }
                else
                    Contact2Error.Text = contactNameErrorMsg;
            }
            else
                Contact2Error.Text = "Please enter 'Contact Name'.";

            UpdatePanelContact2Error.Update();
        }

        protected void ButtonCancel2_Click(object sender, EventArgs e)
        {
            GetContact2Data();
            Contact2Error.Text = "";
            UpdatePanelContact2Error.Update();
        }
        protected void ButtonClear2_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName2", "TwilioTextNum2", "TwilioNum2Active");
            if (success)
            {
                Name2.Text = "";
                ContactName2.Update();
                PhoneNum2.Text = "";
                ContactPhone2.Update();
                Contact2Toggle.SelectedValue = "No";
                ContactToggle2.Update();
                Contact2Error.Text = "";
            }
            else
                Contact2Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact2Error.Update(); ;
        }

        protected void ButtonSave3_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name3.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum3.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum3);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact3Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName3", "TwilioTextNum3", "TwilioNum3Active", newContactName, newContactNum, newIsActive);
                            if (success)
                                Contact3Error.Text = "Contact information has been updated.";
                            else
                                Contact3Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact3Error.Text = cellNumErrorMsg;
                    }
                    else
                        Contact3Error.Text = "Please enter 'Contact Cell Number'.";
                }
                else
                    Contact3Error.Text = contactNameErrorMsg;
            }
            else
                Contact3Error.Text = "Please enter 'Contact Name'.";

            UpdatePanelContact3Error.Update();
        }

        protected void ButtonCancel3_Click(object sender, EventArgs e)
        {
            GetContact3Data();
            Contact3Error.Text = "";
            UpdatePanelContact3Error.Update();
        }

        protected void ButtonClear3_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName3", "TwilioTextNum3", "TwilioNum3Active");
            if (success)
            {
                Name3.Text = "";
                ContactName3.Update();
                PhoneNum3.Text = "";
                ContactPhone3.Update();
                Contact3Toggle.SelectedValue = "No";
                ContactToggle3.Update();
                Contact3Error.Text = "";
            }
            else
                Contact3Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact3Error.Update();

        }

        protected void ButtonSave4_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name4.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum4.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum4);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact4Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName4", "TwilioTextNum4", "TwilioNum4Active", newContactName, newContactNum, newIsActive);
                            if (success)
                                Contact4Error.Text = "Contact information has been updated.";
                            else
                                Contact4Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact4Error.Text = cellNumErrorMsg;
                    }
                    else
                        Contact4Error.Text = "Please enter 'Contact Cell Number'.";
                }
                else
                    Contact4Error.Text = contactNameErrorMsg;
            }
            else
                Contact4Error.Text = "Please enter 'Contact Name'.";

            UpdatePanelContact4Error.Update();
        }

        protected void ButtonCancel4_Click(object sender, EventArgs e)
        {
            GetContact4Data();
            Contact4Error.Text = "";
            UpdatePanelContact4Error.Update();
        }

        protected void ButtonClear4_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName4", "TwilioTextNum4", "TwilioNum4Active");
            if (success)
            {
                Name4.Text = "";
                ContactName4.Update();
                PhoneNum4.Text = "";
                ContactPhone4.Update();
                Contact4Toggle.SelectedValue = "No";
                ContactToggle4.Update();
                Contact4Error.Text = "";
            }
            else
                Contact4Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact4Error.Update(); ;
        }

        protected void HomeButton_Click(object sender, EventArgs e)
        {
                Response.Redirect("https://discover.championx.com/sites/pat/SitePageModern/18668/unbridled-esp-systems-home", true);
        }

    }
}
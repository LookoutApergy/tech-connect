﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ideas - Suggestions.aspx.cs" Inherits="Tech_Connect_Survey_Forms.Ideas___Suggestions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
    <link rel="stylesheet" href="css/modalanimate.css"/>
    <link rel="stylesheet" type="text/css" id="TCStyles" href="TechConnect Computer Styles.css" />
    <script type="text/javascript" src="~/Scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="MainDiv" runat="server"  class="MainDivStyle">      
        <div id="BannerDiv" style="height:150px; position:relative">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/New UNB Logo Banner.png" Height="100%" ImageAlign="Left" Width="98%"  />
            <asp:Label ID="Label2" runat="server" Text="TechConnect" CssClass="TechConnectLabelStyle"></asp:Label>
            <div style="position:absolute; left:90%; bottom:10%; z-index:100; "> 
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Home.png" Style="height:35px" onclick="clickHome()" />
                <asp:Button ID="HomeButton" runat="server" style="visibility:hidden" OnClick="HomeButton_Click"/>
            </div>
        </div>     

            <div id="LabelMainDiv" class="LabelMainDivStyle">
                <asp:Label ID="LabelIdeasSuggestionsOptions" runat="server" Text="Ideas & Suggestions Options" CssClass="LabelMainStyle"></asp:Label>
            </div>

            <asp:UpdatePanel ID="UpdatePanelTitle" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>
                    <div id="headerDiv" class="headerDivStyle">
                        <asp:TextBox ID="TextBox8" runat="server"  CssClass="HiddenTextStyle"  ></asp:TextBox>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>              
            <div  id="labelDiv" runat="server" class="LabelDivStyle">              
                <asp:Label ID="LabelOptionCol" runat="server" Text="Options" CssClass="LabelColumnStyle" ></asp:Label>
                <div class="UpdatePanelPadding">
                    <asp:UpdatePanel ID="UpdatePanelLabelColB" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                            <asp:Label ID="LabelBCol" runat="server" Text="" Cssclass="LabelColumnStyle" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="DDGrid" runat="server" class="DropdownDivStyle">
                <div class="OptionsDivStyle">
                    <asp:DropDownList ID="IdeasSuggestionsOptions" runat="server" AutoPostBack="True" CssClass="DropdownColumnStyle" OnSelectedIndexChanged="IdeasSuggestionsOptions_SelectedIndexChanged"  >
                        <asp:ListItem>  Select Option...</asp:ListItem>
                        <asp:ListItem>Downhole Equipment</asp:ListItem>
                        <asp:ListItem>Surface</asp:ListItem>
                        <asp:ListItem>Application</asp:ListItem>
                        <asp:ListItem>Service Center</asp:ListItem>
                        <asp:ListItem>Field Service</asp:ListItem>
                        <asp:ListItem>Engineering</asp:ListItem>
                        <asp:ListItem>Vendor-3rd Party</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:UpdatePanel ID="UpdatePanelDDColB" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <div id="DEOOptionsB_Div" class="OptionBDropdownDivStyle">
                            <asp:Button ID="ColBOSelectButton" runat="server" AutoPostBack="true" Text="" CssClass="ColCOptionSelectButtonStyle" BorderStyle="Solid" OnClick="ColBOptionSelectButton_Click" />
                            <asp:ListBox ID="OptionBListBox" runat="server" AutoPostBack="true" CssClass="OptionBListBoxStyle" OnSelectedIndexChanged="OptionBListBox_SelectedIndexChanged">
                            </asp:ListBox>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div id="AdditionalCommentsDiv" runat="server" >
                <div runat="server" class="AdditionalCommentsIandSDivStyle">
                    <div class="LabelAddCommentsDivIandSStyle">
                        <asp:Label ID="LabelAsterisk" runat="server" Text="*" CssClass="LabelAsteriskStyle"></asp:Label>
                        <asp:Label ID="LabelAddComments" runat="server" Text="Comments:" CssClass="LabelAddCommentsStyle"></asp:Label>
                    </div>                            
                </div>                
                <div  id="Div1" runat="server" class="UpdatePanelAddCommentsIandSDiv1Style">
                    <div class="UpdatePanelAddCommentsDiv2Style">
                        <asp:UpdatePanel ID="UpdatePanelAddComments" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="AdditionalCommentsTextBox" runat="server" AutoPostBack="True" CssClass="AdditionalCommentsTextBoxStyle" TextMode="MultiLine"  ></asp:TextBox>                              
                            </ContentTemplate>
                        </asp:UpdatePanel>                                                  
                    </div>
                    <div class="AFButtonDivIandSStyle">                                     
                        <asp:UpdatePanel ID="UpdatePanelAFButton" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="AFButtonDiv" runat="server" >
                                    <asp:Button ID="AFButton" runat="server" Text="Attach Files" CssClass="AFButtonIandSStyle" OnClick="AFButton_Click" Visible="True"  />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div> 
                    <div class="UpdatePanelSubmitButtonIandSDivStyle">
                        <asp:UpdatePanel ID="UpdatePanelSubmitButton" runat="server" UpdateMode="Conditional" >
                            <ContentTemplate>
                                    <asp:Button ID="ButtonSubmit" runat="server" AutoPostBack="true" Text="Submit ticket" CssClass="ButtonSubmitStyle" OnClick="ButtonSubmit_Click" />                           
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanelFileUpload" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>
                    <div id="FileUploadDiv" runat="server">                      
                        <div  id="Div2" runat="server" class="FileUploadControlDiv1IandSStyle">
                            <div class="FileUploadControlDiv2Style" >   
                                <div class="FileUploadControlDiv3Style">
                                    <asp:FileUpload ID="FileUpload" runat="server" CssClass="FileUploadStyle" AllowMultiple="true" onchange="AddFileToList(this)" />
                                </div>                                                              
                            </div>                                
                            <div class="ListBoxFileUploadDivStyle" >
                                <asp:ListBox ID="ListBoxFileUpload" runat="server" CssClass="ListBoxFileUploadStyle" >
                                </asp:ListBox>
                            </div>                                
                            <div class="UploadOptionsDiv1IandSStyle" >   
                                <div  id="Div5" runat="server" class="UploadOptionsDiv2Style">
                                    <div class="LabelMaxFilesDivStyle"> 
                                        <asp:Label ID="LabelMaxFiles" runat="server" Text="" CssClass="LabelMaxFilesStyle"></asp:Label>
                                    </div>
                                    <div class="ButtonDeleteDivStyle" > 
                                        <asp:Button ID="ButtonDelete" runat="server" Text="Delete" CssClass="ButtonDeleteClearStyle" OnClick="ButtonDelete_Click"  />
                                    </div>
                                    <div class="ButtonClearDivStyle"> 
                                        <asp:Button ID="ButtonClear" runat="server" Text="Clear" CssClass="ButtonDeleteClearStyle" OnClick="ButtonClear_Click" />
                                    </div>
                                </div>
                            </div>                             
                        </div>                         
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UpdatePanel1Modalpopup" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>                  
                    <asp:Button ID="btnShow" runat="server" Text="Show Modal Popup" Style="visibility:hidden" />
                    <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="PanelModalPopup" TargetControlID="btnShow"
                        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="PanelModalPopup" runat="server" UpdateMode="Always" CssClass="modalPopup" align="left" style = "display:none; " >
                        <div style="position:relative">
                            <div class="ModalPopupBannerDivStyle">
                                <asp:Label ID="ModalPopupLabel1" runat="server" Text="Error:" CssClass="ModalPopupBannerStyle" ></asp:Label>
                            </div>
                            <div class="ModalPopupLabelDivStyle1">
                                <asp:Label ID="ModalPopupLabel2" runat="server" Text="" CssClass="ModalPopupLabelStyle" ></asp:Label>
                            </div>
                            <div class="ModalPopupLabelDivStyle2">
                                <asp:Label ID="ModalPopupLabel3" runat="server" Text="" CssClass="ModalPopupLabelStyle"  ></asp:Label>
                            </div>
                            <div class="ModalPopupLabelDivStyle2">
                                <asp:Label ID="ModalPopupLabel4" runat="server" Text="" CssClass="ModalPopupLabelStyle"  ></asp:Label>
                            </div>
                       </div>
                        <div class="ModalPopupBtnCloseDivStyle">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ModalPopupBtnCloseStyle" OnClientClick="return modalClose()"  />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div>
                <asp:Button ID="uploadButton" runat="server" Text="Button"  style="visibility:hidden" OnClick="uploadButton_Click"/>
                <asp:TextBox ID="TextBox5" runat="server"  CssClass="HiddenTextStyle"></asp:TextBox>
                <asp:TextBox ID="TextBox6" runat="server"  CssClass="HiddenTextStyle"></asp:TextBox>
                <input type="hidden" id="pageHeight" runat="server" value="" />
                <input type="hidden" id="pageWidth" runat="server" value="" />
            </div>

            <div class="TechConnectImageDivStyle">
                <asp:Image ID="TechConnectImage" runat="server" ImageUrl="~/Images/TechConnect Icon with space modified.png"  width="15%" Height="95%"  />
            </div>
        </div>

    </form>
        <script type="text/javascript" >

            $('#PanelModalPopup').on('hidden.bs.modal', function () {
                document.getElementById('TextBox5').value = "it is hidden";
            })

            function AddFileToList(fileUpload)
            {
                var myButton = document.getElementById('uploadButton');
                myButton.click();
            }

            function clickHome() {
                var homeButton = document.getElementById('HomeButton');
                homeButton.click();
            }

            function resetDDCss() {
                var OptCDD = document.getElementById('OptionCDropdown');
            }

            function integersOnly(obj) {
                obj.value = obj.value.replace(/[^0-9]/g, '');
            }

            function modalClose() {
                document.getElementById('TextBox5').value = "it is hidden";
                var popupMsg = document.getElementById('TextBox8').value;
                if (popupMsg == "Exit") 
                    var homeButton = document.getElementById('HomeButton');
                    homeButton.click();
            }

    </script>
</body>
</html>




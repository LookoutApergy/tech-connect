﻿using Microsoft.Ajax.Utilities;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Net;
using Twilio.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Tech_Connect_Survey_Forms.App_Code;

namespace Tech_Connect_Survey_Forms
{
    public partial class ApplicationMobile : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string userEmail = Session["s_UserEmail"] as string;

            if (!IsPostBack)
            {
                hideAdditionalCommentsDiv();
                hideAttachFilesButton();
                hideFileUploadPanel();
                UpdatePanelTitle.Update();
            }

        }
        private void hideFileUploadPanel()
        {
            LabelMaxFiles.Text = string.Empty;
            FileUploadDiv.Visible = false;
            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }
        private void showFileUploadPanel()
        {
            FileUploadDiv.Visible = true;
            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }
        private void hideAttachFilesButton()
        {
            AFButtonDiv.Visible = false;
            UpdatePanelAFButton.Update();
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }
        private void showAttachFilesButton()
        {
            AFButtonDiv.Visible = true;
            UpdatePanelAFButton.Update();
        }

        private void showAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = true;
            UpdatePanelAddComments.Update();
            showAttachFilesButton();
        }

        private void hideAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = false;
            AdditionalCommentsTextBox.Text = string.Empty;
            UpdatePanelAddComments.Update();
        }

        protected void ApplicationOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            showAdditionalCommentsDiv();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            string errorMsg = "";
            errorMsg = validateTextBoxes();
            if (errorMsg.Length == 0)
            {
                StringBuilder emailBody = createBody("email");
                StringBuilder emailSubject = createSubject("email");
                sendEmail(emailBody, emailSubject);

                TechConnect_Utilities util = new TechConnect_Utilities();
                bool sendText = util.CheckSeverity(SeverityDropDownList);
                if (sendText)
                {
                    StringBuilder textBody = createBody("text");
                    sendTextMsg(textBody);
                }

                UpdatePanelSubmitButton.Update();
                util.CleanUploadsFolder();

                ModalPopupLabel1.Text = "Thank you for your submission!";
                ModalPopupLabel2.Text = "A ticket will be created and sent to your email address. ";
                ModalPopupLabel3.Text = "The ticket generator will be ‘Service Desk Plus Ticketing System’.";
                if (sendText)
                    ModalPopupLabel4.Text = "A text will also be sent to a staff member for immediate assistance.";

                TextBox8.Text = "Exit";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
            else
            {
                ModalPopupLabel1.Text = "Error:";
                ModalPopupLabel2.Text = errorMsg;
                ModalPopupLabel3.Text = "";
                ModalPopupLabel4.Text = "";
                TextBox8.Text = "Stay";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
        }

        private void sendEmail(StringBuilder emailBody, StringBuilder emailSubject)
        {
            String userEmailAddress = Session["s_UserEmail"].ToString();
            String supportEmailAddress = Session["s_SupportEmail"].ToString();
            String supportEmailPassword = Session["s_SupportPswd"].ToString();
            String sendGridUserName = Session["s_SendGridUserName"].ToString();
            String sendGridPassword = Session["s_SendGridPassword"].ToString();
            String ticketEmailAddress = Session["s_TicketingEmail"].ToString();

            //String ticketEmailAddress = "tbairok@gmail.com";

            using (MailMessage message = new MailMessage(supportEmailAddress, ticketEmailAddress, emailSubject.ToString(), emailBody.ToString()))
            {
                AddAttachments(message);
                message.ReplyToList.Add(userEmailAddress);
                SmtpClient emailClient = new SmtpClient("smtp.sendgrid.net", 587);
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential(sendGridUserName, sendGridPassword);
                emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                emailClient.EnableSsl = false;

                try
                {
                    emailClient.Send(message);
                }
                catch (SmtpFailedRecipientException ex)
                {

                }
            }
        }

        private void sendTextMsg(StringBuilder textBody)
        {
            var accountSid = Session["s_TwilioAcctID"].ToString();
            var authToken = Session["s_TwilioAuthToken"].ToString();
            var fromPhoneNum = Session["s_TwilioFromPhoneNum"].ToString();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            TwilioClient.Init(accountSid, authToken);
            List<string> TextNums = new List<string>();

            string textnum1 = Session["s_TwilioFromPhoneNum1"].ToString();
            if (textnum1 != "")
                TextNums.Add(textnum1);
            string textnum2 = Session["s_TwilioFromPhoneNum2"].ToString();
            if (textnum2 != "")
                TextNums.Add(textnum2);
            string textnum3 = Session["s_TwilioFromPhoneNum3"].ToString();
            if (textnum3 != "")
                TextNums.Add(textnum3);
            string textnum4 = Session["s_TwilioFromPhoneNum4"].ToString();
            if (textnum4 != "")
                TextNums.Add(textnum4);

            foreach (var textnum in TextNums)
            {
                var messageOptions = new CreateMessageOptions(
                new PhoneNumber(textnum));

                messageOptions.From = new PhoneNumber(fromPhoneNum);
                messageOptions.Body = textBody.ToString();

                var message = MessageResource.Create(messageOptions);
                Console.WriteLine(message.Body);
            }
        }

        private StringBuilder createSubject(string messagetype)
        {
            string applicationOption = ApplicationOptions.SelectedItem.Text;
            string severityLable = LabelSeverity.Text;
            string sLevel = "";

            String sColText = "";
            if (SeverityDropDownList.Items.Count > 0)
            {
                sColText = SeverityDropDownList.SelectedItem.Text;
            }

            int colonIndex = 0;
            colonIndex = sColText.IndexOf(":");
            sLevel = sColText.Substring(0, colonIndex);

            StringBuilder textSubject = new StringBuilder();
            textSubject.Append("TECH CONNECT - Application Option: ");
            textSubject.Append(applicationOption + "; ");
            textSubject.Append("Severity: " + sLevel);

            return textSubject;
        }

        private void AddAttachments(MailMessage message)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            string uploadPath = util.GetUploadPath();
            var folderPath = HttpContext.Current.Server.MapPath(uploadPath);

            bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadupPathExists)
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    Attachment attachment = new Attachment(file);
                    message.Attachments.Add(attachment);
                }
            }
        }

        private string validateTextBoxes()
        {
            string errMsg = "";
            if (errMsg == "")
            {
                if (AdditionalCommentsTextBox.Text.Length > 0)
                {
                    string commentString = AdditionalCommentsTextBox.Text;
                    Regex r = new Regex("^[a-zA-Z0-9-@':&,.!?/_$ ]*$");
                    if (!r.IsMatch(commentString))
                    {
                        char[] commentsArray = commentString.ToCharArray();
                        for (int i = 0; i < commentsArray.Length; i++)
                        {
                            string commentsChar = commentsArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character.";
                                i = commentsArray.Length;
                            }
                        }
                    }
                }
                else
                    errMsg = "Please enter 'Comments' before submitting ticket.";
            }
            return errMsg;
        }

        private StringBuilder createBody(string messagetype)
        {
            string userEmail = Session["s_UserEmail"] as string;
            int endPos = userEmail.IndexOf("@");
            string currentUser = userEmail.Substring(0, endPos);

            string addComentsLable = LabelAddComments.Text;
            string addComentsText = "";
            if (AdditionalCommentsTextBox.Text.Length > 0) addComentsText = AdditionalCommentsTextBox.Text;

            string severityLable = LabelSeverity.Text;
            string sColText = "";
            if (SeverityDropDownList.Items.Count > 0) sColText = SeverityDropDownList.SelectedItem.Text;

            StringBuilder emailBody = new StringBuilder();
            string applicationOption = ApplicationOptions.SelectedItem.Text;

            if (messagetype == "email")
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("");

                emailBody.AppendLine("Requester's Name: " + "\t" + currentUser);
                emailBody.AppendLine("Requester's Email: " + "\t" + "  " + userEmail);

                emailBody.AppendLine("");
                emailBody.AppendLine("Application: " + "\t" + "    " + applicationOption);

                emailBody.AppendLine("Comments: " + "\t" + "  " + addComentsText);
                emailBody.AppendLine("Severity: " + "\t" + "\t" + sColText);
            }
            else
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("Severity: Immediate");
                emailBody.AppendLine("");
                emailBody.AppendLine("Requester's Name: " + currentUser);
                emailBody.AppendLine("Requester's Email: " + userEmail);
                emailBody.AppendLine("Application: " + applicationOption);

                emailBody.AppendLine("Comments: " + addComentsText);
            }

            return emailBody;
        }

        protected void AFButton_Click(object sender, EventArgs e)
        {
            LabelMaxFiles.Text = String.Empty;
            UpdatePanelAFButton.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();

            if (FileUploadDiv.Visible == false)
            {
                ListBoxFileUpload.Items.Clear();
                FileUploadDiv.Visible = true;
            }
            else
            {
                FileUploadDiv.Visible = false;
                UpdatePanelFileUpload.Update();
            }

            UploadLabel.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            var postedFile = FileUpload.PostedFile;
            var newAttachment = FileUpload.PostedFile.FileName.ToString();

            TechConnect_Utilities util = new TechConnect_Utilities();
            bool canUpload = util.CheckUploadFiles(postedFile, ListBoxFileUpload, LabelMaxFiles, FileUpload, UpdatePanelFileUpload);
            if (canUpload)
            {
                ListBoxFileUpload.Items.Add(new ListItem(newAttachment, newAttachment));
                ListBoxFileUpload.SelectedIndex = -1;

                UploadLabel.Visible = true;
                string uploadPath = util.GetUploadPath();

                bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
                if (!userUploadupPathExists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(uploadPath));

                FileUpload.PostedFile.SaveAs(Server.MapPath(uploadPath + newAttachment));
            }

            for (int i = 0; i < ListBoxFileUpload.Items.Count; i++)
            {
                string fileName = ListBoxFileUpload.Items[i].Text;

                TableRow row = new TableRow();
                TableItemStyle rowStyle = new TableItemStyle();
                rowStyle.Height = Unit.Pixel(25);
                row.ApplyStyle(rowStyle);
                TableCell cell1 = new TableCell();
                cell1.ApplyStyle(rowStyle);
                cell1.Text = fileName;
                row.Cells.Add(cell1);
                SelectedFilesTable.Rows.Add(row);
            }

            UpdatePanelFileUpload.Update();
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            int selectedFileIndex = ListBoxFileUpload.SelectedIndex;

            if (selectedFileIndex == -1) LabelMaxFiles.Text = "Please select file to delete.";
            else
            {
                LabelMaxFiles.Text = "";
                string fileName = ListBoxFileUpload.SelectedValue;
                ListBoxFileUpload.Items.RemoveAt(selectedFileIndex);

                string FileToDelete;
                TechConnect_Utilities util = new TechConnect_Utilities();
                string uploadPath = util.GetUploadPath();

                FileToDelete = Server.MapPath(uploadPath + fileName);
                File.Delete(FileToDelete);
            }

            for (int i = 0; i < ListBoxFileUpload.Items.Count; i++)
            {
                string fileName = ListBoxFileUpload.Items[i].Text;
                TableRow row = new TableRow();
                TableItemStyle rowStyle = new TableItemStyle();
                rowStyle.Height = Unit.Pixel(25);
                row.ApplyStyle(rowStyle);
                TableCell cell1 = new TableCell();
                cell1.ApplyStyle(rowStyle);
                cell1.Text = fileName;
                row.Cells.Add(cell1);
                SelectedFilesTable.Rows.Add(row);
            }

            if (ListBoxFileUpload.Items.Count == 0)
            {
                UploadLabel.Visible = false;
            }

            UpdatePanelFileUpload.Update();
        }

        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            ListBoxFileUpload.Items.Clear();
            UpdatePanelFileUpload.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }

        protected void HomeButton_Click(object sender, EventArgs e)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.DeleteUserUploadFolder();

            var TechConnectHomeURL = Session["DefaultUrl"].ToString();
            Response.Redirect(TechConnectHomeURL, false);
        }
    }
}
﻿using Microsoft.Ajax.Utilities;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Net;
using Twilio.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Tech_Connect_Survey_Forms.App_Code;

namespace Tech_Connect_Survey_Forms
{
    public partial class Surface : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterHiddenField("pageHeight", "testtext");
            Page.ClientScript.RegisterHiddenField("pageWidth", "testtext");
            ClientScriptManager cs = this.ClientScript;
            StringBuilder cstext2 = new StringBuilder();

            cstext2.Append("var strW = screen.width;");
            cstext2.Append("var strH = screen.height;");
            cstext2.Append("document.getElementById('pageWidth').value = strW;");
            cstext2.Append("document.getElementById('pageHeight').value = strH;");
            cstext2.Append("if (strW ==  1920) {strW = 1536; strH = 940; document.getElementById('BannerDiv').style.height = '150px';  } " +
                "else if (strW ==  1536) {strH = 710; document.getElementById('BannerDiv').style.height = '150px'; " +
                "}  else if (strW == 1280) {strH = 590; document.getElementById('BannerDiv').style.height = '100px';document.getElementById('TCStyles').setAttribute('href', 'TechConnect Computer Styles Short.css');};");
            cstext2.Append("document.getElementById('form1').style.height = strH + 'px';");
            cstext2.Append("document.getElementById('form1').style.background = '#f8f9fa';");
            cs.RegisterStartupScript(this.GetType(), "test1", cstext2.ToString(), true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string userEmail = Session["s_UserEmail"] as string;

            if (!IsPostBack)
            {
                ClearColumnB();
                ClearColumnC();
                ClearColumnC2();
                ClearColumnsDandE();
                hideAdditionalCommentsDiv();
                hideAttachFilesButton();
                hideFileUploadPanel();
                UpdatePanelTitle.Update();
            }
        }

        private void hideFileUploadPanel()
        {
            LabelMaxFiles.Text = string.Empty;
            FileUploadDiv.Visible = false;
            UpdatePanelFileUpload.Update();
        }

        private void showFileUploadPanel()
        {
            FileUploadDiv.Visible = true;
            UpdatePanelFileUpload.Update();
        }

        private void hideAttachFilesButton()
        {
            AFButtonDiv.Visible = false;
            UpdatePanelAFButton.Update();
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }

        private void showAttachFilesButton()
        {
            AFButtonDiv.Visible = true;
            UpdatePanelAFButton.Update();
        }

        private void clearColumns()
        {
            ClearColumnB();
            ClearColumnC();
            ClearColumnC2();
            ClearColumnsDandE();
            hideAdditionalCommentsDiv();
            hideAttachFilesButton();
            hideFileUploadPanel();
        }

        private void showAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = true;
            UpdatePanelAddComments.Update();
            showAttachFilesButton();
        }

        private void hideAdditionalCommentsDiv()
        {
            AdditionalCommentsDiv.Visible = false;
            AdditionalCommentsTextBox.Text = string.Empty;
            UpdatePanelAddComments.Update();
        }

        /***** Column A/DEOOptions Methods and Events *****/
        protected void SurfaceOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SurfaceOption = SurfaceOptions.SelectedValue.ToString();
            TextBoxOption.Text = SurfaceOption;
            UpdatePanelTitle.Update();
            ClearColumnB();
            ClearColumnC();
            ClearColumnC2();
            ClearColumnsDandE();

            switch (SurfaceOption)
            {
                case "VSD":
                    LoadColB_SelectionButton("VSD Option", "Select Brand...");
                    LoadColB_VSDBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "Transformers":
                    LoadColB_SelectionButton("Transformers Option", "Select Option...");
                    LoadColB_TransformersBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "SWB":
                    LoadColB_SelectionButton("Switchboard", "Select Option...");
                    LoadColB_SWBBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "SWB Accessories":
                    LoadColB_SelectionButton("Switchboard Accs.", "Select Option...");
                    LoadColB_SWBAccessoriesBrands();
                    showAdditionalCommentsDiv();
                    break;
                case "Sensor Surface Equipment":
                    LoadColB_SelectionButton("Sensor Surface Equip", "Select Option...");
                    LoadColB_SensorSurfaceEquipOptions();
                    showAdditionalCommentsDiv();
                    break;
                case "Other":
                    showAdditionalCommentsDiv();
                    break;
                default:
                    clearColumns();
                    break;
            }
        }

        /*  Column B Methods  */

        private void ClearColumnB()
        {
            Label labelB = this.FindControl("LabelBCol") as Label;
            labelB.Text = "";
            OptionBDropdown.Items.Clear();
            labelB.Visible = false;
            OptionBDropdown.Visible = false;

            OptionBListBox.Items.Clear();
            OptionBListBox.Visible = false;
            ColBOSelectButton.Text = String.Empty;
            ColBOSelectButton.Visible = false;

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        private void RefreshColB()
        {
            OptionBListBox.Items[0].Selected = true;
            OptionBListBox.Visible = true;
            OptionBListBox.Focus();

            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        protected void ColBOptionSelectButton_Click(object sender, EventArgs e)
        {
            ClearColumnC();
            ClearColumnC2();
            ClearColumnsDandE();
            OptionBListBox.Visible = true;
            OptionBListBox.SelectedIndex = 0;

            UpdatePanelDDColB.Update();
        }

        private void LoadColB_SelectionButton(string labeltext, string selectiontext)
        {
            ColBOSelectButton.Text = selectiontext;
            ColBOSelectButton.Visible = true;
            Label labelC = this.FindControl("LabelBCol") as Label;
            labelC.Text = labeltext;

            labelC.Visible = true;
            UpdatePanelLabelColB.Update();
            UpdatePanelDDColB.Update();
        }

        protected void OptionBListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearColumnC();
            ClearColumnC2();
            ClearColumnsDandE();
            string SurfaceOptionA = TextBoxOption.Text;
            string SurfaceOptionB = OptionBListBox.SelectedValue.ToString();

            ColBOSelectButton.Text = SurfaceOptionB;
            OptionBListBox.Visible = false;
            UpdatePanelDDColB.Update();

            TextBox1.Text = SurfaceOptionB;
            UpdatePanelTitle.Update();
            TextBox4.Text = SurfaceOptionB;
            UpdatePanelTitle.Update();

            switch (SurfaceOptionA)
            {
                case "VSD":
                    switch (SurfaceOptionB)
                    {
                        case "Smarten":
                            LoadColC_SmartenOptions();
                            LoadColC_SelectionButton("Model Options", "Select Model...");
                            break;
                        case "Spoc":
                            LoadColC2_SpocPulseOptions();
                            LoadColC2_SelectionButton("Pulse Options", "Select Pulse...");
                            break;
                        case "Competitor":
                            LoadColC_CompetitorNameOption();
                            LoadColC2_PulseTextBoxOption();
                            LoadColD_KVATextBox();
                            break;
                        default:
                            break;
                    }
                    break;

                case "Transformers":
                    LoadColC_KVAOptions();
                    LoadColC_SelectionButton("KVA", "Select KVA...");
                    break;

                case "SWB":
                    switch (SurfaceOptionB)
                    {
                        case "ChampionX":
                            LoadColC_SWBVoltOptions();
                            LoadColC_SelectionButton("Switchboard", "Select SWB...");
                            break;
                        case "Other":
                            LoadColC_OtherSWB();
                            break;
                        default:
                            break;
                    }
                    break;

                case "SWB Accessories":
                    switch (SurfaceOptionB)
                    {
                        case "ChampionX":
                            LoadColC_ControllerOptions();
                            LoadColC_SelectionButton("Controller", "Select Controller...");
                            break;
                        case "Other":
                            LoadColC_OtherController();
                            LoadColC2_AccessoriesTextBoxOption();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Sensor Surface Equipment":
                    SSE_PNTextbox();
                    break;
            }
        }

        private void LoadColB_VSDBrands()
        {
            OptionBListBox.Items.Clear();
            OptionBListBox.Items.Add(new ListItem("Select Brand...", "Select Brand..."));
            OptionBListBox.Items.Add(new ListItem("Smarten", "Smarten"));
            OptionBListBox.Items.Add(new ListItem("Spoc", "Spoc"));
            OptionBListBox.Items.Add(new ListItem("Competitor", "Competitor"));
            OptionBListBox.Visible = true;

            RefreshColB();
        }

        private void LoadColB_SWBAccessoriesBrands()
        {
            OptionBListBox.Items.Clear();
            OptionBListBox.Items.Add(new ListItem("Select Brand...", "Select Brand..."));
            OptionBListBox.Items.Add(new ListItem("ChampionX", "ChampionX"));
            OptionBListBox.Items.Add(new ListItem("Other", "Other"));
            OptionBListBox.Visible = true;

            RefreshColB();
        }

        private void LoadColB_SensorSurfaceEquipOptions()
        {
            OptionBListBox.Items.Clear();
            OptionBListBox.Items.Add(new ListItem("Select Option...", "Select Option..."));
            OptionBListBox.Items.Add(new ListItem("Read Out", "Read Out"));
            OptionBListBox.Items.Add(new ListItem("Choke", "Choke"));
            OptionBListBox.Visible = true;

            RefreshColB();
        }

        private void LoadColB_SWBBrands()
        {
            OptionBListBox.Items.Clear();
            OptionBListBox.Items.Add(new ListItem("Select Brand...", "Select Brand..."));
            OptionBListBox.Items.Add(new ListItem("ChampionX", "ChampionX"));
            OptionBListBox.Items.Add(new ListItem("Other", "Other"));
            OptionBListBox.Visible = true;

            RefreshColB();
        }

        private void LoadColB_TransformersBrands()
        {
            OptionBListBox.Items.Clear();
            OptionBListBox.Items.Add(new ListItem("Select Brand...", "Select Brand..."));
            OptionBListBox.Items.Add(new ListItem("Southwest", "Southwest"));
            OptionBListBox.Items.Add(new ListItem("Neco", "Neco"));
            OptionBListBox.Items.Add(new ListItem("Other", "Other"));
            OptionBListBox.Visible = true;

            RefreshColB();
        }

        /* Column C Methods  */

        private void ClearColumnC()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = String.Empty;
            labelC.Visible = false;

            OptionCListBox.Items.Clear();
            OptionCListBox.Visible = false;
            ColCOSelectButton.Text = String.Empty;
            ColCOSelectButton.Visible = false;
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "#f8f9fa");
            CompetitorNameText.Visible = false;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void RefreshColC()
        {
            OptionCListBox.Items[0].Selected = true;
            OptionCListBox.Visible = true;
            OptionCListBox.Focus();

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void LoadColC_SelectionButton(string labeltext, string selectiontext)
        {
            ColCOSelectButton.Text = selectiontext;
            ColCOSelectButton.Visible = true;
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = labeltext;

            labelC.Visible = true;
            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        protected void ColCOptionSelectButton_Click(object sender, EventArgs e)
        {
            ClearColumnC2();
            ClearColumnsDandE();
            //ClearColumnF();
            OptionCListBox.Visible = true;
            OptionCListBox.SelectedIndex = 0;
            UpdatePanelDDColC.Update();
        }

        private void LoadColC_CompetitorNameOption()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Competitor Name";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            ColCOSelectButton.Visible = false;
            OptionCListBox.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void LoadColC_ControllerOptions()
        {
            OptionCListBox.Items.Clear();
            OptionCListBox.Items.Add(new ListItem("Select Controller...", "Select Controller..."));
            OptionCListBox.Items.Add(new ListItem("Icon Motor Sensor", "Icon Motor Sensor"));
            OptionCListBox.Items.Add(new ListItem("Icon Display Sensor", "Icon Display Sensor"));
            OptionCListBox.Visible = true;

            RefreshColC();
        }

        private void LoadColC_OtherSWB()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "SWB";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            ColCOSelectButton.Visible = false;
            OptionCListBox.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void LoadColC_OtherController()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Controller";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            ColCOSelectButton.Visible = false;
            OptionCListBox.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        private void SSE_PNTextbox()
        {
            Label labelC = this.FindControl("LabelCCol") as Label;
            labelC.Text = "Part Number";
            CompetitorNameText.Text = String.Empty;
            CompetitorNameText.Visible = true;
            CompetitorNameText.Attributes.CssStyle.Add("background-color", "white");

            ColCOSelectButton.Visible = false;
            OptionCListBox.Visible = false;
            labelC.Visible = true;
            CompetitorNameText.Visible = true;

            UpdatePanelLabelColC.Update();
            UpdatePanelDDColC.Update();
        }

        protected void OptionCListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string surfaceOptionA = TextBoxOption.Text;
            string surfaceOptionB = TextBox1.Text;
            string surfaceOptionC = OptionCListBox.SelectedValue.ToString();
            TextBox2.Text = surfaceOptionC;
            ColCOSelectButton.Text = surfaceOptionC;
            OptionCListBox.Visible = false;
            UpdatePanelDDColC.Update();
            UpdatePanelTitle.Update();

            switch (surfaceOptionA)
            {
                case "VSD":
                    switch (surfaceOptionB)
                    {
                        case "Smarten":
                            switch (surfaceOptionC)
                            {
                                case "Smarten for Life":
                                    LoadColC2_SmartenPulseOption();
                                    LoadColDE_SFL_Options();
                                    break;
                                default:
                                    LoadColC2_SmartenPulseOption();
                                    LoadColDE_SLH_Options();
                                    break;
                            }
                            break;
                    }
                    break;
                case "SWB Accessories":
                    switch (surfaceOptionB)
                    {
                        case "ChampionX":
                            LoadColC2_AccessoriesOptions();
                            LoadColC2_SelectionButton("Accessories", "Select Accessory...");
                            break;
                    }
                    break;
            }
        }

        private void LoadColC_SmartenOptions()
        {
            OptionCListBox.Items.Clear();
            OptionCListBox.Items.Add(new ListItem("Select Model...", "Select Model..."));
            OptionCListBox.Items.Add(new ListItem("Standard", "Standard"));
            OptionCListBox.Items.Add(new ListItem("Low Harmonic", "Low Harmonic"));
            OptionCListBox.Items.Add(new ListItem("Smarten for Life", "Smarten for Life"));
            OptionCListBox.Visible = true;

            RefreshColC();
        }
        private void LoadColC_SpocOptions()
        {
            OptionCListBox.Items.Clear();
            OptionCListBox.Items.Add(new ListItem("Select Pulse...", "Select Pulse..."));
            OptionCListBox.Items.Add(new ListItem("Option 1", "Option 1"));
            OptionCListBox.Items.Add(new ListItem("Option 2", "Option 2"));

            RefreshColC();
        }

        private void LoadColC_CompetitorOptions()
        {
            OptionCListBox.Items.Clear();
            OptionCListBox.Items.Add(new ListItem("Select Option...", "Select Option..."));
            OptionCListBox.Items.Add(new ListItem("Option 1", "Option 1"));
            OptionCListBox.Items.Add(new ListItem("Option 2", "Option 2"));

            RefreshColC();
        }

        private void LoadColC_SWBVoltOptions()
        {
            OptionCListBox.Items.Clear();
            OptionCListBox.Items.Add(new ListItem("Select SWB...", "Select SWB..."));
            OptionCListBox.Items.Add(new ListItem("600 volts", "600 volts"));
            OptionCListBox.Items.Add(new ListItem("1000 volts", "1000 volts"));
            OptionCListBox.Items.Add(new ListItem("1500 volts", "1500 volts"));
            OptionCListBox.Items.Add(new ListItem("2500 volts", "2500 volts"));
            OptionCListBox.Items.Add(new ListItem("3600 volts", "3600 volts"));
            OptionCListBox.Items.Add(new ListItem("5000 volts", "5000 volts"));

            RefreshColC();
        }

        private void LoadColC_KVAOptions()
        {
            OptionCListBox.Items.Clear();
            OptionCListBox.Items.Add(new ListItem("Select KVA...", "Select KVA..."));
            OptionCListBox.Items.Add(new ListItem("100", "100"));
            OptionCListBox.Items.Add(new ListItem("130", "130"));
            OptionCListBox.Items.Add(new ListItem("150", "150"));
            OptionCListBox.Items.Add(new ListItem("210", "210"));
            OptionCListBox.Items.Add(new ListItem("260", "260"));
            OptionCListBox.Items.Add(new ListItem("355", "355"));
            OptionCListBox.Items.Add(new ListItem("400", "400"));
            OptionCListBox.Items.Add(new ListItem("520", "520"));
            OptionCListBox.Items.Add(new ListItem("1000", "1000"));

            RefreshColC();
        }

        /*  Column C2 Methods   */

        private void ClearColumnC2()
        {
            Label labelC2 = this.FindControl("LabelC2Col") as Label;
            labelC2.Text = String.Empty;
            labelC2.Visible = false;

            OptionC2ListBox.Items.Clear();
            OptionC2ListBox.Visible = false;
            ColC2OSelectButton.Text = String.Empty;
            ColC2OSelectButton.Visible = false;
            PulseText.Text = String.Empty;
            PulseText.Visible = false;

            UpdatePanelLabelColC2.Update();
            UpdatePanelDDColC2.Update();
        }

        protected void ColC2OptionSelectButton_Click(object sender, EventArgs e)
        {
            ClearColumnsDandE();
            OptionC2ListBox.Visible = true;
            OptionC2ListBox.SelectedIndex = 0;
            UpdatePanelDDColC2.Update();
        }

        private void LoadColC2_SmartenPulseOption()
        {
            Label labelC2 = this.FindControl("LabelC2Col") as Label;
            labelC2.Text = "Pulse";
            PulseText.Text = "6";
            PulseText.Visible = true;

            ColC2OSelectButton.Visible = false;
            OptionC2ListBox.Visible = false;
            labelC2.Visible = true;
            PulseText.Visible = true;
            PulseText.Enabled = false;

            UpdatePanelLabelColC2.Update();
            UpdatePanelDDColC2.Update();
        }

        private void LoadColC2_PulseTextBoxOption()
        {
            Label labelC2 = this.FindControl("LabelC2Col") as Label;
            labelC2.Text = "Pulse";
            PulseText.Text = String.Empty;
            PulseText.Visible = true;
            PulseText.Enabled = true;

            ColC2OSelectButton.Visible = false;
            OptionC2ListBox.Visible = false;
            labelC2.Visible = true;
            PulseText.Visible = true;
            PulseText.Attributes.CssStyle.Add("background-color", "white");

            UpdatePanelLabelColC2.Update();
            UpdatePanelDDColC2.Update();
        }

        private void LoadColC2_AccessoriesTextBoxOption()
        {
            Label labelC2 = this.FindControl("LabelC2Col") as Label;
            labelC2.Text = "Accessories";
            PulseText.Text = String.Empty;
            PulseText.Visible = true;
            PulseText.Enabled = true;

            ColC2OSelectButton.Visible = false;
            OptionC2ListBox.Visible = false;
            labelC2.Visible = true;
            PulseText.Visible = true;
            PulseText.Attributes.CssStyle.Add("background-color", "white");

            UpdatePanelLabelColC2.Update();
            UpdatePanelDDColC2.Update();
        }

        private void LoadColC2_SpocPulseOptions()
        {
            OptionC2ListBox.Items.Clear();
            OptionC2ListBox.Items.Add(new ListItem("Select Pulse...", "Select Pulse..."));
            OptionC2ListBox.Items.Add(new ListItem("6", "6"));
            //OptionC2ListBox.Items.Add(new ListItem("12", "12"));
            //OptionC2ListBox.Items.Add(new ListItem("18", "18"));
            //OptionC2ListBox.Items.Add(new ListItem("24", "24"));
            OptionC2ListBox.Items.Add(new ListItem("AFE", "AFE"));
            OptionC2ListBox.Visible = true;

            RefreshColC2();
        }

        private void RefreshColC2()
        {
            OptionC2ListBox.Items[0].Selected = true;
            OptionC2ListBox.Visible = true;
            OptionC2ListBox.Focus();

            UpdatePanelLabelColC2.Update();
            UpdatePanelDDColC2.Update();
        }

        private void LoadColC2_SelectionButton(string labeltext, string selectiontext)
        {
            ColC2OSelectButton.Text = selectiontext;
            ColC2OSelectButton.Visible = true;
            Label labelC2 = this.FindControl("LabelC2Col") as Label;
            labelC2.Text = labeltext;

            labelC2.Visible = true;
            UpdatePanelDDColC2.Update();
            UpdatePanelLabelColC2.Update();
        }

        protected void OptionC2ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ClearColumnF();
            string surfaceOptionA = TextBoxOption.Text;
            string surfaceOptionB = TextBox1.Text;
            string surfaceOptionC = TextBox2.Text;

            string surfaceOptionC2 = OptionC2ListBox.SelectedValue.ToString();
            TextBox3.Text = surfaceOptionC2;
            ColC2OSelectButton.Text = surfaceOptionC2;
            OptionC2ListBox.Visible = false;
            UpdatePanelDDColC2.Update();
            UpdatePanelTitle.Update();

            if (surfaceOptionB == "Spoc")
                LoadColDE_SpocOptions();
        }

        private void LoadColC2_AccessoriesOptions()
        {
            OptionC2ListBox.Items.Clear();
            OptionC2ListBox.Items.Add(new ListItem("Select Accessory...", "Select Accessory..."));
            OptionC2ListBox.Items.Add(new ListItem("Motor Saver, 520CP", "Motor Saver, 520CP"));
            OptionC2ListBox.Items.Add(new ListItem("Motor Saver, 777", "Motor Saver, 777"));
            OptionC2ListBox.Items.Add(new ListItem("Snsr Backspin Mon w/Probe", "Snsr Backspin Mon w/Probe"));
            OptionC2ListBox.Items.Add(new ListItem("Potential Trafo 1500 Volt", "Potential Trafo 1500 Volt"));
            OptionC2ListBox.Items.Add(new ListItem("Digital Amp Readout", "Digital Amp Readout"));
            OptionC2ListBox.Items.Add(new ListItem("Standard Junction Box", "Standard Junction Box"));

            OptionC2ListBox.Visible = true;

            RefreshColC2();
        }

        /***** Columns DE Methods and Events *****/
        private void ClearColumnsDandE()
        {
            DEOOptionsD_Div.Visible = false;
            voltsAmpsDiv.Visible = false;
            VoltsAmpsLabels.Visible = false;
            VoltsAmpsToggle.Visible = false;
            borderDiv.Visible = false;

            KVATextBox.Text = String.Empty;
            KVATextBox.Enabled = false;
            KVATextBox.Visible = false;
            AmpsTextBox.Text = String.Empty;
            AmpsTextBox.Visible = false;

            VoltsAmpsToggle.ClearSelection();
            OptionDDropdown.Items.Clear();
            OptionEDropdown.Items.Clear();
            OptionDDropdown.Enabled = false;
            OptionEDropdown.Enabled = false;

            OptionDDropdown.Visible = false;
            OptionEDropdown.Visible = false;

            Label labelKVA = this.FindControl("LabelKVA") as Label;
            labelKVA.Visible = false;
            DEOOptionsD_Div.Attributes.CssStyle.Add("border-color", "lightgray");

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void refreshColDE()
        {
            OptionDDropdown.Visible = true;
            OptionEDropdown.Visible = true;
            borderDiv.Visible = true;

            HideColDE_VoltsAmpsText();
            ShowColDE_ToggleOptions();

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        protected void OptionDDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int dDropdownIndex = 0;

            dDropdownIndex = OptionDDropdown.SelectedIndex;
            OptionEDropdown.SelectedIndex = dDropdownIndex;
            UpdatePanelDDColDE.Update();

            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = TextBox2.Text;
            string DEOOptionD = OptionDDropdown.SelectedValue.ToString();
            TextBox3.Text = DEOOptionD;
            UpdatePanelTitle.Update();
        }

        protected void OptionEDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eDropdownIndex = 0;

            eDropdownIndex = OptionEDropdown.SelectedIndex;
            OptionDDropdown.SelectedIndex = eDropdownIndex;
            UpdatePanelDDColDE.Update();

            string DEOOptionB = TextBox1.Text;
            string DEOOptionC = TextBox2.Text;
            string DEOOptionD = OptionDDropdown.SelectedValue.ToString();
            TextBox3.Text = DEOOptionD;
            UpdatePanelTitle.Update();
        }

        protected void VoltsAmpsToggle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (VoltsAmpsToggle.SelectedValue.ToString() == "Volts")
            {
                OptionDDropdown.Enabled = true;
                OptionDDropdown.Attributes.CssStyle.Add("background-color", "white");
                OptionEDropdown.Enabled = false;
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "#f8f9fa");
            }
            else if (VoltsAmpsToggle.SelectedValue.ToString() == "Amps")
            {
                OptionDDropdown.Enabled = false;
                OptionDDropdown.Attributes.CssStyle.Add("background-color", "#f8f9fa");
                OptionEDropdown.Enabled = true;
                OptionEDropdown.Attributes.CssStyle.Add("background-color", "white");
            }
            UpdatePanelDDColDE.Update();
        }

        private void HideColDE_ToggleOptions()
        {
            VoltsAmpsToggle.ClearSelection();
            VoltsAmpsToggle.Enabled = false;

            DEOOptionsD_Div.Visible = false;
            voltsAmpsDiv.Visible = false;
            VoltsAmpsToggle.Visible = false;
            OptionDDropdown.Visible = false;
            OptionEDropdown.Visible = false;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void ShowColDE_ToggleOptions()
        {
            VoltsAmpsToggle.Enabled = true;
            VoltsAmpsToggle.SelectedIndex = 0;

            OptionDDropdown.Attributes.CssStyle.Add("background-color", "white");
            OptionDDropdown.Enabled = true;
            OptionEDropdown.Attributes.CssStyle.Add("background-color", "#f8f9fa");
            OptionEDropdown.Enabled = false;

            VoltsAmpsToggle.Visible = true;
            voltsAmpsDiv.Visible = true;
            DEOOptionsD_Div.Visible = true;
            OptionDDropdown.Visible = true;
            OptionEDropdown.Visible = true;
            UpdatePanelLabelColDE.Visible = true;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void HideColDE_VoltsAmpsText()
        {
            DEOOptionsD_Div.Visible = false;
            KVATextBox.Visible = false;
            AmpsTextBox.Visible = false;
            VoltsAmpsLabels.Visible = false;
            DEOOptionsD_Div.Visible = false;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void ShowColDE_VoltsAmpsText()
        {
            VoltsAmpsLabels.Visible = true;
            KVATextBox.Visible = true;
            KVATextBox.Enabled = false;
            AmpsTextBox.Visible = true;
            voltsAmpsDiv.Visible = true;
            DEOOptionsD_Div.Visible = true;

            UpdatePanelLabelColDE.Update();
            UpdatePanelDDColDE.Update();
        }

        private void LoadColD_KVATextBox()
        {
            Label labelKVA = this.FindControl("LabelKVA") as Label;
            labelKVA.Visible = true;
            UpdatePanelLabelColDE.Update();

            DEOOptionsD_Div.Attributes.CssStyle.Add("border-color", "transparent");

            KVATextBox.Text = String.Empty;
            KVATextBox.Visible = true;
            KVATextBox.Enabled = true;
            KVATextBox.Attributes.CssStyle.Add("background-color", "white");
            DEOOptionsD_Div.Visible = true;
            UpdatePanelDDColDE.Update();

        }

        private void LoadColDE_SFL_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem(" Select KVA ...", " Select KVA ..."));
            OptionDDropdown.Items.Add(new ListItem("166", "166"));
            OptionDDropdown.Items.Add(new ListItem("251", "251"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem(" Select Amps ...", " Select Amps ..."));
            OptionEDropdown.Items.Add(new ListItem("200", "200"));
            OptionEDropdown.Items.Add(new ListItem("302", "302"));

            refreshColDE();
        }

        private void LoadColDE_SLH_Options()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem(" Select KVA ...", " Select KVA ..."));
            OptionDDropdown.Items.Add(new ListItem("345", "345"));
            OptionDDropdown.Items.Add(new ListItem("400", "400"));
            OptionDDropdown.Items.Add(new ListItem("500", "500"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem(" Select Amps ...", " Select Amps ..."));
            OptionEDropdown.Items.Add(new ListItem("415", "415"));
            OptionEDropdown.Items.Add(new ListItem("481", "481"));
            OptionEDropdown.Items.Add(new ListItem("600", "600"));

            refreshColDE();
        }

        private void LoadColDE_SpocOptions()
        {
            OptionDDropdown.Items.Clear();
            OptionDDropdown.Items.Add(new ListItem(" Select KVA ...", " Select KVA ..."));
            OptionDDropdown.Items.Add(new ListItem("116", "116"));
            OptionDDropdown.Items.Add(new ListItem("141", "141"));
            OptionDDropdown.Items.Add(new ListItem("170", "170"));
            OptionDDropdown.Items.Add(new ListItem("217", "217"));
            OptionDDropdown.Items.Add(new ListItem("249", "249"));
            OptionDDropdown.Items.Add(new ListItem("319", "319"));
            OptionDDropdown.Items.Add(new ListItem("382", "382"));
            OptionDDropdown.Items.Add(new ListItem("432", "432"));
            OptionDDropdown.Items.Add(new ListItem("490", "490"));
            OptionDDropdown.Items.Add(new ListItem("540", "540"));

            OptionEDropdown.Items.Clear();
            OptionEDropdown.Items.Add(new ListItem(" Select Amps ...", " Select Amps ..."));
            OptionEDropdown.Items.Add(new ListItem("140", "140"));
            OptionEDropdown.Items.Add(new ListItem("170", "170"));
            OptionEDropdown.Items.Add(new ListItem("205", "205"));
            OptionEDropdown.Items.Add(new ListItem("261", "261"));
            OptionEDropdown.Items.Add(new ListItem("300", "300"));
            OptionEDropdown.Items.Add(new ListItem("385", "385"));
            OptionEDropdown.Items.Add(new ListItem("460", "460"));
            OptionEDropdown.Items.Add(new ListItem("520", "520"));
            OptionEDropdown.Items.Add(new ListItem("590", "590"));
            OptionEDropdown.Items.Add(new ListItem("650", "650"));

            refreshColDE();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            string errorMsg = "";
            errorMsg = validateTextBoxes();
            if (errorMsg.Length == 0)
            {
                StringBuilder emailBody = createBody("email");
                StringBuilder emailSubject = createSubject("email");
                sendEmail(emailBody, emailSubject);

                TechConnect_Utilities util = new TechConnect_Utilities();
                bool sendText = util.CheckSeverity(SeverityDropDownList);
                if (sendText)
                {
                    StringBuilder textBody = createBody("text");
                    sendTextMsg(textBody);
                }

                UpdatePanelSubmitButton.Update();
                util.CleanUploadsFolder();

                ModalPopupLabel1.Text = "Thank you for your submission!";
                ModalPopupLabel2.Text = "A ticket will be created and sent to your email address. ";
                ModalPopupLabel3.Text = "The ticket generator will be ‘Service Desk Plus Ticketing System’.";
                if (sendText)
                    ModalPopupLabel4.Text = "A text will also be sent to a staff member for immediate assistance.";

                TextBox8.Text = "Exit";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
            else
            {
                ModalPopupLabel1.Text = "Error:";
                ModalPopupLabel2.Text = errorMsg;
                ModalPopupLabel3.Text = "";
                ModalPopupLabel4.Text = "";
                TextBox8.Text = "Stay";
                UpdatePanelTitle.Update();
                UpdatePanel1Modalpopup.Update();
                mp1.Show();
            }
        }

        private void sendEmail(StringBuilder emailBody, StringBuilder emailSubject)
        {
            String userEmailAddress = Session["s_UserEmail"].ToString();
            String supportEmailAddress = Session["s_SupportEmail"].ToString();
            String supportEmailPassword = Session["s_SupportPswd"].ToString();
            String sendGridUserName = Session["s_SendGridUserName"].ToString();
            String sendGridPassword = Session["s_SendGridPassword"].ToString();
            String ticketEmailAddress = Session["s_TicketingEmail"].ToString();

            //String ticketEmailAddress = "tbairok@gmail.com";

            using (MailMessage message = new MailMessage(supportEmailAddress, ticketEmailAddress, emailSubject.ToString(), emailBody.ToString()))
            {
                AddAttachments(message);
                message.ReplyToList.Add(userEmailAddress);
                SmtpClient emailClient = new SmtpClient("smtp.sendgrid.net", 587);
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = new System.Net.NetworkCredential(sendGridUserName, sendGridPassword);
                emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                emailClient.EnableSsl = false;

                try
                {
                    emailClient.Send(message);
                }
                catch (SmtpFailedRecipientException ex)
                {

                }
            }
        }

        private void sendTextMsg(StringBuilder textBody)
        {
            var accountSid = Session["s_TwilioAcctID"].ToString();
            var authToken = Session["s_TwilioAuthToken"].ToString();
            var fromPhoneNum = Session["s_TwilioFromPhoneNum"].ToString();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            TwilioClient.Init(accountSid, authToken);
            List<string> TextNums = new List<string>();

            string textnum1 = Session["s_TwilioFromPhoneNum1"].ToString();
            if (textnum1 != "")
                TextNums.Add(textnum1);
            string textnum2 = Session["s_TwilioFromPhoneNum2"].ToString();
            if (textnum2 != "")
                TextNums.Add(textnum2);
            string textnum3 = Session["s_TwilioFromPhoneNum3"].ToString();
            if (textnum3 != "")
                TextNums.Add(textnum3);
            string textnum4 = Session["s_TwilioFromPhoneNum4"].ToString();
            if (textnum4 != "")
                TextNums.Add(textnum4);

            foreach (var textnum in TextNums)
            {
                var messageOptions = new CreateMessageOptions(
                new PhoneNumber(textnum));

                messageOptions.From = new PhoneNumber(fromPhoneNum);
                messageOptions.Body = textBody.ToString();

                var message = MessageResource.Create(messageOptions);
                Console.WriteLine(message.Body);
            }
        }

        private StringBuilder createSubject(string messagetype)
        {
            string surfaceOption = SurfaceOptions.SelectedItem.Text;
            string severityLable = LabelSeverity.Text;
            string sLevel = "";

            String sColText = "";
            if (SeverityDropDownList.Items.Count > 0)      
                sColText = SeverityDropDownList.SelectedItem.Text;
          
            int colonIndex = 0;
            colonIndex = sColText.IndexOf(":");
            sLevel = sColText.Substring(0, colonIndex);

            StringBuilder emailSubject = new StringBuilder();
            emailSubject.Append("TECH CONNECT - Surface Option: ");
            emailSubject.Append(surfaceOption + "; ");
            emailSubject.Append("Severity: " + sLevel);

            return emailSubject;
        }

        private void AddAttachments(MailMessage message)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            string uploadPath = util.GetUploadPath();
            var folderPath = HttpContext.Current.Server.MapPath(uploadPath);

            bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
            if (userUploadupPathExists)
            {
                foreach (string file in Directory.GetFiles(folderPath))
                {
                    Attachment attachment = new Attachment(file);
                    message.Attachments.Add(attachment);
                }
            }
        }

        private string validateTextBoxes()
        {
            string errMsg = "";
            Regex r = new Regex("^[a-zA-Z0-9-@':&,.!?/_$ ]*$");

            if (CompetitorNameText.Text.Length > 0)
            {
                string cNameString = CompetitorNameText.Text;
                if (!r.IsMatch(cNameString))
                {
                    char[] competitorArray = cNameString.ToCharArray();
                    for (int i = 0; i < competitorArray.Length; i++)
                    {
                        string commentsChar = competitorArray[i].ToString();
                        if (!r.IsMatch(commentsChar))
                        {
                            errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character from Competitor Name.";
                            i = competitorArray.Length;
                        }
                    }
                }
            }

            if (errMsg == "")
            {
                if (PulseText.Text.Length > 0)
                {
                    string pulseString = PulseText.Text;
                    if (!r.IsMatch(pulseString))
                    {
                        char[] pulseArray = pulseString.ToCharArray();
                        for (int i = 0; i < pulseArray.Length; i++)
                        {
                            string commentsChar = pulseArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character from Pulse.";
                                i = pulseArray.Length;
                            }
                        }
                    }
                }
            }

            if (errMsg == "")
            {
                if (AdditionalCommentsTextBox.Text.Length > 0)
                {
                    string commentString = AdditionalCommentsTextBox.Text;
                    if (!r.IsMatch(commentString))
                    {
                        char[] commentsArray = commentString.ToCharArray();
                        for (int i = 0; i < commentsArray.Length; i++)
                        {
                            string commentsChar = commentsArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character.";
                                i = commentsArray.Length;
                            }
                        }  
                    }
                }
                else
                    errMsg = "Please enter 'Comments' before submitting ticket.";
            }
            return errMsg;
        }

        private StringBuilder createBody(string messagetype)
        {
            string userEmail = Session["s_UserEmail"] as string;

            int endPos = userEmail.IndexOf("@");
            string currentUser = userEmail.Substring(0, endPos);

            string bColLabel = "";
            string bColText = "";
            if (OptionBDropdown.Items.Count > 0)
            {
                if (OptionBDropdown.SelectedIndex > 0)
                {
                    bColLabel = LabelBCol.Text;
                    bColText = OptionBDropdown.SelectedItem.Text;
                }
            }

            string cColLabel = "";
            string cColText = "";
            if (OptionCListBox.Items.Count > 0) {
                if (OptionCListBox.SelectedIndex > 0) {
                    cColLabel = LabelCCol.Text;
                    cColText = ColCOSelectButton.Text;
                }
            }
            else{
                if (CompetitorNameText.Text.Length > 0)
                {
                    cColLabel = LabelCCol.Text;
                    cColText = CompetitorNameText.Text;
                }
            }

            string cCol2Label = "";
            string cCol2Text = "";
            if (OptionC2ListBox.Items.Count > 0)
            {
                if (OptionC2ListBox.SelectedIndex > 0)
                {
                    cCol2Label = LabelC2Col.Text;
                    cCol2Text = ColC2OSelectButton.Text;
                }
            }
            else{
                if (PulseText.Text.Length > 0)
                {
                    cCol2Label = LabelC2Col.Text;
                    cCol2Text = PulseText.Text;
                }
            }

            string vLable = "";
            string aLable = "";
            string dColText = "";
            string eColText = "";
            if (OptionDDropdown.Items.Count > 0)
            {
                if (OptionDDropdown.SelectedIndex > 0)
                {
                    vLable = LabelVolts.Text;
                    aLable = LabelAmps.Text;
                    dColText = OptionDDropdown.SelectedItem.Text;
                    eColText = OptionEDropdown.SelectedItem.Text;
                }
            }
            else{
                if (KVATextBox.Text.Length > 0)
                {
                    vLable = LabelVolts.Text;
                    dColText = KVATextBox.Text;

                    if (AmpsTextBox.Text.Length > 0)
                    {
                        aLable = LabelAmps.Text;
                        eColText = AmpsTextBox.Text;
                    }
                }
            }

            string addComentsLable = LabelAddComments.Text;
            string addComentsText = "";
            if (AdditionalCommentsTextBox.Text.Length > 0) addComentsText = AdditionalCommentsTextBox.Text;

            string severityLable = LabelSeverity.Text;
            string sColText = "";
            if (SeverityDropDownList.Items.Count > 0) sColText = SeverityDropDownList.SelectedItem.Text;

            StringBuilder emailBody = new StringBuilder();
            string surfaceOption = SurfaceOptions.SelectedItem.Text;

            if (messagetype == "email")
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("");

                emailBody.AppendLine("Requester's Name: " + "\t" + "\t" + currentUser);
                emailBody.AppendLine("Requester's Email: " + "\t" + "\t" + userEmail);

                emailBody.AppendLine("");
                emailBody.AppendLine("Surface Option: " + "\t" + "\t" + surfaceOption);

                if (bColText.Length > 0)               
                    emailBody.AppendLine(bColLabel + ": " + "\t" + "\t" + bColText);

                if (cColText.Length > 0)               
                    emailBody.AppendLine(cColLabel + ": " + "\t" + "\t" + cColText);

                if (cCol2Text.Length > 0)
                    emailBody.AppendLine(cCol2Label + ": " + "\t" + "\t" + "\t" + "\t" + cCol2Text);

                if (dColText.Length > 0 )
                {
                    if (eColText.Length > 0)
                        emailBody.AppendLine(vLable + ": " + "\t" + "\t" + dColText + "\t" + "\t" + aLable + ": " + "\t" + "\t" + eColText);                  
                    else               
                        emailBody.AppendLine(vLable + ": " + "\t" + "\t" + dColText);                    
                }

                emailBody.AppendLine("Comments: " + "\t" + "\t" + "\t" + addComentsText);
                emailBody.AppendLine("Severity: " + "\t" + "\t" + "\t" + sColText);
            }
            else
            {
                emailBody.AppendLine("");
                emailBody.AppendLine("TECH CONNECT Ticket Request ");
                emailBody.AppendLine("Severity: Immediate");
                emailBody.AppendLine("");
                emailBody.AppendLine("Requester's Name: " + currentUser);
                emailBody.AppendLine("Requester's Email: " + userEmail);
                emailBody.AppendLine("Surface Option: " + surfaceOption);

                if (bColText.Length > 0) emailBody.AppendLine(bColLabel + ": " + bColText);
                if (cColText.Length > 0) emailBody.AppendLine(cColLabel + ": " + cColText);
                if (dColText.Length > 0)
                {
                    if (eColText.Length > 0)
                        emailBody.AppendLine(vLable + ": " + dColText + "; " + aLable + ": " + eColText);
                    else
                        emailBody.AppendLine(vLable + ": " + dColText);
                }

                emailBody.AppendLine("Comments: " + addComentsText);
            }

            return emailBody;
        }

        protected void AFButton_Click(object sender, EventArgs e)
        {
            LabelMaxFiles.Text = String.Empty;
            UpdatePanelAFButton.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();

            if (FileUploadDiv.Visible == false)
            {
                ListBoxFileUpload.Items.Clear();
                FileUploadDiv.Visible = true;
            }
            else
            {
                FileUploadDiv.Visible = false;
                UpdatePanelFileUpload.Update();
            }

            UpdatePanelFileUpload.Update();
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            var postedFile = FileUpload.PostedFile;
            var newAttachment = FileUpload.PostedFile.FileName.ToString();

            TechConnect_Utilities util = new TechConnect_Utilities();
            bool canUpload = util.CheckUploadFiles(postedFile, ListBoxFileUpload, LabelMaxFiles, FileUpload, UpdatePanelFileUpload);
            if (canUpload)
            {
                ListBoxFileUpload.Items.Add(new ListItem(newAttachment, newAttachment));
                ListBoxFileUpload.SelectedIndex = -1;
                string uploadPath = util.GetUploadPath();

                bool userUploadupPathExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(uploadPath));
                if (!userUploadupPathExists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(uploadPath));

                FileUpload.PostedFile.SaveAs(Server.MapPath(uploadPath + newAttachment));
            }

            UpdatePanelFileUpload.Update();
        }


        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            int selectedFileIndex = ListBoxFileUpload.SelectedIndex;

            if (selectedFileIndex == -1) LabelMaxFiles.Text = "Please select file to delete.";
            else{
                LabelMaxFiles.Text = "";
                string fileName = ListBoxFileUpload.SelectedValue;
                ListBoxFileUpload.Items.RemoveAt(selectedFileIndex);

                string FileToDelete;
                TechConnect_Utilities util = new TechConnect_Utilities();
                string uploadPath = util.GetUploadPath();

                FileToDelete = Server.MapPath(uploadPath + fileName);
                File.Delete(FileToDelete);
            }

            UpdatePanelFileUpload.Update();
        }

        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            ListBoxFileUpload.Items.Clear();
            UpdatePanelFileUpload.Update();

            TechConnect_Utilities util = new TechConnect_Utilities();
            util.CleanUploadsFolder();
        }
        
        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void HomeButton_Click(object sender, EventArgs e)
        {
            TechConnect_Utilities util = new TechConnect_Utilities();
            util.DeleteUserUploadFolder();

            var TechConnectHomeURL = Session["DefaultUrl"].ToString();
            Response.Redirect(TechConnectHomeURL, false);
        }
    }
    }

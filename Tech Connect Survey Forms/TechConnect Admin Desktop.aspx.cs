﻿using System;
using Microsoft.Ajax.Utilities;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Security.Cryptography;
using Tech_Connect_Survey_Forms.Properties;

namespace Tech_Connect_Survey_Forms
{
    public partial class TechConnect_Admin_Desktop : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetContactData();
                GetAdminData();
                TextBox_defaultURL.Text = Session["DefaultUrl"].ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserName.Attributes.Add("autocomplete", "off");
                Password.Attributes.Add("autocomplete", "off");
                UserName.Attributes["value"] = "";
                Password.Attributes["value"] = "";
                Label failureText = (Label)this.FindControl("FailureText");
                failureText.Text = " ";
                UpdatePanelUserName.Update();
                UpdatePanelPassword.Update();
                UpdatePanelFailureText.Update();
            }

            UpdatePanelForm.Update();
        }

        private void GetContactData()
        {
            GetContact1Data();
            GetContact2Data();
            GetContact3Data();
            GetContact4Data();
        }

        private void GetAdminData()
        {
            GetAdmin1Data();
            GetAdmin2Data();
            //GetAdmin3Data();
            //GetAdmin4Data();
        }

        private void GetContact1Data()
        {
            var twilioTextName1 = Properties.Settings.Default.TwilioTextName1;
            Name1.Text = (twilioTextName1 == "none") ? "" : twilioTextName1;
            var twilioTextNum1 = Properties.Settings.Default.TwilioTextNum1;
            PhoneNum1.Text = (twilioTextNum1 == "none" || twilioTextNum1 == "") ? "" : FormatCellNum(twilioTextNum1);
            var ttNumActive1 = Properties.Settings.Default.TwilioNum1Active;
            Contact1Toggle.SelectedValue = ttNumActive1;

            ContactName1.Update();
            ContactPhone1.Update();
            ContactToggle1.Update();
        }

        private void GetContact2Data()
        {
            var twilioTextName2 = Properties.Settings.Default.TwilioTextName2;
            Name2.Text = (twilioTextName2 == "none") ? "" : twilioTextName2;
            var twilioTextNum2 = Properties.Settings.Default.TwilioTextNum2;
            PhoneNum2.Text = (twilioTextNum2 == "none" || twilioTextNum2 == "") ? "" : FormatCellNum(twilioTextNum2);
            var ttNumActive2 = Properties.Settings.Default.TwilioNum2Active;
            Contact2Toggle.SelectedValue = ttNumActive2;

            ContactName2.Update();
            ContactPhone2.Update();
            ContactToggle2.Update();
        }

        private void GetContact3Data()
        {
            var twilioTextName3 = Properties.Settings.Default.TwilioTextName3;
            Name3.Text = (twilioTextName3 == "none") ? "" : twilioTextName3;
            var twilioTextNum3 = Properties.Settings.Default.TwilioTextNum3;
            PhoneNum3.Text = (twilioTextNum3 == "none" || twilioTextNum3 == "") ? "" : FormatCellNum(twilioTextNum3);
            var ttNumActive3 = Properties.Settings.Default.TwilioNum3Active;
            Contact3Toggle.SelectedValue = ttNumActive3;

            ContactName3.Update();
            ContactPhone3.Update();
            ContactToggle3.Update();
        }

        private void GetContact4Data()
        {
            var twilioTextName4 = Properties.Settings.Default.TwilioTextName4;
            Name4.Text = (twilioTextName4 == "none") ? "" : twilioTextName4;
            var twilioTextNum4 = Properties.Settings.Default.TwilioTextNum4;
            PhoneNum4.Text = (twilioTextNum4 == "none" || twilioTextNum4 == "") ? "" : FormatCellNum(twilioTextNum4);
            var ttNumActive4 = Properties.Settings.Default.TwilioNum4Active;
            Contact4Toggle.SelectedValue = ttNumActive4;

            ContactName4.Update();
            ContactPhone4.Update();
            ContactToggle4.Update();
        }

        private void GetAdmin1Data()
        {
            var adminEmail1 = Properties.Settings.Default.AdminEmail1;
            AdminEmail1.Text = (adminEmail1 == "none") ? "" : adminEmail1;

            UpdatePanelAdmin1.Update();
        }

        private void GetAdmin2Data()
        {
            var adminEmail2 = Properties.Settings.Default.AdminEmail2;
            AdminEmail2.Text = (adminEmail2 == "none") ? "" : adminEmail2;

            UpdatePanelAdmin2.Update();
        }
        /*
        private void GetAdmin3Data()
        {
            var adminEmail3 = Properties.Settings.Default.AdminEmail3;
            AdminEmail3.Text = (adminEmail3 == "none") ? "" : adminEmail3;

            UpdatePanelAdmin3.Update();
        }

        private void GetAdmin4Data()
        {
            var adminEmail4 = Properties.Settings.Default.AdminEmail4;
            AdminEmail4.Text = (adminEmail4 == "none") ? "" : adminEmail4;

            UpdatePanelAdmin4.Update();
        }*/

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");

            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");

            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("key");

            string plaintext = null;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        private string DecryptStringAES(string encryptedValue)
        {
            String encryptionKey = Session["s_EncryptionKey"].ToString();
            var keybytes = Encoding.UTF8.GetBytes(encryptionKey);
            var iv = Encoding.UTF8.GetBytes(encryptionKey);
            int encryptedLength = encryptedValue.Length;

            //DECRYPT FROM CRIPTOJS
            encryptedValue = encryptedValue.Replace(' ', '+');
            var encrypted = Convert.FromBase64String(encryptedValue);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);

            return decriptedFromJavascript;
        }

        private void GetContact(string TextName, string TextNum, string TextNumActive, TextBox Name, TextBox PhoneNum, RadioButtonList ToggleName)
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");
            String settingName = "";
            String settingValue = "";
            string[] textContact = new string[] { "", "", "" };

            XmlReader xmlReader = XmlReader.Create(pathName);
            while (xmlReader.Read())
            {
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "setting"))
                {
                    if (xmlReader.HasAttributes)
                    {
                        settingName = xmlReader.GetAttribute("name");
                        if (settingName == TextName)
                        {
                            xmlReader.ReadToFollowing("value");
                            settingValue = xmlReader.ReadElementContentAsString();
                            textContact[0] = settingValue;
                        }
                        if (settingName == TextNum)
                        {
                            xmlReader.ReadToFollowing("value");
                            settingValue = xmlReader.ReadElementContentAsString();
                            textContact[1] = settingValue;
                        }
                        if (settingName == TextNumActive)
                        {
                            xmlReader.ReadToFollowing("value");
                            settingValue = xmlReader.ReadElementContentAsString();
                            textContact[2] = settingValue;
                        }
                    }
                }
            }

            if (textContact[0].Length > 0)
            {
                Name.Text = textContact[0];
                string textNum1 = textContact[1];
                string formattedCell = FormatCellNum(textNum1);
                PhoneNum.Text = formattedCell;
                string active1 = textContact[2];
                ToggleName.SelectedValue = active1;
            }
            else
            {
                Name.Text = "";
                PhoneNum.Text = "";
                ToggleName.SelectedValue = "No";
            }

            xmlReader.Close();
        }

        private string FormatCellNum(string textNum)
        {
            string formattedCell = "";
            textNum = textNum.Substring(2);
            string cellAreaCode = textNum.Substring(0, 3);
            string cellPrefix = textNum.Substring(3, 3);
            string cellNum = textNum.Substring(6, 4);
            formattedCell = "(" + cellAreaCode + ")" + " " + cellPrefix + "-" + cellNum;

            return formattedCell;
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            String userName = UserName.Text;
            String adminPassWord = Password.Text;

            String TechConnectAdminPassword = Properties.Settings.Default.AdminPassword;
            String AdminEmail1 = Properties.Settings.Default.AdminEmail1;
            String AdminEmail2 = Properties.Settings.Default.AdminEmail2;

            Label failureText = (Label)this.FindControl("FailureText");
            if ((userName == AdminEmail1 || userName == AdminEmail2) && adminPassWord == TechConnectAdminPassword)
            //if (adminPassWord == TechConnectAdminPassword)
            {
                failureText.Text = "";
                UserName.Text = "";
                Password.Text = "";
                LoginDiv.Attributes.CssStyle.Add("display", "none");
                TextboxesDiv.Attributes.CssStyle.Add("display", "block");
                MainDiv.Attributes.CssStyle.Add("opacity", "1.0");
            }
            else
            {
                if (userName == "")
                    failureText.Text = "'User Email' is required.";
                else if (userName != AdminEmail1 || userName != AdminEmail2)
                    failureText.Text = "'User Email' is incorrect. Please re-enter.";
                else if (adminPassWord == "")
                    failureText.Text = "'Admin Password' is required.";
                else if (adminPassWord != TechConnectAdminPassword)
                    failureText.Text = "'Admin Password' is incorrect. Please re-enter.";
            }

            UpdatePanelFailureText.Update();
        }

        private string ValidateCellNum(string cellNum, TextBox phoneNum)
        {
            string errMsg = "";
            string formattedPhoneNumber = "";
            Regex phoneRegex =
            new Regex(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");

            if (phoneRegex.IsMatch(cellNum))
            {
                formattedPhoneNumber =
                    phoneRegex.Replace(cellNum, "($1) $2-$3");
            }
            else
                errMsg = "invalid phone number. Please re-enter.";

            phoneNum.Text = formattedPhoneNumber;
            return errMsg;
        }

        private string validateContactName(string name)
        {
            string errMsg = "";
            if (errMsg == "")
            {
                if (name.Length > 0)
                {
                    string commentString = name;
                    Regex r = new Regex("^[a-zA-Z0-9-@':&,.!?/_$ ]*$");
                    if (!r.IsMatch(commentString))
                    {
                        char[] commentsArray = commentString.ToCharArray();
                        for (int i = 0; i < commentsArray.Length; i++)
                        {
                            string commentsChar = commentsArray[i].ToString();
                            if (!r.IsMatch(commentsChar))
                            {
                                errMsg = "Invalid entry. Please remove  '" + commentsChar + "'  character from 'Contact Name'.";
                                i = commentsArray.Length;
                            }
                        }
                    }
                }
                else
                    errMsg = "Please enter 'Contact Name'.";
            }
            return errMsg;
        }

        private string ValidateAdminEmail(TextBox adminEmail)
        {  
            string errMsg = "";
            string email = adminEmail.Text;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            Match match = regex.Match(email);
            if (!match.Success)
                errMsg = email + " is Invalid Email Address";

            return errMsg;
        }

        private bool SaveContact(String TextName, String TextNum, String NumActive, String newName, String newNum, String newActive)
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");
            string formattedTextNum = newNum.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            formattedTextNum = "+1" + formattedTextNum;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathName);
            XmlNodeList userNodes = xmlDoc.SelectNodes("//Tech_Connect_Survey_Forms.Properties.Settings/setting");
            foreach (XmlNode userNode in userNodes)
            {
                string name = userNode.Attributes["name"].Value;

                if (name == TextName)
                {
                    XmlNode nameSettingVal = userNode.FirstChild;
                    string oldName = nameSettingVal.InnerText;
                    nameSettingVal.InnerText = newName;
                }
                if (name == TextNum)
                {
                    XmlNode numSettingVal = userNode.FirstChild;
                    string oldNum = numSettingVal.InnerText;
                    numSettingVal.InnerText = formattedTextNum;
                }
                if (name == NumActive)
                {
                    XmlNode numActiveSettingVal = userNode.FirstChild;
                    string oldActive = numActiveSettingVal.InnerText;
                    numActiveSettingVal.InnerText = newActive;
                }
            }

            try
            {
                xmlDoc.Save(pathName);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
    
        private bool SaveAdminEmail(String EmailNum, String newEmailName)
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathName);
            XmlNodeList userNodes = xmlDoc.SelectNodes("//Tech_Connect_Survey_Forms.Properties.Settings/setting");
            foreach (XmlNode userNode in userNodes)
            {
                string name = userNode.Attributes["name"].Value;
                if (name == EmailNum)
                {
                    XmlNode nameSettingVal = userNode.FirstChild;
                    string oldName = nameSettingVal.InnerText;
                    nameSettingVal.InnerText = newEmailName;
                }
            }

            try
            {
                xmlDoc.Save(pathName);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
      
        private bool ClearContact(String TextName, String TextNum, String NumActive)
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathName);
            XmlNodeList userNodes = xmlDoc.SelectNodes("//Tech_Connect_Survey_Forms.Properties.Settings/setting");
            foreach (XmlNode userNode in userNodes)
            {
                string name = userNode.Attributes["name"].Value;

                if (name == TextName)
                {
                    XmlNode nameSettingVal = userNode.FirstChild;
                    string oldName = nameSettingVal.InnerText;
                    nameSettingVal.InnerText = "none";
                }
                if (name == TextNum)
                {
                    XmlNode numSettingVal = userNode.FirstChild;
                    string oldNum = numSettingVal.InnerText;
                    numSettingVal.InnerText = "none";
                }
                if (name == NumActive)
                {
                    XmlNode numActiveSettingVal = userNode.FirstChild;
                    string oldActive = numActiveSettingVal.InnerText;
                    numActiveSettingVal.InnerText = "No";
                }
            }

            try
            {
                xmlDoc.Save(pathName);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        private bool ClearAdminEmail(String AdminEmail)
        {
            string pathName = HttpContext.Current.Server.MapPath("~/Web.config");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathName);
            XmlNodeList userNodes = xmlDoc.SelectNodes("//Tech_Connect_Survey_Forms.Properties.Settings/setting");
            foreach (XmlNode userNode in userNodes)
            {
                string name = userNode.Attributes["name"].Value;

                if (name == AdminEmail)
                {
                    XmlNode nameSettingVal = userNode.FirstChild;
                    string oldName = nameSettingVal.InnerText;
                    nameSettingVal.InnerText = "none";
                }
            }

            try
            {
                xmlDoc.Save(pathName);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;

        }

        protected void ButtonSave1_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name1.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum1.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum1);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact1Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName1", "TwilioTextNum1", "TwilioNum1Active", newContactName, newContactNum, newIsActive);
                            if (success)
                            {
                                Contact1Error.Text = "Contact information has been updated.";
                                ContactName1.Update();
                                ContactPhone1.Update();
                                ContactToggle1.Update();
                            }
                            else
                                Contact1Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact1Error.Text = cellNumErrorMsg;
                    }
                    else
                        Contact1Error.Text = "Please enter 'Contact Cell Number'.";
                }
                else
                    Contact1Error.Text = contactNameErrorMsg;
            }
            else
                Contact1Error.Text = "Please enter 'Contact Name'.";

            UpdatePanelContact1Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonCancel1_Click(object sender, EventArgs e)
        {
            GetContact1Data();
            Contact1Error.Text = "";

            UpdatePanelContact1Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonClear1_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName1", "TwilioTextNum1", "TwilioNum1Active");
            if (success)
            {
                Name1.Text = "";
                PhoneNum1.Text = "";
                Contact1Toggle.SelectedValue = "No";
                Contact1Error.Text = "Contact information has been cleared.";

                ContactName1.Update();
                ContactPhone1.Update();
                ContactToggle1.Update();
            }
            else
                Contact1Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact1Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonSave2_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name2.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum2.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum2);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact2Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName2", "TwilioTextNum2", "TwilioNum2Active", newContactName, newContactNum, newIsActive);
                            if (success)
                            {
                                Contact2Error.Text = "Contact information has been updated.";
                                ContactName2.Update();
                                ContactPhone2.Update();
                                ContactToggle2.Update();
                            }
                            else
                                Contact2Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact2Error.Text = cellNumErrorMsg;
                    }
                    else
                        Contact2Error.Text = "Please enter 'Contact Cell Number'.";
                }
                else
                    Contact2Error.Text = contactNameErrorMsg;
            }
            else
                Contact2Error.Text = "Please enter 'Contact Name'.";

            UpdatePanelContact2Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonCancel2_Click(object sender, EventArgs e)
        {
            GetContact2Data();
            Contact2Error.Text = "";

            UpdatePanelContact2Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonClear2_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName2", "TwilioTextNum2", "TwilioNum2Active");
            if (success)
            {
                Name2.Text = "";
                PhoneNum2.Text = "";
                Contact2Toggle.SelectedValue = "No";
                Contact2Error.Text = "Contact information has been cleared.";

                ContactName2.Update();
                ContactPhone2.Update();
                ContactToggle2.Update();
            }
            else
                Contact2Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact2Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonSave3_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name3.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum3.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum3);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact3Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName3", "TwilioTextNum3", "TwilioNum3Active", newContactName, newContactNum, newIsActive);
                            if (success)
                            {
                                Contact3Error.Text = "Contact information has been updated.";
                                ContactName3.Update();
                                ContactPhone3.Update();
                                ContactToggle3.Update();
                            }
                            else
                                Contact3Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact3Error.Text = cellNumErrorMsg;
                    }
                    else
                        Contact3Error.Text = "Please enter 'Contact Cell Number'.";
                }
                else
                    Contact3Error.Text = contactNameErrorMsg;
            }
            else
                Contact3Error.Text = "Please enter 'Contact Name'.";

            UpdatePanelContact3Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonCancel3_Click(object sender, EventArgs e)
        {
            GetContact3Data();
            Contact3Error.Text = "";

            UpdatePanelContact3Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonClear3_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName3", "TwilioTextNum3", "TwilioNum3Active");
            if (success)
            {
                Name3.Text = "";
                PhoneNum3.Text = "";
                Contact3Toggle.SelectedValue = "No";
                Contact3Error.Text = "Contact information has been cleared.";

                ContactName3.Update();
                ContactPhone3.Update();
                ContactToggle3.Update();
            }
            else
                Contact3Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact3Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonSave4_Click(object sender, EventArgs e)
        {
            string contactNameErrorMsg = "";
            string newContactName = Name4.Text;
            if (newContactName != "")
            {
                contactNameErrorMsg = validateContactName(newContactName);
                if (contactNameErrorMsg == "")
                {
                    string newContactNum = PhoneNum4.Text;
                    if (newContactNum != "")
                    {
                        string cellNumErrorMsg = ValidateCellNum(newContactNum, PhoneNum4);
                        if (cellNumErrorMsg == "")
                        {
                            string newIsActive = Contact4Toggle.SelectedValue;
                            bool success = SaveContact("TwilioTextName4", "TwilioTextNum4", "TwilioNum4Active", newContactName, newContactNum, newIsActive);
                            if (success)
                            {
                                Contact4Error.Text = "Contact information has been updated.";
                                ContactName4.Update();
                                ContactPhone4.Update();
                                ContactToggle4.Update();
                            }
                            else
                                Contact4Error.Text = "Error updating information. Please try again.";
                        }
                        else
                            Contact4Error.Text = cellNumErrorMsg;
                    }
                    else
                        Contact4Error.Text = "Please enter 'Contact Cell Number'.";
                }
                else
                    Contact4Error.Text = contactNameErrorMsg;
            }
            else
                Contact4Error.Text = "Please enter 'Contact Name'.";

            UpdatePanelContact4Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonCancel4_Click(object sender, EventArgs e)
        {
            GetContact4Data();
            Contact4Error.Text = "";

            UpdatePanelContact4Error.Update();
            UpdatePanelForm.Update();
        }

        protected void ButtonClear4_Click(object sender, EventArgs e)
        {
            bool success = ClearContact("TwilioTextName4", "TwilioTextNum4", "TwilioNum4Active");
            if (success)
            {
                Name4.Text = "";
                PhoneNum4.Text = "";
                Contact4Toggle.SelectedValue = "No";
                Contact4Error.Text = "Contact information has been cleared.";

                ContactName4.Update();
                ContactPhone4.Update();
                ContactToggle4.Update();
            }
            else
                Contact4Error.Text = "Error clearing information. Please try again.";

            UpdatePanelContact4Error.Update();
            UpdatePanelForm.Update();
        }

        protected void AdminButtonSave1_Click(object sender, EventArgs e)
        {
            string adminEmailErrorMsg = "";
            string newAdminEmail = AdminEmail1.Text;
            if (newAdminEmail != "")
            {
                adminEmailErrorMsg = ValidateAdminEmail(AdminEmail1);
                if (adminEmailErrorMsg == "")
                {
                    Admin1Error.Text = adminEmailErrorMsg;
                    bool success = SaveAdminEmail("AdminEmail1", newAdminEmail);
                    if (success)
                    {
                        Admin1Error.Text = "Admin1 information has been updated.";
                        UpdatePanelAdmin1.Update();
                    }
                    else
                        Admin1Error.Text = "Error updating information. Please try again.";
                }
                else
                    Admin1Error.Text = adminEmailErrorMsg;
            }
            else
                Admin1Error.Text = "Please enter email address";

            UpdatePanelAdmin1Error.Update();
            UpdatePanelForm.Update();
        }

        protected void AdminButtonSave2_Click(object sender, EventArgs e)
        {
            string adminEmailErrorMsg = "";
            string newAdminEmail = AdminEmail2.Text;
            if (newAdminEmail != "")
            {
                adminEmailErrorMsg = ValidateAdminEmail(AdminEmail2);
                if (adminEmailErrorMsg == "")
                {
                    Admin2Error.Text = adminEmailErrorMsg;
                    bool success = SaveAdminEmail("AdminEmail2", newAdminEmail);
                    if (success)
                    {
                        Admin2Error.Text = "Admin2 information has been updated.";
                        UpdatePanelAdmin2.Update();
                    }
                    else
                        Admin2Error.Text = "Error updating information. Please try again.";
                }
                else
                    Admin2Error.Text = adminEmailErrorMsg;
            }
            else
                Admin2Error.Text = "Please enter email address";

            UpdatePanelAdmin2Error.Update();
            UpdatePanelForm.Update();
        }
        /*
        protected void AdminButtonSave3_Click(object sender, EventArgs e)
        {
            string adminEmailErrorMsg = "";
            string newAdminEmail = AdminEmail3.Text;
            if (newAdminEmail != "")
            {
                adminEmailErrorMsg = ValidateAdminEmail(AdminEmail3);
                if (adminEmailErrorMsg == "")
                {
                    Admin3Error.Text = adminEmailErrorMsg;
                    bool success = SaveAdminEmail("AdminEmail3", newAdminEmail);
                }
                else
                    Admin3Error.Text = adminEmailErrorMsg;
            }
            else
                Admin3Error.Text = "Please enter email address";

            UpdatePanelAdmin3Error.Update();
        }*/
        /*
        protected void AdminButtonSave4_Click(object sender, EventArgs e)
        {         
            string adminEmailErrorMsg = "";
            string newAdminEmail = AdminEmail4.Text;
            if (newAdminEmail != "")
            {
                adminEmailErrorMsg = ValidateAdminEmail(AdminEmail4);
                if (adminEmailErrorMsg == "")
                {
                    Admin4Error.Text = adminEmailErrorMsg;
                    bool success = SaveAdminEmail("AdminEmail4", newAdminEmail);
                }
                else
                    Admin4Error.Text = adminEmailErrorMsg;
            }
            else
                Admin4Error.Text = "Please enter email address";

            UpdatePanelAdmin4Error.Update();
        }*/

        protected void AdminButtonClear1_Click(object sender, EventArgs e)
        {
            bool success = ClearAdminEmail("AdminEmail1");
            if (success)
            {
                AdminEmail1.Text = "";
                UpdatePanelAdmin1.Update();
                Admin1Error.Text = "Admin1 information has been cleared.";
            }
            else
                Admin1Error.Text = "Error clearing information. Please try again.";

            UpdatePanelAdmin1Error.Update();
            UpdatePanelForm.Update();
        }

        protected void AdminButtonClear2_Click(object sender, EventArgs e)
        {
            bool success = ClearAdminEmail("AdminEmail2");
            if (success)
            {
                AdminEmail2.Text = "";
                UpdatePanelAdmin2.Update();
                Admin2Error.Text = "Admin2 information has been cleared.";
            }
            else
                Admin2Error.Text = "Error clearing information. Please try again.";

            UpdatePanelAdmin2Error.Update();
            UpdatePanelForm.Update();
        }
        /*
        protected void AdminButtonClear3_Click(object sender, EventArgs e)
        {
            bool success = ClearAdminEmail("AdminEmail3");
            if (success)
            {
                AdminEmail3.Text = "";
                UpdatePanelAdmin3.Update();
                Admin3Error.Text = "";
            }
            else
                Admin3Error.Text = "Error clearing information. Please try again.";

            UpdatePanelAdmin3Error.Update(); ;
        }*/
      /*
        protected void AdminButtonClear4_Click(object sender, EventArgs e)
        {
            bool success = ClearAdminEmail("AdminEmail4");
            if (success)
            {
                AdminEmail4.Text = "";
                UpdatePanelAdmin4.Update();
                Admin4Error.Text = "";
            }
            else
                Admin4Error.Text = "Error clearing information. Please try again.";

            UpdatePanelAdmin4Error.Update(); 
        }*/
       
        protected void AdminButtonCancel1_Click(object sender, EventArgs e)
        {
            GetAdmin1Data();
            Admin1Error.Text = "";

            UpdatePanelAdmin1Error.Update();
            UpdatePanelForm.Update();
        }

        protected void AdminButtonCancel2_Click(object sender, EventArgs e)
        {
            GetAdmin2Data();
            Admin2Error.Text = "";

            UpdatePanelAdmin2Error.Update();
            UpdatePanelForm.Update();
        }
        /*
        protected void AdminButtonCancel3_Click(object sender, EventArgs e)
        {
            GetAdmin3Data();
            Admin3Error.Text = "";
            UpdatePanelAdmin3Error.Update();
        }

        protected void AdminButtonCancel4_Click(object sender, EventArgs e)
        {
            GetAdmin4Data();
            Admin4Error.Text = "";
            UpdatePanelAdmin4Error.Update();
        }*/

        protected void HomeButton_Click(object sender, EventArgs e)
        {
            var defaultURL = TextBox_defaultURL.Text.ToString();
            Response.Redirect(defaultURL);
        }
    }
}
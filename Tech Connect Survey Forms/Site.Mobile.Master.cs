using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace Tech_Connect_Survey_Forms
{
    public partial class Site_Mobile : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            InitializeSessionParameters();


        }

        private void InitializeSessionParameters()
        {
            Session["s_encryptedUserEmail"] = Request.QueryString["email"] ?? "";
            Session["s_encryptedAppPassword"] = Request.QueryString["password"] ?? "";
            Session["s_screenWidth"] = Request.QueryString["swidth"] ?? "";
            Session["s_screenHeight"] = Request.QueryString["sheight"] ?? "";
            Session["s_EncryptionKey"] = Properties.Settings.Default.ParamEncryptionKey;
            Session["s_TC_Password"] = Properties.Settings.Default.ApplicationPassword;
            Session["DefaultUrl"] = HttpContext.Current.Request.Url.AbsoluteUri;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            String encryptedEmail = Session["s_encryptedUserEmail"].ToString() ?? "";
            String encryptedPassword = Session["s_encryptedAppPassword"].ToString() ?? "";
            if (encryptedEmail != "" && encryptedPassword != "")
            {
                String decryptedEmail = DecryptStringAES(encryptedEmail.Trim());
                Session["s_UserEmail"] = decryptedEmail;

                String decryptedPassword = DecryptStringAES(encryptedPassword.Trim());
                String techConnectPswd = Session["s_TC_Password"].ToString();

                if (decryptedPassword == techConnectPswd)
                    Session["s_ValidUser"] = "true";
                else
                    Session["s_ValidUser"] = "false";
            }
            else
                Session["s_ValidUser"] = "false";

            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
            {
                GetEmailProperties();
                GetTextProperties();
            }

        }

        private string DecryptStringAES(string encryptedValue)
        {
            String encryptionKey = Session["s_EncryptionKey"].ToString();
            var keybytes = Encoding.UTF8.GetBytes(encryptionKey);
            var iv = Encoding.UTF8.GetBytes(encryptionKey);
            int encryptedLength = encryptedValue.Length;

            //DECRYPT FROM CRIPTOJS
            encryptedValue = encryptedValue.Replace(' ', '+');
            var encrypted = Convert.FromBase64String(encryptedValue);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);

            return decriptedFromJavascript;
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");

            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");

            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("key");

            string plaintext = null;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        private void GetEmailProperties()
        {
            string supportEmail = Properties.Settings.Default.SupportEmailAccount;
            string supportPassword = Properties.Settings.Default.SupportEmailPassword;
            string ticketingEmail = Properties.Settings.Default.TicketingEmail;
            string sendGridUserName = Properties.Settings.Default.SendGridUserName;
            string sendGridPassword = Properties.Settings.Default.SendGridPassword;


            Session["s_SupportEmail"] = supportEmail;
            Session["s_SupportPswd"] = supportPassword;
            Session["s_TicketingEmail"] = ticketingEmail;
            Session["s_SendGridUserName"] = sendGridUserName;
            Session["s_SendGridPassword"] = sendGridPassword;
        }

        private void GetTextProperties()
        {
            var accountSid = Properties.Settings.Default.TwilioAccountID;
            var authToken = Properties.Settings.Default.TwilioAuthToken;
            var fromPhoneNum = Properties.Settings.Default.TwilioFromPhoneNum;

            Session["s_TwilioAcctID"] = accountSid;
            Session["s_TwilioAuthToken"] = authToken;
            Session["s_TwilioFromPhoneNum"] = fromPhoneNum;

            var ttNum1Active1 = Properties.Settings.Default.TwilioNum1Active;
            var twilioTextNum1 = Properties.Settings.Default.TwilioTextNum1;
            Session["s_TwilioFromPhoneNum1"] = (ttNum1Active1 == "Yes") ? twilioTextNum1 : "";

            var ttNum1Active2 = Properties.Settings.Default.TwilioNum2Active;
            var twilioTextNum2 = Properties.Settings.Default.TwilioTextNum2;
            Session["s_TwilioFromPhoneNum2"] = (ttNum1Active2 == "Yes") ? twilioTextNum2 : "";

            var ttNum1Active3 = Properties.Settings.Default.TwilioNum3Active;
            var twilioTextNum3 = Properties.Settings.Default.TwilioTextNum3;
            Session["s_TwilioFromPhoneNum3"] = (ttNum1Active3 == "Yes") ? twilioTextNum3 : "";

            var ttNum1Active4 = Properties.Settings.Default.TwilioNum4Active;
            var twilioTextNum4 = Properties.Settings.Default.TwilioTextNum4;
            Session["s_TwilioFromPhoneNum4"] = (ttNum1Active4 == "Yes") ? twilioTextNum4 : "";

            String text1 = Session["s_TwilioFromPhoneNum1"].ToString();
            String text2 = Session["s_TwilioFromPhoneNum2"].ToString();
            String text3 = Session["s_TwilioFromPhoneNum3"].ToString();
            String text4 = Session["s_TwilioFromPhoneNum4"].ToString();

        }

        protected void HomeButton_Click(object sender, EventArgs e)
        {
            String validUser = Session["s_ValidUser"].ToString();
            if (validUser == "true")
                Response.Redirect("https://discover.championx.com/sites/pat/SitePageModern/18668/unbridled-esp-systems-home", true);
        }
    }
}
